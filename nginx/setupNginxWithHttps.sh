#!/bin/bash

cp api.media.kshrd-ite.com.conf /etc/nginx/conf.d/api.media.kshrd-ite.com.conf
cp media.kshrd-ite.com.conf /etc/nginx/conf.d/media.kshrd-ite.com.conf

nginx -t && nginx -s reload
certbot --nginx -d api.media.kshrd-ite.com -d media.kshrd-ite.com

systemctl restart nginx