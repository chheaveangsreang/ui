
## About The Project

## Built With

*   [ReacJS](https://reactjs.org/)
*   [Tailwind CSS](https://tailwindcss.com/)

## Usage

**Login Page**
1. Login with email and password

**Register Page**
1. Register with google account
2. Use real identity to register

**Student Newsfeed**
1. Newsfeed announcement for student
2. Search announcement by subject
3. Chat 
4. Group announcement
5. Profile setting
6. Logout

**Teacher Newsfeed**
1. Newsfeed announcement for teacher (/newsfeed-st/news)
2. Search announcement by subject
3. Group chat management (/newsfeed-st/chat)
4. Group announcement management (/newsfeed-st/group)
5. Profile setting
6. Logout

**Admin Dashborad**
1. Newsfeed announcement for admin (/admin/newsfeed)
2. Group chat management(/admin/chat)
3. Group chat (/admin/contact/id)
4. Stastistics (/admin/statistic)
5. Accademic management (/admin/academic)
6. Group academic (/admin/groupacademic)
7. Post by generation (/admin/post-generation)
8. User Management (/admin/user)
9. User profile (/admin/profile-detail)
10. Logout



## What we have done

*   [Login Page](#)
*   [Register Page](#)
*   [Student Newsfeed](#)
*   [Teacher Newsfeed](#)
*   [Admin Dashboard](#)
