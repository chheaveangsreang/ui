import { gapi } from "gapi-script";
import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import RoutesAll from "./RoutesAll";

function App() {
  const clientId =
    "832345515082-u0i3a360u803e0pl4bbnpt28mjg942pa.apps.googleusercontent.com";
  useEffect(()=>{
    function start() {
      gapi.client.init({
        clientId: clientId,
        scope: "",
      });
    }
    gapi.load("client: auth2", start);
  },[])

  return (
    <Routes>
          <Route path="/*" element={<RoutesAll />} />
    </Routes>
  )
}

export default App;
