import React from "react";
import notfound from './../assets/icons/notfound.svg'
export default function PageNotFound() {
  return (
    <div>
      <img
        src={notfound}
        alt="Page Not Found!!!"
        className="w-full max-h-screen"
      />
    </div>
  );
}
