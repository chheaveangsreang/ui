import React from "react";
import { Outlet } from "react-router-dom";
import ListUserContactComponent from "../components/message/ListUserContactComponent";
import SmallScreenListUser from "../components/message/SmallScreenListUser";
import ProfileBarComponent from "../components/user/ProfileBarComponent";
export default function MessagePage() {
  return (
    <div>
      <div className="block  max-w-full  border-gray-200 sticky top-0  z-10">
        <div className=" flex justify-between items-center pt-2  p-0 lg:p-3 md:p-3 sm:p-3 bg-gray-100">
          <h3 className="lg:text-3xl md:text-lg sm:text-base text-sm  font-semibold text-left ">
            Message
          </h3>
          <div>
            <ProfileBarComponent/>
          </div>
        </div>
      </div>

      <div className="mt-3 z-10">
        <div className="hidden sm:hidden md:block lg:block">
          <div className="grid grid-cols-12 gap-3">
            <div className="col-span-12 sm:col-span-12 md:col-span-4 lg:col-span-4 bg-white ">
              <ListUserContactComponent />
            </div>
            <div className="col-span-0 sm:col-span-0 md:col-span-8 lg:col-span-8  bg-white w-full hidden sm:hidden md:block lg:block">
             
                 <Outlet />
              
              
            </div>
          </div>
        </div>

        <div className="block sm:block md:hidden lg:hidden">
          <div className="bg-white">
            <SmallScreenListUser />
          </div>
        </div>
      </div>
    </div>
  );
}
