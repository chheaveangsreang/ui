import React from "react";
import { Outlet } from "react-router-dom";
import SideBarAdminComponents from "./../components/SideBarAdminComponents";
export default function HomePageAdmin() {
  return (
    <div className="flex gap-5 bg-gray-100 mx-auto container mx-w-[100vh]">
      <div className="w-[15%]">
        <SideBarAdminComponents />
      </div>

      <div className="w-[85%]">
        <Outlet />
      </div>
    </div>
  );
}
