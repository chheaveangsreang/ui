import React from "react";
import { Outlet } from "react-router-dom";

export default function SignInUpPage() {
  return (
    <div className="flex items-center justify-center h-screen bg-whitesmoke">
      <Outlet />
    </div>
  );
}
