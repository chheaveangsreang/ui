import React from "react";
import { useEffect } from "react";
import { NavLink, useParams } from "react-router-dom";
import authService from "../service/authService";

export default function ConfirmAccouuntPage() {
  const params = useParams();
  useEffect(()=>{
    authService.confirmAcc(params.token).then((res)=>{
      console.log("res", res)
    })
  }, [])
  return (
    <div class="container mx-auto">
      <div class="flex justify-center px-6 my-12">
        <div class="w-full xl:w-3/4 bg-gray-400 lg:w-11/12 flex items-center justify-center">
          <div
            class="w-full h-auto  hidden lg:block lg:w-1/2 bg-cover "
          // style="background-image: url('https://source.unsplash.com/oWTW-jNGl9I/600x800')"
          >
            <img
              src="https://cdni.iconscout.com/illustration/premium/thumb/entrepreneur-getting-business-approval-4819005-4010293.png"
              alt=""
            />
          </div>

          <div class="w-full lg:w-1/2 bg-white p-5  lg:rounded-l-none">
            <div className="grid grid-cols gap">
              <svg
                className="mt-10 lg:mt-14 lg:h-32 md:h-24 h-20  col-start-3 col-span-2"
                viewBox="0 0 66 59"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M36.3 0H29.7C13.2971 0 0 13.2028 0 29.4894V45.8723C0 53.1108 5.90984 58.9787 13.2 58.9787H36.3C52.7029 58.9787 66 45.7759 66 29.4894C66 13.2028 52.7029 0 36.3 0Z"
                  fill="#5A46AA"
                />
                <path
                  d="M24.432 38.216C23.888 38.216 23.472 38.064 23.184 37.76C22.896 37.456 22.752 37.032 22.752 36.488V22.592C22.752 22.048 22.904 21.624 23.208 21.32C23.528 21.016 23.96 20.864 24.504 20.864C24.984 20.864 25.36 20.96 25.632 21.152C25.92 21.328 26.184 21.64 26.424 22.088L31.968 32.216H31.152L36.696 22.088C36.936 21.64 37.192 21.328 37.464 21.152C37.752 20.96 38.128 20.864 38.592 20.864C39.136 20.864 39.552 21.016 39.84 21.32C40.144 21.624 40.296 22.048 40.296 22.592V36.488C40.296 37.032 40.152 37.456 39.864 37.76C39.576 38.064 39.16 38.216 38.616 38.216C38.072 38.216 37.648 38.064 37.344 37.76C37.056 37.456 36.912 37.032 36.912 36.488V26.768H37.44L32.976 34.76C32.784 35.064 32.576 35.296 32.352 35.456C32.144 35.6 31.864 35.672 31.512 35.672C31.16 35.672 30.872 35.592 30.648 35.432C30.424 35.272 30.224 35.048 30.048 34.76L25.536 26.744H26.112V36.488C26.112 37.032 25.968 37.456 25.68 37.76C25.408 38.064 24.992 38.216 24.432 38.216Z"
                  fill="white"
                />
              </svg>
            </div>
            <div class="px-8 mb-4 text-center">
              <h3 class="pt-4 mb-2 text-2xl">KSHRD Media</h3>
              <p>
                Your account has been created and a verification email has been
                sent to your registered email address. Please Click on the
                verification link included in the email to activate your
                account.{" "}
              </p>
            </div>
            <div class="mb-6 text-center">
              <NavLink to={"/"}>
                <button
                  class="w-full px-4 py-2 font-bold text-white bg-purple-500 rounded-full hover:bg-purple-700 focus:outline-none focus:shadow-outline"
                  type="button"
                >
                  Sign In Now
                </button>
              </NavLink>

            </div>
            <hr class="mb-6 border-t" />

          </div>
        </div>
      </div>
    </div>
  );
}