import React from "react";
import { Outlet } from "react-router-dom";
import SideBarComponent from "../components/SideBarComponent";
export default function ShowAllPostPage() {
  return (
    <div className="flex bg-gray-100 mx-auto container">
      <div>
        <SideBarComponent/>
      </div>
      <div className="pl-2 lg:pl-4 md:pl-4 sm:pl-2 pr-0 w-full">
        <div className="h-screen overflow-y-auto no-scrollbar">
          <Outlet />
        </div>
      </div>
    </div>
  );
}
