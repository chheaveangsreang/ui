import { configureStore } from "@reduxjs/toolkit";
import announcementSlice from "../slices/announcementSlice";
import generationReducer from '../slices/generationSlice'
import googleAuth from "../slices/googleAuth";
import groupChatReducer from "../slices/groupChatSlice";
import messageSlice from "../slices/messageSlice";
import  groupReducer  from "../slices/groupSlice"
import groupMemberReducer from "../slices/groupMemberSlice";
import profileDetailReducer from "../slices/profileDetailSlice";
import paginationReducer from "../slices/paginationSlice";
import userGenerationReducer from '../slices/UserPerGenerationSlice'
// import profileDetailReducer from '../slices/profileDetailSlice'
import groupChatMemberReducer from '../slices/groupChatMemberSlice'


export const store = configureStore({

    reducer: {
        generation: generationReducer,
        googleData: googleAuth,
        groupChat: groupChatReducer,
        announcement: announcementSlice,
        message: messageSlice,
        group:groupReducer,
        groupMember: groupMemberReducer,
        user: userGenerationReducer,
        profile: profileDetailReducer,
        groupChatMember:groupChatMemberReducer,
        pagination : paginationReducer

        // devTools: true,
    },
})
