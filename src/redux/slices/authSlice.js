import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"
import authService from "../../service/authService"

// Get user from localStorage
// const user = JSON.parse(localStorage.getItem('jwt'))

const initialState = {
    user: {},
    isSuccess: true,
    message: {}
}

//Register user
export const register = createAsyncThunk('auths/register', async (newUser, thunkAPI) => {
    try {
        return await authService.register(newUser)
    }catch (error) {
        
        const message =
        (error.response &&
            error.response.data &&
            error.response.data.message) ||
            error.message ||
            error.toString()
        return thunkAPI.rejectWithValue(message)
    }
})

// Login user
export const login = createAsyncThunk('auths/login', async (user, thunkAPI) => {
    try {
        return await authService.login(user)
      } catch (error) {
        const message =
          (error.response && error.response.data && error.response.data.message) ||
          error.message ||
          error.toString()
        return thunkAPI.rejectWithValue(message)
    }
})

//Logout
export const logout = createAsyncThunk('auths/logout', () => {
    authService.logout()
})

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        reset: (state) => {
          state.isSuccess = false
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(register.fulfilled, (state, action) => {
                state.isSuccess = true
            })
            .addCase(register.rejected, (state, action) => {
                state.user = null
                state.isSuccess = false
                state.message = action.payload
            })
            .addCase(login.fulfilled, (state, action) => {
                state.isSuccess = true
                state.user = action.payload
            })
            .addCase(login.rejected, (state, action) => {
                state.isSuccess = false
                state.user = null
                state.message = action.payload
            })
            .addCase(logout.fulfilled, (state) => {
                state.user = null
            })
    },
})
    
export const { reset } = authSlice.actions
export default authSlice.reducer