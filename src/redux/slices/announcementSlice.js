
import { createSlice } from "@reduxjs/toolkit";
import { act } from "react-dom/test-utils";


const initialState = {
    value: [],
    postInfo: [],
    userInfo: [],
    commentList: [],
}
export const announcementSlice = createSlice({
    name: "announcement",
    initialState,
    reducers: {
        getALLAnnouncement:(state,action) =>{
            state.value=action.payload
            state.value = state.value.slice().sort((a,b)=>
                (a.postInfo.lastUpdated < b.postInfo.lastUpdated) ? 1 : ((b.postInfo.lastUpdated < a.postInfo.lastUpdated) ? -1: 0)
         
                )
        },
        
        createPost: (state, action) => {
            state.value.push(action.payload)
        },
        makeComment: (state, action) => {
            state.commentList.push(action.payload)
        },
        getComment: (state, action) => {
            state.commentList = action.payload
        },
        replies: (state, action) => {
            state.commentList.replyComment.push (action.payload)
        },
        updateComment: (state, action) => {
            const com = state.value.filter(c => c.id !== action.payload.id);
            state.commentList = [...com, action.payload.payload]
        },
        deleteComment: (state, action) => {
            const com = state.value.filter(c => c.id !== action.payloadid);
            state.commentList = com;
        },
        likePost: (state, action) => {
            state.postInfo.push(action.payload)
        },
      
        deletePost:(state,action)=>{
            state.value = state.value.filter(item => item.postInfo.id !== action.payload)
        },
        editPost:(state,action) => {
            state.value.map((post) => {
                if(post.id === action.payload.id){
                    post.groupId = action.payload.groupId
                    post.subject = action.payload.content
                    post.imageUrl = action.payload.imageUrl
                }
            })
        }
    }

})
export const {updateComment, deleteComment,getALLAnnouncement, createPost, makeComment, getComment, replies,likePost, deletePost, editPost}=announcementSlice.actions
export default announcementSlice.reducer