import { Navigate, Outlet, useLocation } from "react-router-dom"
import { getUserInfo } from "../../utils/crypto";
// import UseAuth from "./UseAuth";

const RequireAuth = ({ allowedRoles }) => {

  const userInfo = getUserInfo()
  console.log("User info : ", userInfo)
  const location = useLocation();
  return (
    allowedRoles?.includes(userInfo?.roles[0]?.name)
          ? <Outlet />
          : <Navigate to="/" state={{ from: location }} replace />
  );
}

export default RequireAuth;

