import { createSlice } from "@reduxjs/toolkit"


const initialState = {
    value: [],
}

export const groupChatSlice = createSlice({
    name: "groupChat",
    initialState,
    reducers:{
        getGroupChat(state, action){
            state.value = action.payload.payload
        },
        getGroupChatById(state, action){
            state.value = action.payload.payload
        },
        postGroupChat:(state, action)=>{
            state.isSuccess = true
            state.value.push(action.payload.payload)         
        },
        editGroupChat:(state,action) => {
            const { id } = action.payload.payload;
            const group = state.value.filter(g => g.id !== id);
            state.value = [...group, action.payload.payload]
        },
        deleteGroupChat:(state,action)=>{
            const id  = action.payload;
            const groups = state.value.filter(group => group.id !== id);
            state.value = groups;
        },

        reset: (state) => {
            state.isSuccess = false
            state.message = {}
        },
    },
})

export const { getGroupChat, getGroupChatById,postGroupChat,deleteGroupChat,editGroupChat} = groupChatSlice.actions
export default groupChatSlice.reducer;