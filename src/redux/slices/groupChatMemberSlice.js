import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    value: [],
    status: 'idle', //'idle' | 'loading' | 'succeeded' | 'failed'
    error: null
}

export const groupChatMemberSlice = createSlice({
    name : "groupChatMember",
    initialState,
    reducers:{
        getAllGroupChatMember(state, action){
            state.value = action.payload.payload
        },
        addGroupChatMember(state,action){
            state.value.push(action.payload.payload)  
        },
        deleteMemberGroupChat(state,action){
            const id  = action.payload;
            const groups = state.value.filter(group => group.id !== id);
            state.value = groups;
        }
    }
})

export const {getAllGroupChatMember,addGroupChatMember,deleteMemberGroupChat} = groupChatMemberSlice.actions
export default  groupChatMemberSlice.reducer