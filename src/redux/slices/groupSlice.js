import { createSlice } from "@reduxjs/toolkit"


const initialState = {
    value: [],
    status: 'idle',
    error: null
}

export const groupSlice = createSlice({
    name: "group",
    initialState,
    reducers:{
        getAllGroup(state, action){
            state.value = action.payload.payload
        },
        postGroup:(state, action)=>{
            state.value.push(action.payload.payload)         
        }, 
        editGroup:(state,action) => {
            const group = state.value.filter(g => g.id !== action.payload.id);
            state.value = [...group, action.payload]
        },
        removeGroup:(state,action)=>{
            const id  = action.payload;
            const groups = state.value.filter(group => group.id !== id);
            state.value = groups;
        },
        
             
       
        postAdded: {
            reducer(state, action) {
                state.value = action.payload
            },
            
        },
       

    },
 
})

export const getPostsStatus = (state) => state.group.status;
export const getPostsError = (state) => state.group.error;
export const selectPostById = (state, postId) =>
    state.group.value.find(post => post.id === postId);

export const { getAllGroup,postAdded,postGroup,creategroup,editGroup,removeGroup} = groupSlice.actions
export default groupSlice.reducer;
