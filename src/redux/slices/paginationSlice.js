import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    page: [],
    
}

export const paginationSlice = createSlice({
    name : "pagination",
    initialState,
    reducers:{
        groupPagination(state, action){
            state.page = action.payload
        },
        newsFeedPagination(state, action){
            state.page = action.payload
        }
        
    }
})

export const {groupPagination,newsFeedPagination} = paginationSlice.actions
export default  paginationSlice.reducer