import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    value: [],
    status: 'idle', //'idle' | 'loading' | 'succeeded' | 'failed'
    error: null
}

export const groupMemberSlice = createSlice({
    name : "groupMember",
    initialState,
    reducers:{
        getAllGroupMember(state, action){
            state.value = action.payload.payload
        },
        addGroupMember(state,action){
            state.value.push(action.payload.payload)  
        },
        deleteGroupMember(state,action){
            const id  = action.payload;
            const groups = state.value.filter(group => group.id !== id);
            state.value = groups;
            console.log("delete: ", action.payload.message)
        },
    }
})

export const {getAllGroupMember,addGroupMember,deleteGroupMember} = groupMemberSlice.actions
export default  groupMemberSlice.reducer