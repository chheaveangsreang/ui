import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: []
};

const messageSlice = createSlice({
  name: "message",
  initialState,
  reducers:{
    getMessage: (state, action) => {
      state.value = action.payload.payload
    },
    sendMsg:(state, action) =>{
      if(state.value.filter(n => n.id === action.payload.id) === true){
        return state.value;
      }
      state.value.push(action.payload)
    },
  },
});

export const messageAction = messageSlice.actions
export default messageSlice.reducer
