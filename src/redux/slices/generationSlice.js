import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    value: [],
}

export const generationSlice = createSlice({
    name: "generation",
    initialState,
    reducers:{
        getAllGeneration(state, action){ 
            state.value = action.payload.payload
        },
        getGeneratonById(state,action){
            state.value = action.payload.payload
        },
        filterUserByGeneration(state,action){
            state.value = action.payload.payload
        },
        editGeneration(state,action){
            const { id } = action.payload;
            console.log("id slice: ",id)
            const group = state.value.filter(g => g.id !== id);
            state.value = [...group, action.payload]
        },
        deleteGeneration(state,action){
            const id  = action.payload;
            const generation = state.value.filter(group => group.id !== id);
            state.value = generation;
        },
        addGeneration(state,action){
            state.value.push(action.payload.payload)      
        }
        
    }
})

export const { 
    getAllGeneration,
    getGeneratonById,
    filterUserByGeneration,
    editGeneration,
    deleteGeneration,
    addGeneration
} = generationSlice.actions
export default generationSlice.reducer;
