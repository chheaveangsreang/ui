import { useContext } from "react";
import AuthContext from "./authRoute";

const UseAuth = () => {
    return useContext(AuthContext);
}

export default UseAuth;