import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    value: [],
    status: 'idle', //'idle' | 'loading' | 'succeeded' | 'failed'
    error: null
}

export const profileDetailSlice = createSlice({
    name:"profile",
    initialState,
    reducers:{
        getProfileUser(state,action){
            state.value = action.payload
        },
        editProfileUser(state,action){
            // const { id } = action.payload.payload;
            // console.log("data id slice: ",id)
            // const group = state.value.filter(g => g.id !== id);
            // state.value = [...group, action.payload.payload]

            const 
               { id }
               = action.payload.payload;

               console.log("slice id: ",id)

              if (id) {
                state.value = state.value.map((item) =>
                  item._id === id ? action.payload.payload : item
                );
                // state.tours = state.tours.map((item) =>
                //   item._id === id ? action.payload : item
                // );
              }
        }

    }


})
export const {getProfileUser,editProfileUser} = profileDetailSlice.actions
export default profileDetailSlice.reducer;