import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: [],
};

export const googleAuth = createSlice({
  name: "auth",
  initialState,
  reducers: {
    fetchDataFromGoogle: (state, action) => {
      state.value = action.payload;
    },
  },
});

export const { fetchDataFromGoogle } = googleAuth.actions;
export default googleAuth.reducer;
