
import { createSlice } from "@reduxjs/toolkit";
const initialState = {
    value: 
        {
            role:'',
        }
} 
export const roleSlice = createSlice({
    name: "role",
    initialState,
    reducers: {
        postAdded:(state,action) =>{
            state.value=action.payload
        }
    }

})
export const {postAdded}=roleSlice.actions
export const selectRole = (state)=> state.role.value
export default roleSlice.reducer