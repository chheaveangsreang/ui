import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    value: [],
    status: 'idle', //'idle' | 'loading' | 'succeeded' | 'failed'
    error: null
}

export const UserPerGenerationSlice = createSlice({
    name: "user",
    initialState,
    reducers:{
        getAllUser(state,action){
            state.value = action.payload.payload

        },
        listUserGneration(state,action){
            state.value = action.payload.payload
        },
        removeUserGeneration(state,action){
         
                const id  = action.payload;
                console.log("id delete: ",id)
                const user = state.value.filter(group => group.id !== id);
                state.value = user;
     
        },
        editUserGeneration(state, action){
          
                // console.log("first", action.payload.payload)
                const user = state.value.filter(u => u.id !== action.payload.id);
                state.value = [...user, action.payload]
           
            
        },
        getAllRole(state,action){
            state.value = action.payload.payload
        },
        getAllCourse(state,action){
            state.value = action.payload.payload
        }
    }
})
export const {getAllUser,listUserGneration,removeUserGeneration,editUserGeneration,getAllRole,getAllCourse} = UserPerGenerationSlice.actions
export default UserPerGenerationSlice.reducer