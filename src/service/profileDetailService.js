import api from "../utils/api";


export const getProfileUserById = async(uuid) => {
    const result = await api.get(`users/${uuid}`)
    return result.data;
}
export const editProfileUser = async(data) => {
    const result = await api.put(`users/`,data)
    console.log("result: ",result)
    return result.data;
}


const profileDetailService = {
    getProfileUserById,
    editProfileUser
    
}

export default profileDetailService;