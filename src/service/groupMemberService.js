import api from "../utils/api"

// member
export const listGroupMember = async(id,limit,page)=>{
    const response = await api.get(`announcement/groups/${id}/member?limit=${limit}&page=${page}`)
    return response.data

}
export const addGroupMember = async(data)=>{
    const response = await api.post(`announcement/groups/member`,data)
    return response.data
}
export const deleteGroupMember = async(data)=>{
    const response = await api.delete(`announcement/groups/member`,data)
    return response.data
}


const groupMemberService = {
    listGroupMember,
    addGroupMember,
    deleteGroupMember
}
export default groupMemberService;