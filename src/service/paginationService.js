import api from "../utils/api"

const page = async(limit,page) => {
    const response = await api.get(`posts/announcement/group?limit=${limit}&page=${page}`)
    return response.data;
}

const paginationService = {
    page,
}

export default paginationService;