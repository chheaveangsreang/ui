import api from "../utils/api"

const getPostService = async() => {
    const response = await api.get(`posts/?limit=200`)
    return response;
}

const createPost = async(postData) => {
    const response = await api.post(`posts/`,postData)
    return response;
}
const createComment = async(postId,content) => {
    const response = await api.post(`posts/${postId}/comment?content=${content}`)
    return response;
}
const updateComment = async(id,content) => {
    const response = await api.put(`posts/comment/${id}?content=${content}`)
    return response;
}
const uploadFile = async (files,config) => {
    const response = await api.post(`files/`,files,config);
    console.log("Response: ", response)
    return response;
}
const uploadMultipleFile = async (files) => {
    const response = await api.post(`files/multipleFiles`,files);
    console.log("Response: ", response)
    return response;
}

const reactPost = async (postId) => {
    const response = await api.post(`posts/${postId}/react`);
    return response;
}

const replyComment = async (id,content) => {
    const response = await api.post(`posts/comment/${id}/reply?content=${content}`)
    return response;
}

const deleteComment = async (id) => {
    const response = await api.delete(`posts/comment/${id}`)
    return response;
}

const deletePost = async (id) => {
    const response = await api.delete(`posts/${id}`)
    return response;
}
const filterPostByGroupId = async (groupId, limit, page) =>{
    const response = await api.get(`posts/group/filter?groupId=${groupId}&limit=${limit}&page=${page}`)
    return response.data;
}

const updatePost = async (id,data) => {
    const response = await api.put(`posts/${id}`,data);
    return response.data.payload;
}
const postService = {
    getPostService,
    createPost,
    createComment,
    uploadFile,
    reactPost,
    replyComment,
    uploadMultipleFile,
    deleteComment,
    updateComment,
    deletePost,
    filterPostByGroupId,
    updatePost,
}

export default postService;
