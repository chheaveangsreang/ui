import api from "../utils/api";


export const updateUser = async(formData) => {
    const result = await api.put(`v1/users/managements/`, formData)
    return result.data;
}
export const deleteUser = async(id) => {
    const result = await api.delete(`users/managements/${id}`)
    return result.data;
}
export const getAllRoles = async() => {
    const result = await api.get(`statistics/role`)
    return result.data;
}
export const getAllCourse = async() => {
    const result = await api.get(`statistics/course`)
    return result.data;
}

const userManagementService = {
    getAllRoles,
    deleteUser,
    updateUser,
    getAllCourse
}

export default userManagementService;
