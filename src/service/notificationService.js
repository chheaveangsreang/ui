
import api from "../utils/api"

const getAllNotification = async (limit, page) => {
    const response = await api.get(`notifications/?limit=${limit}&page=${page}`)
    return response.data;
}
const getUnReadNotification = async () => {
    const response = await api.get(`notifications/unread`)
    return response.data;
}

const notificationService = {
    getAllNotification,
    getUnReadNotification,
}

export default notificationService;