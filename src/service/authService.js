import api from "../utils/api"

//Register user
const register = async (userData) => {
    const response = await api.post("auths/register", userData)
    return response.data;
  
}

// Login user
const login = async (userData) => {
    const response = await api.post("auths/login", userData)
    // if (response.data.success) {
    //     const encryptedToken = await encryptToken(response.data.payload.token);
    //   localStorage.setItem("jwt", encryptedToken);
    // }
    return response.data
}

const confirmAcc = async (token) => {
    const response = await api.get(`auths/register/confirm?token=${token}`)
    // if (response.data.success) {
    //     const encryptedToken = await encryptToken(response.data.payload.token);
    //   localStorage.setItem("jwt", encryptedToken);
    // }
    return response.data
}

// Logout user
const logout = () => {
    if(localStorage.getItem('jwt')!== null || localStorage.getItem('userInfo')) {
        localStorage.removeItem('jwt')
        localStorage.removeItem('userInfo')
        localStorage.clear()
    }
}

const authService = {
    register,
    logout,
    login,
    confirmAcc
}

export default authService;
