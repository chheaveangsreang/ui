import api from "../utils/api";

export const getAllGroup = async() => {
    const result = await api.get(`posts/announcement/group?limit=50`)
    return result.data;
}

export const addGroup = async(data)=>{
    const response = await api.post('announcement/groups/',data)
    return response.data
}
export const deleteGroup = async(id)=>{
    const response = await api.delete(`announcement/groups/${id}`)
    return response.data
}
export const updateGroup = async(data)=>{
    const response = await api.put('announcement/groups/',data)
    return response.data
}

const groupService = {
    getAllGroup,
    addGroup,
    deleteGroup,
    updateGroup
}

export default groupService;