import api from "../utils/api";

//Get all generation
const getAllGeneration = async (limit,page) =>{
    const response = await api.get(`generations/all?limit=${limit}&page=${page}`)
    return response.data;
}
const getGenerationInfo = async (id) =>{
    const response = await api.get(`statistics/?id=${id}`)
    return response.data;
}
const getGenerationById = async (id) =>{
    const response = await api.get(`generations/${id}`)
    return response.data;
}
const editGeneration= async (data) =>{
    const response = await api.put(`generations/`,data)
    return response.data;
}
const deleteGeneration= async (id) =>{
    const response = await api.delete(`generations/?id=${id}`)
    return response.data;
}
const createGeneration= async (data) =>{
    const response = await api.post(`generations/`,data)
    return response.data;
}
  
const generationService = {
    getAllGeneration,
    getGenerationInfo,
    getGenerationById,
    editGeneration,
    deleteGeneration,
    createGeneration
}

export default generationService;