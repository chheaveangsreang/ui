import api from "../utils/api";

//Get all groupchat
const getGroupChat = async (limit,page) =>{
    const response = await api.get(`chats/group?limit=${limit}&page=${page}`)
    return response.data;
}
//Get all groupchat
const getGroupChatById = async (id) =>{
    const response = await api.get(`chats/groupchat/${id}`)
    return response.data;
}
//Update group groupchat
const updateGroupChat = async (id,groupChat) =>{
    const response = await api.put(`groupchats/${id}`, groupChat)
    return response.data;
}
//Delete group groupchat
const deleteGroupChat = async (id) =>{
    const response = await api.delete(`groupchats/${id}`)
    return response.data;
}
//Greate new gorup chat
const makeGroupChat = async (groupData) =>{
    const response = await api.post("groupchats/", groupData)
    return response.data;
}
 
const groupChatService = {
    getGroupChat,
    updateGroupChat,
    deleteGroupChat,
    getGroupChatById,
    makeGroupChat
}

export default groupChatService;