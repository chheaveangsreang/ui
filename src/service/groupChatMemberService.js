import api from "../utils/api"


export const listGroupChatMember = async(id)=>{
    const response = await api.get(`chats/member/groupchat/${id}`)
    console.log("group chat member: ",response.data)
    return response.data

}
export const addGroupChatMember = async(data)=>{
    const response = await api.post(`groupchats/member`,data)
    return response.data
}
export const deleteGroupChatMember = async(data)=>{
    const response = await api.delete(`groupchats/member`,data)
    return response.data
}


const groupChatMemberService = {
    listGroupChatMember,
    addGroupChatMember,
    deleteGroupChatMember
}

export default groupChatMemberService;