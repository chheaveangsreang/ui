
import api from "../utils/api"

 const getAllPostedAnnouncement = async(limit,page) => {
    const result = await api.get(`posts/?limit=${limit}&page=${page}`)
    return result.data
}

const deletePostAnnouncement = async(id) => {
    const result = await api.delete(`posts/${id}`)
    return result.data
}
const announcement = {
    getAllPostedAnnouncement,
    deletePostAnnouncement
}
export default announcement;
