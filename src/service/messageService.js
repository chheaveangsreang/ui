import api from "../utils/api"
import socket from "../utils/socket";

const getMessageByGroupId = async (id) => {
    const response = await api.get(`chats/message/groupchat/${id}`)
    return response.data;
}

const sendMessgage = async (messageRequest)=>{
    const response = await api.post("chats/private",messageRequest)
    return response.data;
}

const messageService = {
    getMessageByGroupId,
    sendMessgage,
}

export default messageService;