import api from "../utils/api";


export const filterUserByGeneration = async(id) => {
    const result = await api.get(`users/generation?generationId=${id}`)
    return result.data;
}
export const listUserByGeneration = async(id,limit,page) => {
    const result = await api.get(`users/generation?generationId=${id}&limit=${limit}&page=${page}`)
    return result.data;
}
export const deleteUserByGeneration = async(id) => {
    const result = await api.delete(`users/managements/${id}`)
    return result.data;
}
export const editUserByGeneration = async(data) => {
    const result = await api.put(`users/managements/`,data)
    return result.data;
}
export const getAllRole = async() => {
    const result = await api.put(`statistics/role`)
    console.log("data: ",result)
    return result.data;
}
export const getAllCourse = async() => {
    const result = await api.put(`statistics/course`)
    console.log("data: ",result)
    return result.data;
}

const UserPerGenerationService={
    filterUserByGeneration,
    listUserByGeneration,
    deleteUserByGeneration,
    editUserByGeneration,
    getAllRole,
    getAllCourse
}

export default UserPerGenerationService
