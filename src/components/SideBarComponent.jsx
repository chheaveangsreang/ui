import React, { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import chatlogo from "./../assets/images/chat-logo.png";
import LogoutComponent from "./auth/LogoutComponent";
import home from "./../assets/icons/home.svg";
import message from "./../assets/icons/message.svg";
import group from "./../assets/icons/group.svg";
import NotificationModalComponent from "./notification/NotificationModalComponent";
import { getUserInfo } from "../utils/crypto";
import SockJS from "sockjs-client";
import { over } from "stompjs";
import notificationService from "../service/notificationService";
import { toast } from "react-toastify";

var stompClient = null;

export default function SideBarComponent() {
  const [role, setRole] = useState("")
  const userInfo = getUserInfo()
  const [numUnreadChat, setNumUnreadChat] = useState(0);
  const [numUnreadNotificatoin, setNumUnreadNotificatoin] = useState(0);
  const [unread, setUnread] = useState(true);

  useEffect(() => {
    setRole(userInfo?.roles[0].name)
    notificationService.getUnReadNotification().then(res => {
      setNumUnreadNotificatoin(res.payload.unreadNotification);
      setNumUnreadChat(res.payload.unreadChat)
    })
    connect();
  }, [unread])

  const navigate = useNavigate();

  const connect = () => {
    let Sock = new SockJS('https://api.media.kshrd-ite.com/ws-kshrdmedia');
    stompClient = over(Sock);
    stompClient.connect({}, onConnected);
  }

  const onConnected = () => {
    stompClient.subscribe(`/user/${userInfo?.id}/private-message-notification`, onReceiveMessage);
    stompClient.subscribe(`/user/${userInfo?.id}/private-post-notification`, onReceivePost);
    stompClient.subscribe(`/user/${userInfo?.id}/private-comment-notification`, onReceiveComment);
    stompClient.subscribe(`/user/${userInfo?.id}/private-comment-notification`, onReceiveComment);
    stompClient.subscribe(`/user/${userInfo?.id}/private-reply-comment-notification`, onReceiveReplyComment);
  }

  const onReceiveMessage = (g) => {
    let receive = JSON.parse(g.body);
    toast.info(receive.userSendInfo.fullName + " send a message", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    if (numUnreadChat <= 9) {
      setNumUnreadChat((count) => count + 1)
    } else {
      setNumUnreadChat("9++")
    }
  }
  const onReceivePost = (g) => {
    let receive = JSON.parse(g.body);
    toast.info(receive.userPostInfo.fullName + " post an announcement", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    notificationCount();
  }
  const onReceiveComment = (g) => {
    let receive = JSON.parse(g.body);
    toast.info(receive.userCommentInfo.fullName + " comment on your post", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    notificationCount();
  }
  const onReceiveReplyComment = (g) => {
    let receive = JSON.parse(g.body);
    toast.info(receive.userCommentInfo.fullName + " reply your comment", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    notificationCount();
  }

  const notificationCount = () => {
    if (numUnreadNotificatoin <= 9) {
      setNumUnreadNotificatoin((count) => count + 1)
    } else {
      setNumUnreadNotificatoin("9++")
    }
  }

  return (
    <div className="bg-white ring-1 h-screen  ring-slate-900/5 pb-96 ">
      <div className="w-[50px] sm:w-[50px] lg:w-[70px] md:w-[70px]">
        <div>
          <div className="mb-20 pt-3 w-[45px] sm:w-[50px] md:w-[60px] lg:w-[60px] mx-auto">

            <img
              src={chatlogo}
              alt=""
              onClick={() => {
                setNumUnreadChat(0);
                navigate("/user/newfeed");
              }}
            />

          </div>
        </div>

        <div>
          <ul className="menu bg-base-100 align-middle">
            <li
              className=" w-full py-3 z-20"
            
            >
              {role === "ROLE_TEACHER" || role === "ROLE_STUDENT" ?
                <NavLink
                  to="/user/newfeed"
                  className={({ isActive }) =>
                    [
                      "group max-w-xs ",
                      isActive ? "bg-purple-200 rounded-md" : null,
                    ]
                      .filter(Boolean)
                      .join("")
                  }
                >
                  <img src={home} alt="" className="text-whte mx-auto w-7 h-7" />
                </NavLink> : navigate('/')
              }
            </li>
            <li
              className=" w-full  py-3 mx-auto "
            
              onClick={() => {
                  setUnread(false)
                  setNumUnreadChat(0)
                }
              }
            >
              {role === "ROLE_TEACHER" || role === "ROLE_STUDENT" ?

                <NavLink
                  to="/user/contact-user"
                  className={({ isActive }) =>
                    [
                      "group max-w-xs ",
                      isActive ? "bg-purple-200 rounded-md" : null,
                    ]
                      .filter(Boolean)
                      .join(" ")
                  }
                >
                  <img src={message} alt="" className="mx-auto w-7 h-7" />
                  {numUnreadChat > 0 ? <div class="badge badge-primary">{numUnreadChat}</div> : ""}
                </NavLink>
                :
                navigate('/')
              }

            </li>
            <li
              onClick={() => {
                setUnread(false)
                setNumUnreadNotificatoin(0);
              }}
              className=" w-full py-3 mx-auto "
         
            >
              <NotificationModalComponent />
              {numUnreadNotificatoin > 0 ? <div class="badge badge-secondary">{numUnreadNotificatoin}</div> : ""}
            </li>
            <li
              className=" w-full py-3 mx-auto "
            
            >
              {role === "ROLE_TEACHER" ?
                <NavLink
                  to="/user/group-teacher"
                  className={({ isActive }) =>
                    [
                      "group max-w-xs ",
                      isActive ? "bg-purple-200 rounded-md" : null,
                    ]
                      .filter(Boolean)
                      .join(" ")
                  }
                >
                  <img src={group} alt="" className="mx-auto " />
                </NavLink> : role === "ROLE_STUDENT" ?
                  <NavLink
                    to="/user/group"
                    className={({ isActive }) =>
                      [
                        "group max-w-xs ",
                        isActive ? "bg-purple-200 rounded-md" : null,
                      ]
                        .filter(Boolean)
                        .join(" ")
                    }
                  >
                    <img src={group} alt="" className="mx-auto " />
                  </NavLink>

                  : ""}

            </li>

            <li
              className=" w-full  py-3 mx-auto"
          
            >
              <LogoutComponent />
            </li>
          </ul>
        </div>
      </div>



    </div>
  );
}
