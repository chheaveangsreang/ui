import React, { useEffect, useState } from "react";
import notificationService from "../../service/notificationService";
import notificationIcon from "./../../assets/icons/notification.svg";
export default function NotificationModalComponent() {

  const [notification, setNotification] = useState()

  useEffect(() => {
    notificationService.getAllNotification(30, 1).then(res => {
        setNotification(
          res.payload.sort((a, b) => {
            var dateA = new Date(a.createdDate), dateB = new Date(b.createdDate)
            return dateB - dateA
          })
        )
      }
    )
  }, [])

  const [showModal, setShowModal] = useState(false);

  return (
    <div className="group max-w-xs  mx-auto hover:rounded-md  flex justify-center ">
      <label htmlFor="my-modal-3" className="modal-button">
        <img src={notificationIcon} alt="" className="w-7 h-7" />
        
      </label>

      <input type="checkbox" id="my-modal-3" className="modal-toggle" />
      <div className="modal">
        <div className="modal-box pt-0 max-h-[80vh] no-scrollbar">
          <div className="sticky top-0 px-0 bg-white">
            <div className="flex justify-between pt-3">
              <div className="lg:text-xl md:text-lg sm:text-base text-base font-semibold text-black">
                Notification
              </div>
              <div>
                <label htmlFor="my-modal-3" className=" right-2 top-2">
                  <svg
                    onClick={() => setShowModal(false)}
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-7 w-7 text-red-700 hover:bg-gray-200 hover:cursor-pointer rounded-full"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                </label>
              </div>
            </div>

          </div>

          <div className="py-2">
            {notification?.map((item, index) => (
              <div
                key={index}
                className="flex justify-between  py-2 my-2 bg-slate-100 px-3 rounded-md"
              >
                <div className="flex  items-center basis-[85%]">
                  <img
                    className="shrink-0  w-10 h-10 md:w-12 md:h-12 sm:w-12 sm:h-12 lg:w-12 lg:h-12  rounded-full mr-2"
                    src={item.userInfo.profileImage}
                    alt=""
                  />
                  <div className="text-sm text-slate-700  font-bold text-left  truncate">
                    <p className="mr-2 text-sm">{item.userInfo.fullName}</p>

                    <div className="text-sm  text-slate-500 truncate">
                      <small className=" text-[10x] sm:text-sm md:text-sm lg:text-sm">
                        {item.message}
                      </small>
                    </div>
                  </div>
                </div>

                <small className="text-[10px] basis-[15%] text-black">
                  {new Date(item.createdDate).toLocaleDateString('en-US', { date: true, hour: 'numeric', hour12: true, minute: 'numeric' })}
                </small>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
