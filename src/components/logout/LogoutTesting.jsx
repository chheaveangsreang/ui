import React from "react";
import logo from "../../../src/assets/logo.png";

export default function LogoutTesting() {
  return (
    <div className="w-full h-screen flex justify-center items-center">
      <div class="card card-compact w-96 bg-base-100 shadow-xl ">
        <figure>
          <img className="w-20" src={logo} alt="Shoes" />
        </figure>
        <div class="card-body items-center ">
          <h2 className="card-title ">KHSRD Media Logout</h2>
          <p>Are you sure?</p>
          <div class="card-actions justify-end">
            <button class="btn bg-purple-500">Logout</button>
            <button class="btn bg-red-600">Cencal</button>
          </div>
        </div>
      </div>
    </div>
  );
}
