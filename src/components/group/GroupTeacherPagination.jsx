import React, { useEffect } from 'react'
import ReactPaginate from 'react-paginate'
import { useSelector, useDispatch } from 'react-redux';
import { getAllGroup } from '../../redux/slices/groupSlice';
import groupService from '../../service/groupService';

export default function DynamicPagination({setPage}) {

  const allgroup = useSelector((state) => state.group.value)

  const dispatch = useDispatch();

  useEffect(() => {
    groupService.getAllGroup().then(r => dispatch(getAllGroup(r)))
  },[dispatch])

  console.log("All group: ",allgroup.length)

  const handlePageClick = ({selected}) => {   
    setPage(selected+1);
    console.log("selected",selected)
  }

    const pageSize = Math.ceil(allgroup.length/10);
    console.log("pagesize:",pageSize)
  return (
    
    <div>
      {allgroup.length > 0 ?
      <ReactPaginate
        previousLabel={"Prev"}
        nextLabel={"Next"}
        pageCount={pageSize}
        onPageChange={handlePageClick}
        containerClassName={"paginationButton"}
        previousLinkClassName={"paginationButtonPrev"}
        nextLinkClassName={"paginationButtonNext"}
        activeClassName={"onActive"} 
      /> : ''}
    </div>
  )
}
