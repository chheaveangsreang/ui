import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import groupicon from "./../../assets/icons/group.svg";
import { useSelector } from "react-redux";
import ProfileBarComponent from "../user/ProfileBarComponent";
export default function GroupComponent() {

  const [search, setNewSearch] = useState("");
  const group = useSelector((state) => state.group.value);
  const handleSearchChange = (e) => {
    setNewSearch(e.target.value);
  };

  const filtered = !search
    ? group
    : group.filter((g) =>
        g.groupName.toLowerCase().includes(search.toLowerCase())
      );
 
  return (
    <div>
      <div className="block  max-w-full border-gray-200 ">
        <div className="flex justify-between  items-center p-2 lg:p-3 md:p-3 sm:p-3 pt-2">
          <h3 className="whitespace-nowrap lg:text-3xl md:text-lg sm:text-base text-base   font-bold text-left">
            Group
          </h3>

          <div>
            
            <ProfileBarComponent/>
          </div>
        </div>
        <div className="w-[60%] sm:w-[50%] md:w-[50%] lg:w-[40%] mt-5">
          <div>
            <form>
              <div className="relative w-full">
                <input
                  type="search"
                  value={search}
                  onChange={handleSearchChange}
                  className="block p-2 lg:p-2.5 md:p-2.5 w-full  lg:text-sm md:text-sm text-xs text-gray-900 bg-white rounded-lg border  focus:outline-none"
                  placeholder="Search group ...."
                  required
                />
                <button
                  type="submit"
                  className="absolute top-0 right-0 p-2 lg:p-2.5 md:p-2.5 text-sm font-medium text-white bg-blue-700 rounded-r-lg border border-blue-700  focus:ring-4 focus:outline-none  "
                >
                  <svg
                    className="lg:w-5 lg:h-5 md:w-5 md:h-5 w-4 h-4"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    ></path>
                  </svg>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div className=" bg-bgmodal mt-2 mr-2 sm:mr-2 md:mr-0 lg:mr-0 ">
        <div className="h-[80vh] overflow-y-auto">
          <table className="w-full text-sm text-left text-gray-500  divide-y-2">
            <thead className="text-xs text-gray-700 uppercase   ">
              <tr>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base"
                >
                  No
                </th>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base"
                >
                  All Group
                </th>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base"
                >
                  Created
                </th>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base"
                >
                  Member
                </th>
                {/* <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base hidden sm:block md:block lg:block"
                >
                  Action
                </th> */}
              </tr>
            </thead>
            <tbody>
              {filtered.length > 0 ? filtered?.map((item, index) => (
                <tr
                  className="bg-white border-b-4 rounded-lg border-gray-100"
                  key={index}
                >
                  <td className="px-2 py-2 lg:px-6   md:px-6  sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base">
                    {index +=1}
                  </td>
                  <td className="max-w-[500px]  font-medium text-gray-900  flex items-center px-2 py-2 lg:px-6   md:px-6 sm:px-2 sm:py-2">
                    <div
                      // to="/user/post-generation"
                      className="flex justify-start items-center"
                    >
                      <img
                        src={item.imageUrl}
                        alt=""
                        className="object-cover w-[30px] h-[30px] lg:w-[50px] lg:h-[50px] md:w-[50px] md:h-[50px] sm:w-[30px] sm:h-[30px]   rounded-lg mr-2"
                      />
                      <p className="text-[10px] sm:text-sm md:text-sm lg:text-base">
                        {item.groupName}
                      </p>
                    </div>
                  </td>
                  <td className="px-2 py-2 lg:px-6 md:px-6  sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base">
                    {item.createdDate}
                  </td>
                  <td className="px-2 py-2 lg:px-6   md:px-6  sm:px-2 sm:py-2 ">
                    <div
                      className="flex font-medium hover:underline items-center"
                    >
                      <img
                        src={groupicon}
                        alt=""
                        className="h-3 w-3 mr-1 py-0 sm:h-3 sm:w-3 md:h-6 md:w-6 lg:h-6 lg:w-6"
                      />
                      <p className="text-[10px] sm:text-sm md:text-sm lg:text-base">
                        {item.amountOfMember}
                      </p>
                    </div>
                  </td>
                  {/* <td className="px-2 py-2 lg:px-6   md:px-6  sm:px-2 sm:py-2 hidden sm:block md:block lg:block">
                    <NavLink
                      to="/user/post-generation"
                      className="font-medium text-yellow-600 hover:underline text-center"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-4 w-4  sm:h-4 sm:w-4 md:h-6 md:w-6 lg:h-6 lg:w-6 border-2 text-purple-800 border-purple-600 rounded-md"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        strokeWidth={2}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                        />
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                        />
                      </svg>
                    </NavLink>
                  </td> */}
                </tr>
              )) : <b className="text-center">No data available</b> }
            </tbody>
          </table>
          {/* pagenation */}
          {/* <div className="flex justify-end  py-3">
            <div>
              <Pagination />
            </div>
          </div> */}
        </div>
      </div>
    </div>
  );
}
