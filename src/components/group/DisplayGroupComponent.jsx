import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getUserInfo } from "../../utils/crypto";
import paginationService from "../../service/paginationService";
import { groupPagination } from "../../redux/slices/paginationSlice";
import postService from "../../service/postService";
import { getALLAnnouncement } from "../../redux/slices/announcementSlice";

export default function DisplayGroupComponent() {
  const navigate = useNavigate();
  const [role, setRole] = useState("");
  const userInfo = getUserInfo();
  const dispatch = useDispatch();
  const groupShow = useSelector((state) => state?.pagination?.page);
  
  useEffect(() => {
    setRole(userInfo?.roles[0]?.name);
  }, [userInfo]);

  useEffect(() => {
    paginationService.page(5,1).then((r) => dispatch(groupPagination(r)));
  }, [dispatch]);

  const handleOnClick = (id,groupName) =>{
    postService.filterPostByGroupId(id, 10, 1).then(r =>{
      dispatch(getALLAnnouncement(r.payload));
      console.log("All ann: ", r.payload)
    })
    
    if(role === "ROLE_ADMIN"){
      navigate(`/admin/post-generation/${id}`, {state:{group: groupName}})
    }else{
      navigate(`/user/post-generation/${id}`, {state:{group: groupName}})
    }
    console.log("group : ", groupName)
  }

  return (
    // <div>
    <div className="bg-white rounded-md p-3" style={{ maxHeight: "300px" }}>
      <div className="flex justify-between">
        <p className="text-sm md:text-lg lg:text-lg  text-slate-700 group-hover:text-slate-900 font-bold text-left">
          Groups
        </p>
        {role === "ROLE_ADMIN" ? (
          <p
            onClick={() => navigate("/admin/groupacademic")}
            className="text-purple-800 text-sm cursor-pointer"
          >
            {" "}
            More
          </p>
        ) : role === "ROLE_TEACHER" ? (
          <p
            onClick={() => navigate("/user/group-teacher")}
            className="text-purple-800 text-sm cursor-pointer"
          >
            More
          </p>
        ) : role === "ROLE_STUDENT" ? (
          <p
            onClick={() => navigate("/user/group")}
            className="text-purple-800 text-sm cursor-pointer"
          >
            More
          </p>
        ) : (
          navigate("*")
        )}
      </div>
      {groupShow?.payload?.length > 0 ? groupShow?.payload?.map((key, idex) => (
        
        <div key={idex} className="group flex items-center m-1 justify-start cursor-pointer " onClick={()=> handleOnClick(key.id,key.groupName)}>
          <img
            className="shrink-0 h-11 w-11 rounded-full"
            src={key?.imageUrl}
            alt=""
          />
          <p className=" text-sm pl-3  text-slate-700 group-hover:text-slate-900 font-semibold text-left truncate">
            {key?.groupName}
          </p>
        </div>
      )) : <b className="text-center">No data available</b>}
      <hr />
    </div>
  );
}
