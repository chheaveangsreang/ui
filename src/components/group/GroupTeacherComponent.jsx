import React, { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import groupicon from "./../../assets/icons/group.svg";
import EditGroupComponent from "./EditGroupComponent";
import AddGroupComponent from "./AddGroupComponent";
import DeleteGroupComponent from "./DeleteGroupComponent";
import menu from "./../../assets/icons/menu.svg";
import { useDispatch, useSelector } from "react-redux";
import ProfileBarComponent from "../user/ProfileBarComponent";
import { groupPagination } from "../../redux/slices/paginationSlice";
import DynamicPagination from "./GroupTeacherPagination";
import paginationService from "../../service/paginationService";
import { getUserInfo } from "../../utils/crypto";
import postService from "../../service/postService";
import { getALLAnnouncement } from "../../redux/slices/announcementSlice";

export default function GroupTeacherComponent() {
  const [role, setRole] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate()
  const getGroup = useSelector((state) => state?.pagination?.page)

  const userInfo = getUserInfo()
  const [search, setNewSearch] = useState("");

  const handleSearchChange = (e) => {
    setNewSearch(e.target.value);
  };

  useEffect(() => {
    setRole(userInfo?.roles[0]?.name);
  }, [dispatch]);

  const [page,setPage] = useState(1)

  // Pagination
  useEffect(() => {
    paginationService.page(10,page).then((r) => dispatch(groupPagination(r)))
  },[dispatch, page])
  // console.log("page: ",page)

  const handleOnClick = (id,groupName) =>{
    postService.filterPostByGroupId(id, 10, 1).then(r =>{
      dispatch(getALLAnnouncement(r.payload));
    })

    if(role === "ROLE_ADMIN"){
      navigate(`/admin/post-generation/${id}`, {state:{group: groupName}})
    }else{
      navigate(`/user/post-generation/${id}`, {state:{group: groupName}})
    }
  }

  return (
    <div>
      <div className="block  max-w-full border-gray-200 ">
        <div className="flex justify-between  items-center p-0 lg:p-3 md:p-3 sm:p-3 pt-2">
          <h3 className="whitespace-nowrap lg:text-3xl md:text-lg sm:text-base text-base   font-bold text-left">
            Group
          </h3>
          <div>
            <ProfileBarComponent/>
          </div>
        </div>
        <div className="flex justify-between items-center mt-4">
          <div className="flex justify-start gap-2 basis-[80%]">
            <div className="w-[100%]">
              <form>
                <div className="relative w-full">
                  <input
                    type="search"
                    value={search} onChange={handleSearchChange}
                    className="block p-2 lg:p-2.5 md:p-2.5 w-full  lg:text-sm md:text-sm text-xs text-gray-900 bg-white rounded-lg border  focus:outline-none"
                    placeholder="Search group ...."
                    required
                  />
                  <button
                    className="absolute top-0 right-0 p-2 lg:p-2.5 md:p-2.5 text-sm font-medium text-white bg-blue-700 rounded-r-lg border border-blue-700  focus:ring-4 focus:outline-none  "
                  >
                    <svg
                      className="lg:w-5 lg:h-5 md:w-5 md:h-5 w-4 h-4"
                      fill="none"
                      stroke="currentColor"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                      ></path>
                    </svg>
                  </button>
                </div>
              </form>
            </div>
            <div className="w-[100%]">

            </div>
          </div>
          <div className="flex justify-end basis-[20%]">
            <div className="mr-2">
              <AddGroupComponent />
            </div>
          </div>
        </div>
      </div>

      <div className="bg-white mt-2 mr-2 sm:mr-2 md:mr-0 lg:mr-0 ">
        <div className="h-[80vh] overflow-y-auto ">
          <table className="table-auto w-full text-sm text-left text-gray-500  divide-y-2 bg-bgmodal">
            <thead className="text-xs text-gray-700 uppercase  ">
              <tr>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base"
                >
                  No
                </th>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base"
                >
                  All Group
                </th>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base  hidden sm:hidden md:block lg:block"
                >
                  Create By
                </th>

                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base"
                >
                  Member
                </th>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base  hidden sm:hidden md:hidden lg:block"
                >
                  Created
                </th>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base"
                >
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {getGroup?.payload?.length > 0 ? getGroup?.payload?.filter((item) => {
                if(search === ''){
                  return item;
                }else if(item?.groupName?.toLowerCase().includes(search?.toLowerCase())){
                  return item;
                }
                }).map((item, index) => (
                <tr
                  className="bg-white border-b-4 border-gray-200 rounded-lg"
                  key={index}
                >
                  <td className="px-2 py-2 lg:px-6   md:px-6  sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base">
                    {item.id}
                  </td>
                  <td className="max-w-[500px]  font-medium text-gray-900  items-center px-2 py-2 lg:px-6   md:px-6 sm:px-2 sm:py-2">
                    {role === "ROLE_TEACHER" || role === "ROLE_STUDENT" ? (
                      <div
                      onClick={()=> handleOnClick(item?.id,item?.groupName)}
                        className="flex justify-start items-center hover:cursor-pointer"
                      >
                        <img
                          src={item?.imageUrl}
                          alt=""
                          className="object-cover w-[40px] lg:w-[50px]  md:w-[50px]  sm:w-[40px]   rounded-lg mr-2"
                        />
                        <p className="text-xs sm:text-sm md:text-sm lg:text-base">
                          {item?.groupName}
                        </p>
                      </div>
                    ) : role === "ROLE_ADMIN" ? (
                      <div
                      onClick={()=> handleOnClick(item?.id,item?.groupName)}
                        className="flex justify-start items-center hover:cursor-pointer"
                      >
                        <img
                          src={item?.imageUrl}
                          alt=""
                          className="object-cover w-[40px] lg:w-[50px]  md:w-[50px]  sm:w-[40px]   rounded-lg mr-2"
                        />
                        <p className="text-xs sm:text-sm md:text-sm lg:text-base">
                          {item?.groupName}
                        </p>
                      </div>
                    ) : (
                      <div></div>
                    )}
                  </td>
                  <td className="px-2 mt-5 py-2 lg:px-6 md:px-6  sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base hidden sm:hidden md:block lg:block ">
                    {item?.createdBy?.fullName}
                  </td>

                  <td className="px-2 py-2 lg:px-6   md:px-6  sm:px-2 sm:py-2 ">
                    {role === "ROLE_TEACHER" || role === "ROLE_STUDENT" ?
                    (
                      <NavLink
                        to={`${item?.id}`}
                        className="flex font-medium hover:underline items-center"
                      >
                        <img
                          src={groupicon}
                          alt=""
                          className="h-3 w-3 mr-1 py-0 sm:h-3 sm:w-3 md:h-6 md:w-6 lg:h-6 lg:w-6"
                        />
                        <p className="text-xs sm:text-sm md:text-sm lg:text-base">
                          {item?.amountOfMember}
                        </p>
                      </NavLink>
                    ) : role === "ROLE_ADMIN" ? (
                      <NavLink
                        to={`${item?.id}`}
                        className="flex font-medium hover:underline items-center"
                      >
                        <img
                          src={groupicon}
                          alt=""
                          className="h-3 w-3 mr-1 py-0 sm:h-3 sm:w-3 md:h-6 md:w-6 lg:h-6 lg:w-6"
                        />
                        <p className="text-xs sm:text-sm md:text-sm lg:text-base">
                          {item?.amountOfMember}
                        </p>
                      </NavLink>
                    ) : (
                      <div></div>
                    )}
                  </td>
                  <td className="px-2 mt-5 py-2  lg:px-6 md:px-6  sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base hidden sm:hidden md:hidden lg:block ">
                    {item?.createdDate}
                  </td>
                  <td className="px-2 py-2 lg:px-6   md:px-6  sm:px-2 sm:py-2 items-center text-center">
                    <div className="dropdown dropdown-end">
                      <label tabIndex="0" className="m-1 cursor-pointer">
                        <img src={menu} alt="" className=" rounded-lg mr-3" />
                      </label>
                      <ul
                        tabIndex="0"
                        className="dropdown-content p-2 shadow bg-bgmodal rounded-md w-[200px] mr-[20px]"
                      >
                        <div>
                          <EditGroupComponent index={item} />
                        </div>
                        <div>
                          <DeleteGroupComponent id={item?.id} />
                        </div>
                      </ul>
                    </div>
                  </td>
                </tr>
              )) : <b>No data available</b>}
            </tbody>
          </table>
          <div className="flex justify-end  py-3">
            <DynamicPagination setPage={setPage}/>
          </div>
        </div>
      </div>
    </div>
  );
}
