import React, { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import add from "./../../assets/icons/add.svg";
import { useDispatch, useSelector } from "react-redux";
import { postGroup } from "../../redux/slices/groupSlice";
import api from "../../utils/api";
import { useNavigate } from "react-router-dom";
import groupService from "../../service/groupService";
import { getUserInfo } from "../../utils/crypto";

export default function AddGroupComponent() {
  
  let [isOpen, setIsOpen] = useState(false);
  const navigate = useNavigate()

  const [addRequestStatus, setAddRequestStatus] = useState('idle')
  const userInfo = getUserInfo()

  const [generationId, setGenerationId] = useState(userInfo.generation?.id)
  const [groupName, setGroupName] = useState("")
  const dispatch = useDispatch();
  const [image, setImage] = useState('')
  const [imageUrl, setImageUrl] = useState()

  const fileChange = (e) => {
    const objectUrl = URL.createObjectURL(e.target.files[0])
    setImageUrl(objectUrl)

    const formData = new FormData();
    formData.append('file', e.target.files[0])
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    api.post('files/', formData, config).then((res) => setImage(res.data.fileUrl))
  }


  const data = {
    generationId: generationId,
    groupName: groupName,
    imageUrl: image
  }

  const onSubmit = () => {
      try {
          
          groupService.addGroup(data).then((r)=>dispatch(postGroup(r)))
          setIsOpen(false)
          setAddRequestStatus('pending')
          setGroupName(null)
          setImageUrl(null)
          setImage(null)
         
       
      } catch (err) {
          console.error('Failed to save the post', err)
      } finally {
          setAddRequestStatus('idle')
      }

      console.log("data: ",data)
  
  
  }




  function closeModal() {

    setGroupName(null)
    setImageUrl(null)
    setImage(null)
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }





  return (
    <>
      <div
        onClick={openModal}
        className="group max-w-xs mx-auto hover:rounded-md focus:rounded-sm flex justify-centern mt-3 bg-white p-1 cursor-pointer"
      >
        <img src={add} alt="" className="w-5 h-5 bg-white" />
      </div>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={openModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto ">
            <div className="flex min-h-full items-center justify-center text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className=" max-w-[300px] md:max-w-[400px] sm:max-w-[300px] transform overflow-hidden rounded-lg bg-white py-5 px-5 w-full  text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900"
                  >
                    <p className="text-2xl font-semibold pt-3 text-black text-center">
                      Create Group
                    </p>
                  </Dialog.Title>

                  <div className="pt-5 mb-3">
                    <input
                      required

                      onChange={(e) => setGroupName(e.target.value)}
                      type="text"
                      placeholder="Group Name"
                      className="text-[10px] px-2 sm:text-sm md:text-sm lg:text-sm py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md focus:ring-1"
                    />
                  </div>
                  <div>
                    <p className="mb-1">Choose Image</p>
                    <div className="relative overflow-hidden">
                      <div className="flex justify-center items-center">
                        <label
                          htmlFor="dropzone-file"
                          className="flex w-full flex-col justify-center items-center  h-64  rounded-lg   cursor-pointer "
                        >
                          <div className="flex flex-col justify-center items-center pt-5 pb-6">
                            <svg
                              className="mb-3 w-10 h-10 text-gray-400"
                              fill="none"
                              stroke="currentColor"
                              viewBox="0 0 24 24"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"
                              ></path>
                            </svg>
                            <p className="mb-2 text-sm text-gray-500 dark:text-gray-400">
                              <span className="font-semibold">
                                Click to upload
                              </span>

                            </p>
                          </div>
                          <input id="dropzone-file"  className="form-control hidden"  onChange={fileChange}  type="file"></input>
                        
                          <img src={imageUrl} alt="" className="object-contain absolute w-full h-full  top-0 left-0 right-0 bottom-0 "/>
                        </label>
                      </div>


                    </div>
                  </div>

                  <div className="flex justify-end mt-5">
                    <div>
                      <button
                        onClick={closeModal}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2 mb-2 text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-red-800  rounded-md "
                      >
                        Cancel
                      </button>
                    </div>
                    <div>
                      <button
                        onClick={() => onSubmit()}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2 mb-2 text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-blue-800 rounded-md"
                      >
                        OK
                      </button>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
