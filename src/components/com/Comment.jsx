import CommentForm from "./CommentForm";

const Comment = ({
  comment,
  replies,
  setActiveComment,
  activeComment,
  updateComment,
  deleteComment,
  addComment,
  parentId = null,
  currentUserId,
}) => {
  const isEditing =
    activeComment &&
    activeComment.id === comment.id &&
    activeComment.type === "editing";
  const isReplying =
    activeComment &&
    activeComment.id === comment.id &&
    activeComment.type === "replying";

  const canDelete =
    currentUserId === comment.userCommentInfo?.id
  const canReply = Boolean(currentUserId);
  const canEdit = currentUserId === comment.userCommentInfo?.id
  const replyId = parentId ? parentId : comment.id;
  const createdAt = new Date(comment.createdDate).toLocaleDateString();

  return (
    <div key={comment.id} className="flex mt-5">
      <div className="mr-3">
        <img src={comment.userCommentInfo?.profileImage} alt="user" className="object-cover h-8 sm:h-10 md:h-10 lg:h-10 mr-2 lg:mr-0 md:mr-0 w-8 sm:w-10 md:w-10  lg:w-10 rounded-full"/>
      </div>
      <div className="w-full">
        <div className="flex items-center ">
          <div className="mr-7 text-black font-semibold text-[16px]">{comment.userCommentInfo?.fullName}</div>
          <div className="text-xs">{createdAt}</div>
        </div>
        {!isEditing && <div className="text-sm">{comment.comment}</div>}
        {isEditing && (
          <CommentForm
            submitLabel="Update"
            hasCancelButton
            initialText={comment.comment}
            handleSubmit={(text) => updateComment(text, comment.id)}
            handleCancel={() => {
              setActiveComment(null);
            }}
          />
        )}
        <div className="flex cursor-pointer text-xs mt-2 ">
          {canReply && (
            <div
              className="mr-2 text-blue-600 hover:underline"
              onClick={() =>
                setActiveComment({ id: comment.id, type: "replying" })
              }
            >
              Reply
            </div>
          )}
          {canEdit && (
            <div
              className="mr-2 text-green-600 hover:underline"
              onClick={() =>
                setActiveComment({ id: comment.id, type: "editing" })
              }
            >
              Edit
            </div>
          )}
          {canDelete && (
            <div
              className="mr-2 text-red-500 hover:underline"
              onClick={() => deleteComment(comment.id)}
            >
              Delete
            </div>
          )}
        </div>
        {isReplying && (
          <CommentForm
            submitLabel="Reply"
            handleSubmit={(text) => addComment(text, replyId)}
          />
        )}
        {replies?.length > 0 && (
          <div className="mt-5">
            {replies.map((reply) => (
              <Comment
                comment={reply}
                key={reply.id}
                setActiveComment={setActiveComment}
                activeComment={activeComment}
                updateComment={updateComment}
                deleteComment={deleteComment}
                addComment={addComment}
                parentId={comment.id}
                replies={[]}
                currentUserId={currentUserId}
              />
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default Comment;