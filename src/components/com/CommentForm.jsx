import { useState } from "react";

const CommentForm = ({
  postId,
  handleSubmit,
  submitLabel,
  hasCancelButton = false,
  handleCancel,
  initialText = "",
}) => {
  const [text, setText] = useState(initialText);
  const isTextareaDisabled = text.length === 0;
  const onSubmit = (event) => {
    event.preventDefault();
    handleSubmit(text, postId);
    setText("");
  };
  return (
    <form onSubmit={onSubmit} className="mb-5">
      <textarea
        className="w-full border-2 border-blue-500 focus:outline-none h-[70px] rounded-lg px-2"
        value={text}
        onChange={(e) => setText(e.target.value)}
        placeholder="Comment ...."
      />
      <button className=" p-1.5  px-3 rounded-md text-sm bg-blue-500 text-white" disabled={isTextareaDisabled}>
        {submitLabel}
      </button>
      {hasCancelButton && (
        <button
          type="button"
          className=" p-1.5  px-2 rounded-md ml-2 text-black text-sm bg-gray-200"
          onClick={handleCancel}
        >
          Cancel
        </button>
      )}
    </form>
  );
};

export default CommentForm;