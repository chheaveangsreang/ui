import { useEffect, useState } from "react";
import CommentForm from "./CommentForm";
import Comment from "./Comment";
import postService from "../../service/postService";

const Comments = ({ postId, currentUserId, backendComments }) => {
  const [activeComment, setActiveComment] = useState(null);
  const [backendComment, setBackendComment] = useState(backendComments);
  // const [replies, setReplies] = useState(backendComments.replyComment);

  const addComment = (postId, text) => {
    postService.createComment(text, postId).then((comment) => {
      setBackendComment([comment.data.payload, ...backendComment]);
      setActiveComment(null);
    });
  };
  console.log("back", backendComment)

  const replyComment = (commentId, text) => {
    postService.replyComment(text, commentId).then((comment) => {
      setBackendComment([comment.data.payload, ...backendComment]);
      // let newReplies = [comment.data.payload, ...replies];
      // setBackendComment({...backendComment, replyComment: newReplies });
      setActiveComment(null);
    });
  };

  const updatedComment = (text, commentId) => {
    postService.updateComment(commentId, text).then((r) => {
      const updatedBackendComments = backendComment.map((com) => {
        if (com.id === commentId) {
          return { ...com, comment: text };
        }
        return com;
      });
      setBackendComment(updatedBackendComments);
    });
    setActiveComment(null);
  };

  const deletedComment = (commentId) => {
    console.log("comemtid", commentId)
    postService.deleteComment(commentId).then((r) => {
      const updatedBackendComments = backendComment.filter(
        (comment) => comment.id !== commentId
      );
      setBackendComment(updatedBackendComments);
    });
  };

  return (
    <div className="mt-4">
      <div>
        <CommentForm postId={postId} submitLabel="Enter" handleSubmit={addComment} />
        {(backendComment?.map((rootComment) => (
          <Comment
            comment={rootComment}
            replies={rootComment.replyComment}
            activeComment={activeComment}
            setActiveComment={setActiveComment}
            addComment={replyComment}
            deleteComment={deletedComment}
            updateComment={updatedComment}
            currentUserId={currentUserId}
          />
        )))}
      </div>
    </div>

  );
};

export default Comments;