import { useState } from "react";

const Pagination = () => {
  let [num, setNum] = useState(1);
  let [cur, setCur] = useState(1);

  const pages = [
    { page: num },
    { page: num + 1 },
    { page: num + 2 },
    { page: num + 3 },
  ];
  function Next() {
    setNum(++num);
  }
  function back() {
    num > 1 && setNum(--num);
  }
  return (
    <div className="flex bg-white font-[Poppins] w-full p-2">
      <button
        onClick={back}
        className="h-8 
               px-2 rounded-full hover:bg-indigo-600 hover:text-white"
      >
        <p className="text-sm">Prev</p>
      </button>
      {pages.map((pg, i) => (
        <button
          key={i}
          onClick={() => setCur(pg.page)}
          className={`h-8 
               w-12 rounded-full  ${
                 cur === pg.page && "bg-purple-400 text-white"
               }`}
        >
          {pg.page}
        </button>
      ))}
      <button
        onClick={Next}
        className="h-8 
               px-2 rounded-full hover:bg-indigo-600 hover:text-white"
      >
        <p className="text-sm">Next</p>
      </button>
    </div>
  );
};

export default Pagination;
