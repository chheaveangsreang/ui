
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";

ChartJS.register(
  CategoryScale, 
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);



export default function AnalyticChart({statistic, generation}) {
   const options = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: "Bar Chart",
      },
    },
    maintainAspectRatio: false,
  };
  
  
  const labels = generation?.map((g)=>g?.generationName)
  let tempData = generation?.map((g)=>g?.amountOfStudent)
  
   const data = {
    labels,
    datasets: [
      {
        label: "Students Data",
        data: tempData,
        backgroundColor: "#a855f7",
      },
    ],
  };
  return <Bar options={options} data={data} />;
}
