import React, { useEffect } from 'react'
import ReactPaginate from 'react-paginate'
import { useSelector, useDispatch } from 'react-redux';
import { getALLAnnouncement } from '../../redux/slices/announcementSlice';
import { newsFeedPagination } from '../../redux/slices/paginationSlice';
import postService from '../../service/postService';

export default function NewsFeedPagination() {
const dispatch = useDispatch();
  const allPost = useSelector((state) => state.pagination.page)
  useEffect(() => {
    postService.getPostService().then(r => dispatch(newsFeedPagination(r.data.payload)))
  },[dispatch])

  console.log("All Post: ",allPost.length)

//   const handlePageClick = ({selected}) => {   
//     setPage(selected+1);
//     console.log("selected",selected)
//   }

    const pageSize = Math.ceil(allPost.length/1);
    console.log("pagesize:",pageSize);
    const handleSeeMore = () => {
        
    }
  return (
    
    <div className='flex justify-center'>
      {/* {allPost.length > 0 ?
      <ReactPaginate
        previousLabel={"Prev"}
        nextLabel={"Next"}
        pageCount={pageSize}
        onPageChange={handlePageClick}
        containerClassName={"paginationButton"}
        previousLinkClassName={"paginationButtonPrev"}
        nextLinkClassName={"paginationButtonNext"}
        activeClassName={"onActive"} 
      /> : ''} */}

      {pageSize > 1 ?
        <button onClick={handleSeeMore} className='bg-violet-700 text-white text-md h-8 hover:bg-violet-400 hover:text-gray-900 rounded-lg w-24'> See More </button> : ''  
    }
    </div>
  )
}
