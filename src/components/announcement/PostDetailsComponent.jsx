import React, { useEffect, useState } from "react";
import pp1 from "./../../assets/images/pp1.jpg";
import chatlogo from "./../../assets/images/chat-logo.png";
import { NavLink, useLocation, useNavigate } from "react-router-dom";
import submit from "./../../assets/icons/submit.svg";
import ReacHtmlPaser from 'react-html-parser'
import { useSelector } from "react-redux";
import ProfileBarComponent from "../user/ProfileBarComponent";
import ImageGallery from "react-image-gallery";
import { getUserInfo } from "../../utils/crypto";
import Comments from "../com/Comments";
export default function PostDetailsComponent() {
  const [role, setRole] = useState("");
  const location = useLocation();
  const navigate = useNavigate();
  const userInfo = getUserInfo()
  let id = location.state.id
  let fullName = location.state.fullName
  let content = location.state.content
  let numberOfLike = location.state.numberOfLike
  let imageUrl = location.state.imageUrl
  let numberOfComment = location.state.numberOfComment
  let createdDate = location.state.createdDatedate
  let comment = location.state.commentList
  let subject = location.state.subject
  let profileImage = location.state.profileImage
  let uuid = location.state.uuid
  useEffect(() => {
    comment?.map((item) => {
      console.log("Comment: ",item.comment) 
    })
  },[comment])




    useEffect(()=>{
    setRole(userInfo?.roles[0].name)
  },[])
  
  
  return (
    <div className="container mx-w-[70%] mx-auto">
      <div className="block  max-w-full border-gray-200  sticky top-0 bg-gray-100 z-10">
        <div className="items-center  py-5 p-0 lg:p-3 md:p-3 sm:p-3 flex justify-between">
          <h3 className="lg:text-3xl md:text-lg sm:text-base text-base  sm:block md:block lg:block  font-bold text-left ">
            Post Detail
          </h3>

          <div>
            <ProfileBarComponent/>
          </div>
        </div>
      </div>

      <div className="grid grid-cols-12 sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 gap-2 p-2 lg:p-8 md:p-5 sm:p-2 bg-white  mr-2 sm:mr-2 md:mr-0 lg:mr-0 min-h-[90vh]">
        <div className="col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-6">
          <div className="grid">
          <ImageGallery
                        items={imageUrl
                          ?.substring(1, imageUrl?.length - 1)
                          .split(",")
                          .map((i) => {
                            return {
                              original: i,
                            }
                          })
                        }
                        showPlayButton={false}
                        lazyLoad={true}
                        showFullscreenButton={false}
                        showNav={true}
                      />
          </div>
        </div>

        <div className="col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-6">
          <div className="grid grid-cols-12  md:grid-cols-12 lg:grid-cols-12 gap-2">
            <div
              className="bg-white  mb-3 col-span-12 md:col-span-12 lg:col-span-12"
              style={{ minHeight: "500px" }}
            >
              <div className="flex justify-between pb-5 px-0 sm:px-0 md:px-5 lg:px-5">
                <div className="flex">
                  <div>{role ==="ROLE_ADMIN" ? 
                    <NavLink to={`/admin/profile-detail/${uuid}`}>
                      <img
                        className="w-8 h-8 md:w-12 md:h-12 sm:w-10 sm:h-10 lg:w-12 lg:h-12  rounded-full shadow-lg mr-2 "
                        src={profileImage}
                        alt=""
                      />
                    </NavLink> :
                    role === "ROLE_TEACHER" || role === "ROLE_STUDENT" ?
                    <NavLink to={`/user/profile-detail/${uuid}`}>
                      <img
                        className="w-8 h-8 md:w-12 md:h-12 sm:w-10 sm:h-10 lg:w-12 lg:h-12  rounded-full shadow-lg mr-2 "
                        src={profileImage}
                        alt=""
                      />
                    </NavLink> 
                    :
                    ""
                    }
                  </div>
                  <div>
                    <h5 className="lg:text-lg md:text-lg sm:text-sm text-xs   font-semibold text-left text-gray-900 dark:text-white px-0 py-0">
                      {fullName}
                    </h5>
                    
                    <div className="flex">
                      <small className="text-gray-500 dark:text-gray-400 text-xs mr-2">
                        {subject}
                      </small>
                    </div>
                  </div>
                </div>

                <div>
                  <small className="text-gray-500 dark:text-gray-400 text-xs text-right">
                  {new Date(
                            createdDate
                          ).toLocaleDateString()}
                  </small>
                </div>
              </div>
              <div className="pl-3 sm:pl-12 md:pl-12 lg:pl-12">
                <p className="text-xs sm:text-sm md:text-sm lg:text-base text-left text-black-100">
                  {ReacHtmlPaser(content)}
                </p>

                {/* like and comment */}
                <div className="flex justify-start mt-1 py-2">
                  <div className="flex justify-start mr-3 ">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-5 w-5 sm:h-6 sm:w-6 lg:w-6"
                      style={{ color: "red" }}
                      viewBox="0 0 24 24"
                      fill="currentColor"
                    >
                      <path
                        fillRule="evenodd"
                        d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                        clipRule="evenodd"
                      />
                    </svg>
                    <p className="md:text-base lg:text-base sm:text-sm text-xs">
                      {" "}
                      {numberOfLike}
                    </p>
                  </div>
                  <div className="flex justify-start">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-5 w-5 sm:h-6 sm:w-6 lg:w-6"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"
                      />
                    </svg>
                    <p className="md:text-base lg:text-base sm:text-sm text-xs">
                      {" "}
                      {numberOfComment}
                    </p>
                  </div>
                </div>

               

                {/*comment  */}
                <div className="mb111-3">
                    <Comments
                      // key={index}
                      currentUserId={userInfo?.id}
                      postId={id}
                      backendComments={comment}
                    />
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
