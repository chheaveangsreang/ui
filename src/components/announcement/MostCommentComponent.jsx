import React, { useEffect } from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { getUserInfo } from "../../utils/crypto";
import chatlogo from "./../../assets/images/chat-logo.png";
export default function MostCommentComponent() {
  const mostComments = useNavigate();
  const [role,setRole] = useState('')

  const userInfo = getUserInfo()
  useEffect(()=>{
    setRole(userInfo?.roles[0]?.name)
  },[userInfo])


  return (
    <div className="bg-white rounded-md p-3" style={{ maxHeight: "400px" }}>
      <div className="flex justify-between cursor-pointer">
        <p className="text-sm d:text-lg lg:text-lg  text-slate-700 group-hover:text-slate-900 font-semibold text-left">
          Most Comment
        </p>

      </div>

      {role === "ROLE_ADMIN" ? 
      <NavLink 
      
        to="/admin/post-detail/1"
        className="group flex items-center m-2 py-2 justify-start"
      >
        <img
          className="shrink-0 h-11 w-11 rounded-full"
          src={chatlogo}
          alt=""
        />
        <p className="text-sm pl-3  text-slate-700 group-hover:text-slate-900 font-semibold text-left truncate">
          Sopheap <br />
          <small className="text-xs  text-slate-500 group-hover:text-slate-700 text-left truncate">
            Dear Student, We will meet up at real soccer
          </small>
        </p>
      </NavLink>: role === "ROLE_TEACHER" || role === "ROLE_TEACHER"  ?
       <NavLink 
       to="/user/post-detail/1"
       className="group flex items-center m-2 py-2 justify-start"
     >
       <img
         className="shrink-0 h-11 w-11 rounded-full"
         src={chatlogo}
         alt=""
       />
       <p className="text-sm pl-3  text-slate-700 group-hover:text-slate-900 font-semibold text-left truncate">
         Sopheap <br />
         <small className="text-xs  text-slate-500 group-hover:text-slate-700 text-left truncate">
           Dear Student, We will meet up at real soccer
         </small>
       </p>
     </NavLink>
      :
      ""

    }
     
    </div>
  );
}
