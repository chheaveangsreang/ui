import React, { useEffect, useState } from "react";
import { NavLink, useLocation, useNavigate, useParams } from "react-router-dom";
import DisplayGroupComponent from "../group/DisplayGroupComponent";
import MostCommentComponent from "../announcement/MostCommentComponent";
import like from "./../../assets/icons/like.svg";
import message from "./../../assets/icons/message.svg";
import group from "./../../assets/icons/group.svg";
import unlike from "../../assets/icons/unlike.svg"
import droplist from "./../../assets/icons/dropdown.svg";
import update from "./../../assets/icons/update.svg";
import PostAnnouncementComponent from "./PostAnnouncementComponent";
import DeleteAnnouncementComponent from "./DeleteAnnouncementComponent";
import { useDispatch, useSelector } from "react-redux";
import postedAnnouncement from "../../service/announcement";
import { getALLAnnouncement, likePost } from "../../redux/slices/announcementSlice";
import ProfileBarComponent from "../user/ProfileBarComponent";
import Comments from "../com/Comments";
import ImageGallery from "react-image-gallery";
import ReacHtmlPaser from "react-html-parser"
import { getUserInfo } from "../../utils/crypto";
import postService from "../../service/postService";

export default function NewsFeedComponent() {
  const [showMore, setShowMore] = useState(false);
  const [role, setRole] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();

    const [search, setSearch] = useState('')

  const getPost = useSelector((state) => state.announcement.value);
  const param = useParams()
  const location = useLocation()
  const userInfo = getUserInfo()
  let groupName = location.state.group

  useEffect(() => {
    setRole(userInfo?.roles[0]?.name);
    postService
      .filterPostByGroupId(param.id, 20, 1)
      .then((r) => {
        dispatch(getALLAnnouncement(r.payload))
      }
      );
  }, [dispatch]);

  //React Post
  const handleReactPost = (e,id,numberOfLike) => {
    e.preventDefault();
    postService.reactPost(id).then(r => dispatch(likePost(r.data)));
  }
 
  const filterSearch = !search ?
  getPost
:
getPost?.filter((g)=>
      // g?.userPostInfo?.fullName.toLowerCase().includes(search?.toLowerCase())
    
      g?.postInfo?.subject.toLowerCase().includes(search?.toLowerCase())
     
  ) 

  const handleSearch = (e) => { 
    setSearch(e.target.value) 

  };
  
  return (
    <div>
      <div className="block  max-w-full border-gray-200 sticky top-0 z-10">
        <div className="flex justify-between  items-center bg-gray-100 p-2 lg:p-3 md:p-3 sm:p-3">
          <h3 className="lg:text-3xl md:text-lg sm:text-base text-base   font-bold text-left ">
            {groupName}
          </h3>

          <div>
            <span className="flex items-center justify-end">
              <ProfileBarComponent />
            </span>
          </div>
        </div>

        <div className="grid-cols-2 pb-3  sm:grid-cols-2 md:grid-cols-2 lg:flex justify-between pt-4 bg-gray-100">
       

          {/* NarBar */}
          <div className="w-full ">
            <div className="bg-gray-100 px-2">
              <div className="lg:w-[50%] md:w-[70%] sm:w-[75%] w-[100%]">
                <form>
                  <div className="relative w-full">
                    <input
                      onChange={handleSearch}
                      type="search"
                      className="block p-2.5 w-full z-20 lg:text-sm md:text-sm text-xs text-gray-900 bg-white rounded-lg border  focus:outline-none"
                      placeholder="Search...."
                      required
                    />
                    <button
                      type="submit"
                      className="absolute top-0 right-0 p-2 lg:p-2.5 md:p-2.5 text-sm font-medium text-white bg-[#5A46AA] rounded-r-lg border border-gray-300  focus:ring-4 focus:outline-none  "
                    >
                      <svg
                        className="lg:w-5 lg:h-5 md:w-5 md:h-5 w-4 h-4"
                        fill="none"
                        stroke="currentColor"
                        viewBox="0 0 24 24"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                          d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                        ></path>
                      </svg>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>

        </div>
      </div>
      <div className="grid grid-cols-12  md:grid-cols-12 lg:grid-cols-12 gap-2 mr-2 sm:mr-2 md:mr-0 lg:mr-0 mt-2">
        <div className=" mb-3 col-span-12 md:col-span-12 lg:col-span-9 ">

          {/*show post */}
          <div className="mb-3 bg-white no-scrollbar overflow-y-auto max-h-[100vh] ">
            <div>
              <PostAnnouncementComponent />
            </div>

            {filterSearch?.length >0 ? filterSearch?.map((item, index) => (
              <div key={index} className="border-b-[8px] border-gray-200 ">
                <div className="flex justify-between px-3 lg:px-4 md:px-4 sm:px-3 pt-2">
                  <div className="flex flex-row items-center  py-3 pb-5 ">
                    <NavLink to={`/profile-detail/${item?.userPostInfo?.uuid}`}>
                      <img
                        className="object-cover w-8 h-8 md:w-12 md:h-12 sm:w-12 sm:h-12 lg:w-12 lg:h-12  rounded-full shadow-lg mr-2"
                        src={item?.userPostInfo?.profileImage}
                        alt=""
                      />
                    </NavLink>

                    <div className="text-left ml-2">
                      <NavLink
                        to="/user/profile-detail"
                        className="text-sm sm:text-base md:text-base lg:text-base font-semibold text-gray-900 dark:text-white"
                      >
                        {item?.userPostInfo?.fullName}
                      </NavLink>

                      <div className="flex justify-start">
                        <small className="text-gray-500 dark:text-gray-400 text-xs mr-3">
                          {new Date(item?.postInfo?.createdDate).toLocaleDateString()}
                        </small>

                        <img src={group} alt="" className="h-4 w-4" />
                      </div>
                    </div>
                  </div>

                  {/* {role === "ROLE_ADMIN" ?  */}
                  <div
                    className={`${role === "ROLE_ADMIN" || role === "ROLE_TEACHER"
                      ? "block"
                      : "hidden"
                      } "pb-5 z-1"`}
                  >
                    <div className="dropdown dropdown-end">
                      <label tabIndex="0" className="m-1 cursor-pointer">
                        <img
                          src={droplist}
                          alt=""
                          className="w-1.5 sm:w-1.5 md:w-2 lg:w-2"
                        />
                      </label>
                      <ul
                        tabIndex="0"
                        className={`${role === "ROLE_TEACHER" || role === "ROLE_ADMIN"
                          ? "block"
                          : "hiddem"
                          } dropdown-content p-2 shadow bg-bgmodal rounded-md w-[200px] mr-[20px]`}
                      >
                        <div onClick={() => navigate(`/edit-announcement/${item?.postInfo?.id}`,{state: {subject: item?.postInfo?.subject,content: item?.postInfo?.content,annId: item?.postInfo?.groupId,image: item?.postInfo?.imageUrl,postId: item?.postInfo?.id}})}
                            className="flex justify-center items-center hover:bg-green-300 bg-green-200 p-2 rounded-lg mb-2 cursor-pointer">
                          
                            <img src={update} alt="" />
                            <p className="ml-3 text-green-600">Edit</p>
                          
                        </div>
                        <div>
                          <DeleteAnnouncementComponent id={item?.postInfo?.id}/>
                        </div>
                      </ul>
                    </div>
                  </div>
                </div>

                <div className="mx-4 sm:mx-4 md:mx-5 lg:mx-10 z-0">
                  {/*Show more or show less */}
                  <div className="text-xs sm:text-sm md:text-sm lg:text-sm text-left text-black-100">
                    {showMore
                      ? ReacHtmlPaser(item?.postInfo?.content)
                      : ReacHtmlPaser(item?.postInfo?.content?.substring(0, 50))}
                    <button
                      onClick={() => setShowMore(!showMore)}
                      className="ml-3 text-purple-800 font-bold cursor-pointer"
                    >
                      {showMore ? "Show less" : "Show more"}
                    </button>
                  </div>

                  {/* image */}


                  <div className={`${item?.postInfo?.imageUrl === null || item?.postInfo?.imageUrl === "" ? "hidden" : "block"}`}>
                    <div className=" w-[80%] mx-auto">
                      <ImageGallery items={
                        item?.postInfo?.imageUrl
                          ?.substring(1, item?.postInfo?.imageUrl?.length - 1)
                          .split(",")
                          .map((i) => {
                            return {
                              original: i,


                            }
                          })
                      }
                        originalHeight={100}
                        originalWidth={100}
                        showIndex={true}
                      
                        lazyLoad={true}
                        showPlayButton={true}
                        showFullscreenButton={false}
                      />
                    </div>

                  </div>


                  {/* like and comment */}
                  <div className="flex justify-start  py-3">
                    <div className="flex justify-start mr-3 items-center">
                      {item.numberOfLike %2 !== 0 ?
                      <img src={like} alt="like" className="hover:cursor-pointer" onClick={e=>handleReactPost(e,item.postInfo.id)} />
                        : <img src={unlike} className="hover:cursor-pointer" onClick={e=>handleReactPost(e,item.postInfo.id)} alt="unlike"/>
                      }
                      <p className="text-sm ml-1"> {item.numberOfLike}</p>
                    </div>
                    <div className="flex justify-start items-center">
                      <img src={message} alt="" className="w-6 h-6" />
                      <p className="text-sm ml-1 "> {item?.numberOfComment}</p>
                    </div>
                  </div>

                  
                  {role==="ROLE_ADMIN" ? 
                    <div
                    className="p-2 cursor-pointer text-purple-700 text-left underline text-sm"
                    onClick={() =>
                      navigate(`/admin/post-detail/${item?.postInfo?.id}`, {state : {id:item?.postInfo?.id, commentList:item?.commentList , imageUrl:item?.postInfo?.imageUrl,numberOfLike:item?.numberOfLike, numberOfComment:item?.numberOfComment, content:item?.postInfo?.content, createdDatedate:item?.postInfo?.createdDate, fullName:item?.userPostInfo?.fullName}})}
                  >

                  
                    View more detail
                  </div>

                    :

                    <div
                      className="p-2 cursor-pointer text-purple-700 text-left underline text-sm"
                      onClick={() =>
                        navigate(`/user/post-detail/${item?.postInfo?.id}`, {state : {id:item?.postInfo?.id, commentList:item?.commentList , imageUrl:item?.postInfo?.imageUrl,numberOfLike:item?.numberOfLike, numberOfComment:item?.numberOfComment, content:item?.postInfo?.content, createdDatedate:item?.postInfo?.createdDate, fullName:item?.userPostInfo?.fullName}})}
                    >
                    
                      View more detail
                    </div>
                  }

                  <div className="mb-3">
                    <Comments key={index}
                      currentUserId={userInfo?.id}
                      postId={item?.postInfo?.id}
                      backendComments={item?.commentList}
                    />
                  </div>
                </div>
              </div>
            )) : <b className="text-center">No data available</b>}
          </div>
          {/* end show post */}
        </div>

        <div className="sm:col-span-0 md:col-span-0 lg:col-span-3 pr-2 hidden md:hidden sm:hidden lg:block ">
          <div className="overflow-x-auto sticky xl:top-[14%] lg:top-[19%] z-0 ">
            <div className="mb-3">
              <DisplayGroupComponent />
            </div>
            
            {/* <div >
              <MostCommentComponent/>
              
            </div> */}
            
          </div>
        </div>
      </div>
    </div>
  );
}
