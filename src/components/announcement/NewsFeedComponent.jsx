import React, { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import DisplayGroupComponent from "../group/DisplayGroupComponent";
import MostCommentComponent from "../announcement/MostCommentComponent";
import like from "./../../assets/icons/like.svg";
import message from "./../../assets/icons/message.svg";
import group from "./../../assets/icons/group.svg";
import unlike from "../../assets/icons/unlike.svg";
import droplist from "./../../assets/icons/dropdown.svg";
import update from "./../../assets/icons/update.svg";
import PostAnnouncementComponent from "./PostAnnouncementComponent";
import DeleteAnnouncementComponent from "./DeleteAnnouncementComponent";
import { useDispatch, useSelector } from "react-redux";
import postedAnnouncement from "../../service/announcement";
import {
  getALLAnnouncement,
  likePost,
} from "../../redux/slices/announcementSlice";
import ProfileBarComponent from "../user/ProfileBarComponent";
import Comments from "../com/Comments";
import ImageGallery from "react-image-gallery";
import ReacHtmlPaser from "react-html-parser";
import { getUserInfo } from "../../utils/crypto";
import postService from "../../service/postService";
import InfiniteScroll from "react-infinite-scroll-component";
import loading from "../../assets/icons/loading.svg";
import chatlogo from "./../../assets/images/chat-logo.png";
import FilterComponent from "./FilterComponent";

export default function NewsFeedComponent() {
  const [showMore, setShowMore] = useState(false);
  const [role, setRole] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [page, setPage] = useState(1);
  const [limit] = useState(15);

  const getPost = useSelector((state) => state.announcement.value);

  const [filtered,setFiltered] = useState([])
  const [subject,setSubject] = useState("")

  const userInfo = getUserInfo()
  const [search, setSearch] = useState('');
  useEffect(() => {
    setRole(userInfo?.roles[0].name)
    postedAnnouncement.getAllPostedAnnouncement(limit, page).then((r) => {
      dispatch(getALLAnnouncement(r.payload));
    });
  }, []);

  const fetchMoreData = () => {
    setTimeout(() => {
      setPage(page + 1);
      postedAnnouncement.getAllPostedAnnouncement(limit, page).then((r) => {
        dispatch(getALLAnnouncement(r.payload));
      });
    }, 1500);
  };


  //Testing
  useEffect(() => {
    getPost.map((item) => (
      item.commentList.map((a) => ( 
        Math.max(a.comment.length)) ?
        // console.log("comment: ", a.comment) && console.log("UserComment: ",a.userCommentInfo.fullName) && console.log("Image: ", a.userCommentInfo.profileImage)
        ''
        : "No one commented yet!"
      )
    ))
  },[getPost])
  

  //React Post
  const handleReactPost = (e, id, numberOfLike) => {
    e.preventDefault();
    postService.reactPost(id).then((r) => dispatch(likePost(r.data))
    )
  };


  const filterSearch = !search ?
    filtered
  :
    filtered?.filter((g)=>
        // g?.userPostInfo?.fullName.toLowerCase().includes(search?.toLowerCase())
      
        g?.postInfo?.subject.toLowerCase().includes(search?.toLowerCase())
       
    ) 
  
    const handleSearch = (e) => { 
      setSearch(e.target.value) 

    };




  return (
    <div>
      <div className="block  max-w-full border-gray-200 sticky top-0 z-10">
        <div className="flex justify-between items-center p-2 lg:p-3 md:p-3 sm:p-3 bg-gray-100">
          <div>
            <div className="flex items-center justify-center">
              <img
                className="block sm:hidden h-8"
                src={chatlogo}
                alt="logo"
                onClick={() => {
                  navigate("/user/newfeed");
                }}
              />
              <h3 className="ml-1 text-lg font-bold">Announcement</h3>
            </div>
          </div>

          <div>
            <span className="flex items-center justify-end">
              <ProfileBarComponent />
            </span>
          </div>
        </div>

        <div className="grid-cols-2 pb-3  sm:grid-cols-2 md:grid-cols-2 lg:flex justify-between pt-4 bg-gray-100">
          <div className="w-full">
            <FilterComponent getPost={getPost} setFiltered={setFiltered} subject={subject} setSubject={setSubject}/>
          </div>

          {/* NarBar */}
          <div className="w-100 mx-2">
            <div className="flex w-full">
              <input
           
                onChange={handleSearch}
                type="search"
                className="input block p-2.5"
                placeholder="Search...."
                required
                value={search}
              />
              <button className="btn btn-primary" >
                <svg
                  className="lg:w-5 lg:h-5 md:w-5 md:h-5 w-4 h-4"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                  ></path>
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-12  md:grid-cols-12 lg:grid-cols-12 gap-2 mr-2 sm:mr-2 md:mr-0 lg:mr-0 mt-2">
        <div className=" mb-3 col-span-12 md:col-span-12 lg:col-span-9 ">
          {/*show post */}
          <div>
            <InfiniteScroll
              dataLength={getPost.length}
              next={fetchMoreData}
              hasMore={true}
              className="rounded-2xl"
              // style={{
              //   height: "400px", overflow: "auto"
              // }}
              height={800}
              loader={
                <div className="bg-transparent flex items-center justify-center">
                  <img src={loading} alt="loading" className="h-14 w-14" />
                </div>
              }
            >
              <div className="card overflow-hidden mb-3 bg-white no-scrollbar overflow-y-auto max-h-[100vh] ">
                <PostAnnouncementComponent />
              </div>
              {filterSearch.length > 0 ? filterSearch?.map((item, index) => (
                <div key={index} className="card bg-white mb-3">
                  <div className="flex justify-between px-3 lg:px-4 md:px-4 sm:px-3 pt-2">
                    <div className="flex flex-row items-center  py-3 pb-5 ">
                      {role==="ROLE_ADMIN"
                      ? 
                      <NavLink to={`/admin/profile-detail/${item?.userPostInfo?.uuid}`}>
                        <img
                          className="object-cover w-8 h-8 md:w-12 md:h-12 sm:w-12 sm:h-12 lg:w-12 lg:h-12  rounded-full shadow-lg mr-2"
                          src={item?.userPostInfo?.profileImage}
                          alt=""
                        />
                      </NavLink>

                      :
                      <NavLink to={`/admin/profile-detail/${item?.userPostInfo?.uuid}`}>
                        <img
                          className="object-cover w-8 h-8 md:w-12 md:h-12 sm:w-12 sm:h-12 lg:w-12 lg:h-12  rounded-full shadow-lg mr-2"
                          src={item?.userPostInfo?.profileImage}
                          alt=""
                        />
                      </NavLink>
                      }

                      <div className="text-left ml-2">
                        <div
                         
                          className="text-sm sm:text-base md:text-base lg:text-base font-semibold text-gray-900 dark:text-white"
                        >
                          {item?.userPostInfo?.fullName}
                        </div>

                        <div className="flex justify-start">
                          <small className="text-gray-500 dark:text-gray-400 text-xs mr-3">
                            {new Date(
                              item?.postInfo?.createdDate
                            ).toLocaleDateString()}
                          </small>

                          <img src={group} alt="" className="h-4 w-4" />
                        </div>
                      </div>
                    </div>
                    <div
                      className={`${
                        role === "ROLE_ADMIN" || role === "ROLE_TEACHER"
                          ? 
                          "block"
                          : 
                          "hidden"
                          } pb-5 z-1`}
                    >
                      <div className="dropdown dropdown-end">
                        <label tabIndex="0" className="m-1 cursor-pointer">
                          <img
                            src={droplist}
                            alt=""
                            className="w-1.5 sm:w-1.5 md:w-2 lg:w-2"
                          />
                        </label>
                        <ul
                          tabIndex="0"
                          className={`${
                            role === "ROLE_TEACHER" || role === "ROLE_ADMIN"
                              ? "block"
                              : "hiddem"
                          } dropdown-content p-2 shadow bg-bgmodal rounded-md w-[200px] mr-[20px]`}
                        >
                          <div
                            onClick={() =>
                              navigate(
                                `/edit-announcement/${item?.postInfo?.id}`,
                                {
                                  state: {
                                    subject: item?.postInfo?.subject,
                                    content: item?.postInfo?.content,
                                    annId: item?.postInfo?.groupId,
                                    image: item?.postInfo?.imageUrl,
                                    postId: item?.postInfo?.id,
                                  },
                                }
                              )
                            }
                            className="flex justify-center items-center hover:white bg-white p-2 rounded-lg mb-2 cursor-pointer"
                          >
                            <img src={update} alt="" />
                            <p className="ml-3 text-green-600">Edit</p>
                          </div>
                          <div>
                            <DeleteAnnouncementComponent
                              index={item?.postInfo?.id}
                            />
                          </div>
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div className="mx-4 sm:mx-4 md:mx-5 lg:mx-10 z-0">
                    <div>
                      <b>{item?.postInfo?.subject}</b>
                    </div>
                    <div className="text-xs sm:text-sm md:text-sm lg:text-sm text-left text-black-100">
                      {showMore
                        ? ReacHtmlPaser(item?.postInfo?.content)
                        : ReacHtmlPaser(
                            item?.postInfo?.content?.substring(0, 50)
                          )}
                      <button
                        onClick={() => setShowMore(!showMore)}
                        className="ml-3 text-purple-800 font-bold cursor-pointer"
                      >
                        {showMore ? "Show less" : "Show more"}
                      </button>
                    </div>

                    <div
                      className={`${
                        item?.postInfo?.imageUrl === null ||
                        item?.postInfo?.imageUrl === ""
                          ? "hidden"
                          : "block"
                      }`}
                    >
                      <div className=" w-[80%] mx-auto">
                        <ImageGallery
                          items={item?.postInfo?.imageUrl
                            ?.substring(1, item?.postInfo?.imageUrl?.length - 1)
                            .split(",")
                            .map((i) => {
                              return {
                                original: i,
                              };
                            })}
                          showPlayButton={false}
                          lazyLoad={true}
                          showFullscreenButton={false}
                          showNav={true}
                        />
                      </div>
                    </div>
                    <div className="flex justify-start  py-3">
                      <div className="flex justify-start mr-3 items-center">
                        {item?.numberOfLike % 2 !== 0 ? (
                          <img
                            src={like}
                            alt="like"
                            className="hover:cursor-pointer"
                            onClick={(e) =>
                              handleReactPost(e, item?.postInfo?.id)
                            }
                          />
                        ) : (
                          <img
                            src={unlike}
                            className="hover:cursor-pointer"
                            onClick={(e) =>
                              handleReactPost(e, item?.postInfo?.id)
                            }
                            alt="unlike"
                          />
                        )}
                        <p className="text-sm ml-2"> {item?.numberOfLike}</p>
                      </div>
                      <div className="flex justify-start items-center">
                        <img src={message} alt="" className="w-6 h-6" />
                        <p className="text-sm ml-1 ">
                          {" "}
                          {item?.numberOfComment}
                        </p>
                      </div>
                    </div>

                    {role==="ROLE_ADMIN" ? 
                    <div
                    className="p-2 cursor-pointer text-purple-700 text-left underline text-sm"
                    onClick={() =>
                      navigate(`/admin/post-detail/${item?.postInfo?.id}`, {state : {id:item?.postInfo?.id, commentList:item?.commentList , imageUrl:item?.postInfo?.imageUrl,numberOfLike:item?.numberOfLike, numberOfComment:item?.numberOfComment, content:item?.postInfo?.content, createdDatedate:item?.postInfo?.createdDate, fullName:item?.userPostInfo?.fullName,uuid:item?.userPostInfo?.uuid}})}
                  >

                  
                    View more detail
                  </div>

                    :

                    <div
                      className="p-2 cursor-pointer text-purple-700 text-left underline text-sm"
                      onClick={() =>
                        navigate(`/user/post-detail/${item?.postInfo?.id}`, {state : {id:item?.postInfo?.id, commentList:item?.commentList , imageUrl:item?.postInfo?.imageUrl,numberOfLike:item?.numberOfLike, numberOfComment:item?.numberOfComment, content:item?.postInfo?.content, createdDatedate:item?.postInfo?.createdDate, fullName:item?.userPostInfo?.fullName,uuid:item?.userPostInfo?.uuid}})}
                    >
                    
                      View more detail
                    </div>
                  

                  }

                    <div className="mb-3">
                      <Comments
                        key={index}
                        currentUserId={userInfo?.id}
                        postId={item?.postInfo?.id}
                        backendComments={item?.commentList}
                      />
                    </div>
                  </div>
                </div>
              )):<b>No data available</b> }
            </InfiniteScroll>
          </div>
          {/* end show post */}
        </div>

        <div className="sm:col-span-0 md:col-span-0 lg:col-span-3 pr-2 hidden md:hidden sm:hidden lg:block ">
          <div className="overflow-x-auto sticky xl:top-[14%] lg:top-[19%] z-0 ">
            <div className="mb-3">
              <DisplayGroupComponent />
            </div>

            {/* <div>
              <MostCommentComponent />
            </div> */}
          </div>
        </div>
      </div>
    </div>
  );
}
