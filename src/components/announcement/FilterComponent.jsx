import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'

export default function FilterComponent({getPost,setFiltered,subject,setSubject}) {

    useEffect(()=>{
        if(subject===""){
            setFiltered(getPost)
            return
        }
        const filtered = getPost.filter(
            (name)=>name.postInfo.subject.toLowerCase().includes(subject.toLowerCase()))

        setFiltered(filtered) 
     
    },[subject, getPost, setFiltered])



  return (
    // <div>
        <div className="flex justify-start px-2 bg-gray-100 ">
                  <button
                  onClick={()=>setSubject("")}
                  className={`${subject=== "" ? "bg-purple-500" : "bg-white"} py-1 px-2 flex items-center sm:px-4 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2 mb-2 text-xs md:text-sm lg:text-sm font-medium  text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200`}
             
                
                  >
                    All
                  </button>
                
                  <button
                   onClick={()=>setSubject("Monthly Result")}
                   className={`${subject=== "Monthly Result" || subject === "Monthly Party" ? "bg-purple-500" : "bg-white"} py-1 px-2 flex items-center sm:px-4 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2 mb-2 text-xs md:text-sm lg:text-sm font-medium  text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200`}
             
             
                  >
                    Monthly Result
                  </button>
                  <button
                     onClick={()=>setSubject("Sport Event")}
                     className={`${subject=== "Sport Event" ? "bg-purple-500" : "bg-white"} py-1 px-2 flex items-center sm:px-4 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2 mb-2 text-xs md:text-sm lg:text-sm font-medium  text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200`}
             
                    
                  >
                    Sport Event
                  </button>
                </div>
    // </div>
  )
}
