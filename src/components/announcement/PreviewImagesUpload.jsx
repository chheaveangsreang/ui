import React from "react";

export default function PreviewImagesUpload({ imagePre }) {
  return (
    <div className="grid xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 mt-2.5 gap-3 container">
      {imagePre?.map((url) => {
        return (
          <img
            alt=""
            className="mb-3 w-48 h-48 object-cover xl:mx-auto lg:mx-auto md:mx-auto sm:mx-auto"
            src={url}
          />
        );
      })}
    </div>
  );
}
