import React, { useEffect, useState } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import PreviewImagesUpload from "./PreviewImagesUpload";
import postService from "../../service/postService";
import groupService from "../../service/groupService";
import { getAllGroup } from "../../redux/slices/groupSlice";
import { getUserInfo } from "../../utils/crypto";
import { createPost } from "../../redux/slices/announcementSlice";

export default function PostAnnouncementComponent({ ...props }) {
  const [role, setRole] = useState("");

  // Get Group Announcements
  const dispatch = useDispatch();

  const groups = useSelector((state) => state.group.value);

  const [groupId, setGroupId] = useState();
  const [message, setMessage] = useState()

  useEffect(() => {
    groupService.getAllGroup(20, 1).then((r) => dispatch(getAllGroup(r)));
  }, []);

  const HandleOnChangeGroup = (e) => {
    setGroupId(e.target.value);
  };

  const userInfo = getUserInfo();
  //All data that require to post
  const [subject, setSubject] = useState("");
  const [content, setContent] = useState("");
  const [imageUrl, setImageUrl] = useState("");

  //Handle adding subject
  const handleOnchangeSubject = (e) => {
    setSubject(e.target.value);
  };



  //Handle adding everything in CK editor
  const handleOnchangeContent = (e, editor) => {
    const data = editor.getData();
    setContent(data);
  };

  //  Upload Multiple Images
  const [imagePre, setImagePre] = useState([]);
  //Handle Image Change
  const handleMultipleImage = (e) => {
    setImageUrl(e.target.files);
    const selectedFiles = [];
    const targetFiles = e.target.files;
    const targetFilesObject = [...targetFiles];
    targetFilesObject.map((file) => {
      return selectedFiles.push(URL.createObjectURL(file));
    });
    setImagePre(selectedFiles);
  };
  //Handle upload multiple images and post announcement
  const uploadMultipleFiles = async (e) => {
    e.preventDefault();
    let imageUpload = [];
    if (imageUrl) {
      const formData = new FormData();
      for (let i = 0; i < imageUrl.length; i++) {
        formData.append("files", imageUrl[i]);
      }
      await postService
        .uploadMultipleFile(formData)
        .then((r) => (imageUpload = r.data));
    }
    const postData = {
      groupId: groupId,
      subject: subject,
      content: content,
      imageUrl: imageUpload?.map((item) => item?.fileUrl) ?? "[]",
    };
    console.log("postdata", postData);
    postService.createPost(postData).then((r) => {
      console.log("r post: ",r)
      dispatch(createPost(r.data.payload));
      setImageUrl([]);
      if(r.success===true){
        setMessage("Upload successfully")
      }
    });
    document.getElementById('subject').value = ""
    setImagePre(null)
    setContent('')

  };


  console.log("gorupn post: ",groups)

  function uploadAdapter(loader) {
    return {
      upload: () => {
        return new Promise((resolve, reject) => {
          loader.file.then((file) => {
            //Single file
            // var formData = new FormData();
            // formData.append('file', file)
            // formData.append("destination", "files");

            //Multiple files
            var formData = new FormData();
            formData.append("file", file);
            formData.append("destination", "files");

            const config = {
              headers: {
                "content-type": "multipart/form-data",
              },
            };

            postService
              .uploadFile(formData, config)
              .then((response) => {
                console.log("Response: ", response);
                resolve({
                  default: `${response?.data?.fileUrl}`,
                });
              })
              .catch((error) => {
                reject(error);
              });
          });
        });
      },
    };
  }
  function uploadPlugin(editor) {
    editor.plugins.get("FileRepository").createUploadAdapter = (loader) => {
      return uploadAdapter(loader);
    };
  }
  useEffect(() => {
    setRole(userInfo?.roles[0].name);
  }, []);
  return (
    <div
      className={`${
        role === "ROLE_TEACHER" || role === "ROLE_ADMIN" ? "block" : "hidden"
      }  p-3 bg-white`}
    >
      <form>
        <div className="grid-cols-2  sm:grid-cols-2 md:flex lg:flex justify-between gap-2">
          <div className="basis-[40%] pt-2">
            <input
              id="subject"
              onChange={handleOnchangeSubject}
              required
              placeholder="Write subject here ...."
              type="text"
              className="input input-bordered max-w-xs px-3 text-xs sm:text-sm md:text-sm lg:text-sm py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none block w-full rounded-md focus:ring-1 mt-0.4"
            />
          </div>
          <div className="basis-[40%] pt-2">
            <select
            
            onChange={HandleOnChangeGroup}
            class="select w-full max-w-xs h-8">
              <option disabled selected>
                Pick your group
              </option>
              {groups?.map((g) => {
                return (
                  <option value={g?.id} key={g?.id}>
                    {g?.groupName}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="bg-grey-lighter basis-[20%] pt-2">
            <label for="multiple_files" class="btn btn-square btn-outline h-12 w-36">
            <svg
                className="w-8 h-8"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
              </svg>
            </label>
            <input
              type="file"
            
              id="multiple_files"
              multiple
              class="hidden w-full text-sm text-slate-500
                      file:mr-4 file:py-2 file:px-4 file:mt-2.5
                      file:rounded-full file:border-0
                      file:text-sm file:font-semibold
                      file:bg-violet-250 file:text-violet-800
                      hover:file:bg-violet-100"
              onChange={handleMultipleImage}
            />
            
          </div>
        </div>
        <div className="text-xs sm:text-sm md:text-sm lg:text-sm">
          <div>
            <PreviewImagesUpload imagePre={imagePre}/>
          </div>
          <CKEditor
        
            config={{
              placeholder: "Write content here...",
              removePlugins: ["EasyImage", "ImageUpload", "MediaEmbed"],
              extraPlugins: [uploadPlugin],
            }}
            editor={ClassicEditor}
            onReady={(editor) => {}}
            onBlur={(event, editor) => {}}
            onFocus={(event, editor) => {}}
            onChange={handleOnchangeContent}
            {...props}
          />
        </div>
        <div className="mt-2 text-right text-sm sm:text-sm md:text-base lg:text-base ">
          <button
            // onClick={handleOnPostAnnouncement}
            onClick={uploadMultipleFiles}
            className="btn btn-sm bg-purple-800 hover:bg-purple-800 outline-none border-0"
          >
            Post
          </button>
        </div>
      </form>
    </div>
  );
}
