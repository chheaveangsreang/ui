import React, { useEffect, useState } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { useSelector } from "react-redux";
import { getUserInfo } from "../../utils/crypto";
import groupService from "../../service/groupService";
import { getAllGroup } from "../../redux/slices/groupSlice";
import { useDispatch } from "react-redux/es/exports";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import postService from "../../service/postService";

export default function EditAnnouncementComponent({ ...props }) {
  const groups = useSelector((state) => state.group.value);
  const [role, setRole] = useState("");
  const navigate = useNavigate();
  const userInfo = getUserInfo();
  const dispatch = useDispatch();
  const location = useLocation();
  const [subject,setSubject] = useState(location.state.subject)
  const [content,setContent] = useState(location.state.content)
  const [imageUrl,setImageUrl] = useState(location.state.image)
  const {id} = useParams();
  useEffect(()=>{
    setRole(userInfo.roles[0].name)
  },[userInfo])
  useEffect(() => {
    groupService.getAllGroup(20, 1)
      .then((r) => dispatch(getAllGroup(r)));
  }, [dispatch]);

  
  const [groupId,setGroupId] = useState(id)
  
  const handleOnChange = (e) => {
    setGroupId(e.target.value)
  }

  const handleOnchangeSubject = (e) => {
    setSubject(e.target.value);
  }
  const handleOnChangeContent = (e,editor) => {
    const data = editor.getData();
    setContent(data);
  }
  const [imagePre,setImagePre] = useState([])
  const [msg,setMsg]=useState("")
  
  useEffect(() => {
    setImagePre(imageUrl?.substring(1, imageUrl?.length - 1)
  .split(","))
  },[])
  

  const handleMultipleImage = (e) => {
    setImageUrl(e.target.files);
    const selectedFiles = [];
    const targetFiles = e.target.files;
    const targetFilesObject = [...targetFiles];
    targetFilesObject.map((file) => {
      return selectedFiles.push(URL.createObjectURL(file));
    });
    setImagePre(selectedFiles);
  };

  const handleEditPost = async (e) => {
    e.preventDefault();
    let imageUpload = [];
    if (imageUrl) {
      const formData = new FormData();
      for (let i = 0; i < imageUrl.length; i++) {
        formData.append('files', imageUrl[i]);
      }
      await postService.uploadMultipleFile(formData).then(r => imageUpload = r.data)
    }
    const postData = {
      groupId: groupId,
      subject: subject,
      content: content,
      imageUrl: imageUpload?.map((item) => item?.fileUrl) ?? "[]",
    }
    
    await postService.updatePost(location.state.postId,postData);
    navigate('/user/newfeed')

  }
  return (
    
    <div
      className={`${
        role === "ROLE_TEACHER" || role === "ROLE_ADMIN" ? "block" : "hidden"
      } p-3 bg-white border-b-[10px] pt-5 container mx-auto`}
    >
      <div>
        <p className="text-lg sm:text-lg md:text-xl lg:text-xl font-semibold mb-2">
          Edit Announcement
        </p>
      </div>
      <form>
        <div className="mb-2">
                  {msg && (
                    <label>
                      <div className="p-2 text-center w-full bg-red-200 text-red-600 rounded-lg">
                          {msg}
                      </div>

                    </label>
                  )}
                </div>
        <div className="flex mb-2 gap-2">
          <div className="basis-[50%] pt-2">
            <input
              onChange={handleOnchangeSubject}
              value={subject}
              required
              placeholder="Write subject here ...."
              type="text"
              className="px-3 text-xs sm:text-sm md:text-sm lg:text-sm py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none block w-full rounded-md focus:ring-1"
            />
          </div>
          <div className="basis-[50%] pt-2">
            <select
              onChange={handleOnChange}
              required
              value={groupId}
              className="text-xs sm:text-sm md:text-base lg:text-base   px-3 py-2 w-full bg-white border shadow-sm border-slate-300 placeholder-purple-400 focus:outline-none block  rounded-md focus:ring-1"
            >
              <option disabled hidden>
                Choose group
              </option>
              {groups?.map((g) => (
               
              <option selected={g?.id === location.state.annId ? true : false} value={g?.id} key={g?.id}>{g?.groupName}</option>
              
              
              ))}
            </select>
          </div>
          <div className="bg-grey-lighter basis-[20%] pt-2">
            <label className="p-0.5 flex flex-col items-center px-2  bg-white text-blue rounded-lg  tracking-wide border-[1.5px] border-gray-300 cursor-pointer hover:bg-purple-700 hover:text-white">
              <svg className="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
              </svg>
              <input type='file' className="hidden" onChange={handleMultipleImage} id="multiple_files" multiple />
            </label>
          </div>
        </div>
        <div className="grid xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 mt-2.5 gap-3 container">
      {imagePre.map((url) => {
        console.log('url; ',url)
        return (
          
          <img
          alt=""
            className="mb-3 w-48 h-48 object-cover xl:mx-auto lg:mx-auto md:mx-auto sm:mx-auto"
            src={url}
          />
        );
      })}
    </div>
        <div className="w-[99%] sm:w-[99%] md:w-[100%] lg:w-[100%]">
          <CKEditor

            data = {content}
            config={{ placeholder: "Write content here..." ,
              
              removePlugins: ["EasyImage","ImageUpload","MediaEmbed"],
          }}
            editor={ClassicEditor}
            onReady={(editor) => {}}
            onBlur={(event, editor) => {}}
            onFocus={(event, editor) => {}}
            onChange={(handleOnChangeContent)} 
             
            {...props}
          />
        </div>

        <div className="mt-2 text-right text-sm sm:text-sm md:text-base lg:text-base ">
          <button onClick={handleEditPost} className=" text-[10px] text-xs sm:text-sm bg-[#5A46AA] md:text-sm lg:text-sm hover:bg-purple-300 hover:text-[#101010] font-semibold text-white py-2 px-8 border border-black-200 rounded-md">
            Save
          </button>
        </div>
      </form>
    </div>
  );
}
