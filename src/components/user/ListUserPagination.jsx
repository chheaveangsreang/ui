import React, { useEffect } from 'react'
import ReactPaginate from 'react-paginate'
import { useSelector, useDispatch } from 'react-redux';
import { listUserGneration } from '../../redux/slices/UserPerGenerationSlice';
import UserPerGenerationService from '../../service/UserPerGenerationService';

export default function ListUserPagination({setPage,data}) {


  console.log("All group: ",data?.payload?.length)

  const handlePageClick = ({selected}) => {   
    setPage(selected+1);
    console.log("selected",selected)
  }

    const pageSize = Math.ceil(data?.payload?.length/10);
    console.log("pagesize:",pageSize)
  return (
    
    <div>
      {data?.payload?.length > 0 ?
      <ReactPaginate
        previousLabel={"Prev"}
        nextLabel={"Next"}
        pageCount={pageSize}
        onPageChange={handlePageClick}
        containerClassName={"paginationButton"}
        previousLinkClassName={"paginationButtonPrev"}
        nextLinkClassName={"paginationButtonNext"}
        activeClassName={"onActive"} 
      /> : ''}
    </div>
  )
}
