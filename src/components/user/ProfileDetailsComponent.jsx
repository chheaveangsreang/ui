import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import header from "../../assets/images/header setting.png";
import camera from "./../../assets/icons/camera.svg";
import { useDispatch, useSelector } from "react-redux";
import api from "../../utils/api";

import editProfileUser from "../../redux/slices/groupChatSlice";
import profileDetailService from "../../service/profileDetailService";
import { getUserInfo } from "../../utils/crypto";
import ProfileBarComponent from "./ProfileBarComponent";
export default function ProfileDetailsComponent() {
  const { id } = useParams();
  const userInfo = getUserInfo();
  const [data, setData] = useState({});

  const profileUser = useSelector((state) => state.profile.value);
  const [image, setImage] = useState();
  const [imageUrl, setImageUrl] = useState("");
  const [fullName, setFullName] = useState();
  const [email, setEmail] = useState();
  const [gender, setGender] = useState();
  const [dob, setDob] = useState();
  const [role, setRole] = useState("");
  const [generation, setGeneration] = useState([]);

  const [success, setSuccess] = useState("");

  const dispatch = useDispatch();

  useEffect(() => {
    profileDetailService.getProfileUserById(id).then((r) => setData(r)); //dispatch(getProfileUser(r))
  }, [dispatch, id]);

  useEffect(() => {
    setFullName(data?.payload?.fullName);
    setGender(data?.payload?.gender);
    setDob(data?.payload?.dob);
    setImage(data?.payload?.profileImage);
    setEmail(data?.payload?.email);
    setGeneration(data?.payload?.generation?.generationName);
    setRole(userInfo?.roles[0].name);
  }, [data]);

  const handleEdit = (e) => {
    e.preventDefault();
    const data = {
      uuid: id,
      fullName: fullName,
      profileImage: image,
      dob: dob,
      gender: gender,
    };

    profileDetailService.editProfileUser(data).then((r) => {
      dispatch(editProfileUser(r));
      if (r.success === true) {
        setSuccess("You have updated your profile successfully");
      }
    });

    onDisable();
  };

  const fileChange = (e) => {
    // const objectUrl = URL.createObjectURL(e.target.files[0]);
    // setImageUrl(objectUrl);

    const formData = new FormData();
    formData.append("file", e.target.files[0]);
    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };
    api
      .post("files/", formData, config)
      .then((res) => setImage(res.data.fileUrl));
  };

  const [disable, setDisable] = useState(true);

  const onDisable = () => {
    setDisable(!disable);
  };

  console.log("data user: ", data, fullName, email);

  return (
    <div className="container mx-auto">
      <div className="block  max-w-full border-gray-100 pt-2">
        <div className="items-center  p-0 lg:p-3 md:p-3 sm:p-3 flex justify-between">
          <h3 className=" lg:text-3xl md:text-lg sm:text-base text-base  sm:block md:block lg:block  font-bold text-left col-span-4 sm:col-span-4 md:col-span-3 lg:col-span-4">
            Profile Detail
          </h3>

          <div>
            <ProfileBarComponent />
          </div>
        </div>
      </div>

      <div className="pt-5 p-0 lg:p-3 md:p-3 sm:p-3 mr-2 sm:mr-2 md:mr-0 lg:mr-0">
        <div>
          <div className="grid grid-rows-2">
            <div>
              <div>
                <img src={header} alt="" className="bg-white" />
              </div>

              <div className="bg-white lg:px-10 md:px-10 sm:px-8 px-2 top-[140px] w-full lg:pl-14 md:pl-5">
                <div className="flex flex-row items-center">
                  <div className="relative py-2 ">
                    <img
                      className=" object-contain w-[50px] h-[50px] md:w-[100px] md:h-[100px] sm:w-[80px] sm:h-[80px] lg:w-[120px] lg:h-[120px] outline outline-4 outline-purple-400 rounded-full shadow-lg mr-10"
                      src={image || ""}
                      alt=""
                    />
                    <span className="bg-purple-700 bottom-[8px] p-[2px] md:p-2 sm:bottom-[10px] lg:bottom-1 md:bottom-[9px] sm:p-1 rounded-full absolute">
                      <label htmlFor="multiple_files">
                        <img
                          src={camera}
                          alt=""
                          className="cursor-pointer object-cover h-4 w-4 lg:h-5 lg-w-5 md:h-5 md:w-5 sm:h-5 sm:w-5 text-white"
                        />
                        <input
                          onChange={fileChange}
                          id="multiple_files"
                          type="file"
                          className="hidden"
                        />
                      </label>
                    </span>
                  </div>
                  <div className="ml-2">
                    <h5 className="text-sm sm:text-sm md:text-xl lg:text-2xl font-semibold text-gray-900 text-left dark:text-white px-0 py-0 ">
                      {fullName}
                    </h5>
                    <div className="flex justify-between">
                      <p className="text-xs sm:text-sm md:text-sm lg:text-base lg:font-bold text-left ">
                        Your account is ready, you can now apply for advice.
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              {/* edit profile */}
              <div className="mt-5 bg-white  px-5 lg:px-5 md:px-5 sm:px-5">
                <div className="grid grid-cols-12 lg:divide-x-2 md:divide-x-0">
                  <div className="lg:mx-auto pb-5 lg:pl-0 md:pl-0 pl-5 col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-2  text-left divide-x-2 flex justify-end items-start">
                    <div className={id === userInfo?.uuid ? "block" : "hidden"}>
                      <button
                        onClick={onDisable}
                        className="text-[10px]   mt-3 text-xs sm:text-sm   md:text-sm lg:text-sm  mb-4 bg-transparent  text-black-700 font-semibold  py-2 px-3 border border-blue-800 lg:mr-3 rounded-md"
                      >
                        <div className="flex justify-evenly items-center">
                          <div>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="h-4 w-4 mr-1 text-blue-800"
                              fill="none"
                              viewBox="0 0 24 24"
                              stroke="currentColor"
                              strokeWidth={2}
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"
                              />
                            </svg>
                          </div>
                          <span className="hidden sm:hidden md:block lg:block">
                            Edit Profile
                          </span>
                        </div>
                      </button>
                    </div>
                  </div>

                  <div className="mt-3 col-span-12 sm:col-span-12 md:col-span-12 lg:col-span-10 lg:pl-5 md:pl-0 sm:pl-0 pl-0">
                    <form>
                      <div className="mb-2">
                        {success && (
                          <label>
                            <div className="p-2 text-center w-full bg-green-200 text-green-600 rounded-lg">
                              {success}
                            </div>
                          </label>
                        )}
                      </div>
                      <div>
                        {/* <div className="flex justify-between py-3">
                        <h1 className="lg:text-xl md:text-lg sm:text-base text-sm font-semibold text-slate-400">
                          Edit Profile
                        </h1>
                        <h1 className="text-slate-400 text-xs sm:text-sm md:text-base lg:text-base">
                          Last update August 1
                        </h1>
                      </div> */}
                        <div className="grid grid-cols-2 items-center gap-0 lg:gap-10 md:gap-0">
                          <div className="md:col-span-2 lg:col-span-1 sm:col-span-2 col-span-2">
                            <label className="block mb-4">
                              <span className="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium focus:outline-none focus:border-sky-500 focus:ring-sky-500 text-slate-700 text-left">
                                FullName
                              </span>
                              <input
                                disabled={disable}
                                value={fullName}
                                onChange={(e) => setFullName(e.target.value)}
                                type="text"
                                name="name"
                                required
                                className="disabled:bg-gray-100 text-xs sm:text-sm md:text-base lg:text-base mt-1 px-3 py-2 bg-white border shadow-sm border-slate-300  block w-full rounded-md focus:ring-1"
                              />
                            </label>
                            <label className="block mb-4">
                              <span className="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700 text-left">
                                Date of Birth
                              </span>
                              <input
                                disabled={disable}
                                value={dob || ""}
                                onChange={(e) => setDob(e.target.value)}
                                type="date"
                                name="dob"
                                required
                                className="disabled:bg-gray-100 text-xs sm:text-sm md:text-base lg:text-base mt-1 px-3 py-2 bg-white border shadow-sm border-slate-300 focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md focus:ring-1"
                                placeholder="Date of Birth"
                              />
                            </label>
                            <label className="block mb-4">
                              <span className="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700 text-left">
                                Role
                              </span>
                              <select
                                disabled={true}
                                value={role || ""}
                                onChange={(e) => setRole(e.target.value)}
                                required
                                id="role"
                                className="disabled:bg-gray-100 text-xs sm:text-sm md:text-base lg:text-base mt-1 mb-1 px-3 py-2 w-full bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  rounded-md focus:ring-1"
                              >
                                <option value={userInfo?.roles[0]?.id}>
                                  {role}
                                </option>
                              </select>
                            </label>
                          </div>
                          <div className="md:col-span-2 lg:col-span-1 sm:col-span-2 col-span-2">
                            <label className="block mb-4">
                              <span className="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700 text-left">
                                Email
                              </span>
                              <input
                                value={email || ""}
                                onChange={(e) => setEmail(e.target.value)}
                                disabled={true}
                                type="email"
                                name="email"
                                required
                                className="disabled:bg-gray-100 text-xs sm:text-sm md:text-base lg:text-base mt-1 px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md  focus:ring-1"
                              />
                            </label>
                            <label className="block mb-4">
                              <span className="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700 text-left">
                                Gender
                              </span>

                              <select
                                onChange={(e) => setGender(e.target.value)}
                                id="gender"
                                name="gender"
                                disabled={disable}
                                className="disabled:bg-gray-100 px-3 py-2 text-xs w-full  bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  rounded-md sm:text-sm focus:ring-1"
                                defaultValue={""}
                              >
                                <option disabled hidden value="">
                                  Gender
                                </option>
                                <option
                                  selected={gender === "male" ? true : false}
                                  value="male"
                                >
                                  Male
                                </option>
                                <option
                                  selected={gender === "female" ? true : false}
                                  value="female"
                                >
                                  Female
                                </option>
                              </select>
                            </label>
                            <label className="block mb-4">
                              <span className="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700 text-left">
                                Generation
                              </span>
                              <select
                                disabled={true}
                                id="generation"
                                value={generation || ""}
                                onChange={(e) => setGeneration(e.target.value)}
                                required
                                className="disabled:bg-gray-100 text-xs  md:text-base lg:text-base mt-1 mb-1 px-3 py-2 w-full bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  rounded-md sm:text-sm focus:ring-1"
                              >
                                <option
                                  value={profileUser?.generation?.id}
                                  defaultValue
                                >
                                  {generation}
                                </option>
                              </select>
                            </label>
                          </div>
                          <div className="text-left text-sm sm:text-sm md:text-base lg:text-base ">
                            <button
                              type="submit"
                              disabled={disable}
                              onClick={(e) => handleEdit(e)}
                              className="disabled:bg-purple-400 text-[10px] text-xs sm:text-sm bg-purple-800 md:text-sm lg:text-sm  mb-4 hover:bg-purple-900 text-black-700 font-semibold text-white py-2 px-8 border border-black-200 rounded-md"
                            >
                              Save
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
