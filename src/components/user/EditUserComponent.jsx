import React, { Fragment, useEffect, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import update from "./../../assets/icons/update.svg";
import { useDispatch, useSelector } from "react-redux";
import userManagementService from "../../service/userManagementService";
import UserPerGenerationService from "../../service/UserPerGenerationService";
import { editUserGeneration } from "../../redux/slices/UserPerGenerationSlice";
export default function EditUserComponent({ index }) {

  let [isOpen, setIsOpen] = useState(false);
  const [roles, setRole] = useState([]);
  const [course, setCourse] = useState([]);
  const data = index;
  const [formData, setFormData] = useState(
    {
      userId: index.id,
      generationId: index.generation.id,
      courseId: index.course.id,
      roleId: index.roles[0].id
    }
  );
  const dispatch = useDispatch();

  const generationUser = useSelector((state) => state.generation.value);

  useEffect(() => {
    userManagementService.getAllCourse().then(res => setCourse(res.payload))
    userManagementService.getAllRoles().then(res => setRole(res.payload))
  }, []);

  const handleEdit = () => {
    if (formData) {
      UserPerGenerationService.editUserByGeneration(formData).then(
        (r) => {
          dispatch(editUserGeneration(r.payload))
          if(r.success===true){
            setIsOpen(false)
          }else{
            setIsOpen(true)
          }
        })
     
    }
  }

  const handleOnChange = (e) => {
    setFormData((pre) => ({
      ...pre,
      [e.target.name]: e.target.value,
    }));
  }


  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  return (
    <>
      <li onClick={openModal}>
        <div className="flex justify-center items-center w-full hover:bg-green-300 bg-green-200 p-2 rounded-lg mb-2 cursor-pointer">
          <img src={update} alt="" />
          <p className="ml-3 text-green-600">Edit</p>
        </div>
      </li>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={openModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto ">
            <div className="flex min-h-full items-center justify-center text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className=" max-w-[80%]  bg-white md:max-w-[80%] sm:max-w-[80%] lg:max-w-[700px] transform overflow-hidden rounded-lg  py-5 px-5 w-full  text-left align-middle shadow-xl transition-all overflow-y-auto">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900"
                  >
                    <p className="text-lg sm:text-lg md:text-2xl lg:text-2xl font-semibold text-black text-center mb-3">
                      Edit User
                    </p>
                  </Dialog.Title>

                  <div className="grid-cols-2   sm:flex md:flex   lg:flex justify-between  gap-3  ">
                    <div className=" mt-3  rounded-lg  w-full max-w-[30%] mb-2">
                      <img
                        src={data?.profileImage}
                        alt=""
                        className="object-cover"
                      />
                    </div>

                    <div className=" mt-3   rounded-lg w-full">
                      <div>
                        <select
                          required
                          name="roleId"
                          onChange={handleOnChange}
                          className="text-sm sm:text-sm md:text-base lg:text-base p-2.5   px-3  w-full bg-white border shadow-sm border-slate-300 placeholder-purple-400 focus:outline-none block  rounded-md focus:ring-1"
                        >

                          {roles?.map((item, key) => {
                            return (
                              <option key={key}
                                defaultValue={data?.roles[0].id}
                                selected={item.name === data?.roles[0].name ? true : false}
                                value={item.id}>
                                {item.name}
                              </option>
                            )
                          })}

                        </select>
                      </div>
                      <div className="mt-2">
                        <select
                          required
                          name="courseId"
                          onChange={handleOnChange}
                          className=" text-sm sm:text-sm md:text-base lg:text-base p-2.5   px-3  w-full bg-white border shadow-sm border-slate-300 placeholder-purple-400 focus:outline-none block  rounded-md focus:ring-1"
                        >
                          {course?.map((item, key) => {
                            return (
                              <option key={key}
                                defaultValue={data?.course?.id}
                                selected={item.courseName === data?.course?.courseName ? true : false}
                                value={item.id}>
                                {item.courseName}
                              </option>
                            )
                          })}

                        </select>
                      </div>
                      <div className="mt-2">
                        <select
                          onChange={handleOnChange}
                          name="generationId"
                          required
                          className="text-sm sm:text-sm md:text-base lg:text-base p-2.5   px-3  w-full bg-white border shadow-sm border-slate-300 placeholder-purple-400 focus:outline-none block  rounded-md focus:ring-1"
                        >

                          {generationUser?.map((item, key) => {
                            return (
                              <option
                                defaultValue={generationUser?.payload?.id}
                                selected={item.id === data?.generation?.id ? true : false}
                                key={key}
                                value={item.id}>
                                {item.generationName}
                              </option>
                            );
                          })}
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="flex justify-end mt-3 ">
                    <div>
                      <button
                        onClick={closeModal}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2  text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-red-800  rounded-md "
                      >
                        Cancel
                      </button>
                    </div>
                    <div>
                      <button
                        onClick={handleEdit}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2   text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-blue-800 rounded-md"
                      >
                        OK
                      </button>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
