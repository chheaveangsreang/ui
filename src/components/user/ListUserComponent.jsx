import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import menu from "../../../src/assets/icons/menu.svg";
import inf from "../../../src/assets/images/img-not-found.jpg";
import { getAllGeneration } from "../../redux/slices/generationSlice";
import generationService from "../../service/generationService";
import UserPerGenerationService from "../../service/UserPerGenerationService";
import DeleteUserComponent from "./DeleteUserComponent";
import EditUserComponent from "./EditUserComponent";
import ListUserPagination from "./ListUserPagination";
import ProfileBarComponent from "./ProfileBarComponent";


export default function ListUserComponent() {


  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const [search, setNewSearch] = useState("");
  
  const navigate = useNavigate()

  const generation = useSelector((state) => state.generation.value);
  const [page,setPage] = useState(1)
  const handleOnChange = (e) => {
    UserPerGenerationService.listUserByGeneration(e.target.value,10,page).then((r) =>{
      setData(r)
    });
    console.log("id:",e.target.value)
  };


  const handleSearchChange = (e) => {
    setNewSearch(e.target.value);
  };
  
  useEffect(() => {
    generationService
      .getAllGeneration(10, 1)
      .then((res) => dispatch(getAllGeneration(res)));
  }, [dispatch]);

  console.log("generation: ",generation)
  console.log(" data generation: ",data)
 


  useEffect(() => {
    // UserPerGenerationService.listUserByGeneration();
  });

  return (
    <div>
      <div className="bg-gray-100 mt-0 pt-0 h-screen">
        <div className="block  max-w-full border-gray-200 sticky top-0 mt-0 pt-0 bg-gray-100">
          <div className="grid grid-flow-row-dense md:grid-cols-12 grid-cols-6 lg:grid-cols-12  items-center  lg:p-3 md:p-3 p-3">
            <h3 className="whitespace-nowrap lg:text-3xl md:text-2xg sm:text-xl text-xl  sm:block md:block lg:block  font-bold text-left col-span-2  md:col-span-3 lg:col-span-6">
              User
            </h3>
            <div className="lg:col-span-6 md:col-span-9 col-span-4 justify-end">
              <ProfileBarComponent/>
            </div>
          </div>

          <div className="flex justify-between  items-center lg:p-3 md:p-3 p-3">
            <div className="whitespace-nowrap w-[50%] lg:text-3xl md:text-lg sm:text-sm text-xs    font-bold text-left ">
              
                <div className="relative w-full">
                  <input
                    type="search"
                    value={search}
                    onChange={handleSearchChange}
                    className="block p-2 lg:p-2.5 md:p-2.5 w-full  lg:text-sm md:text-sm text-xs text-gray-900 bg-white rounded-lg border  focus:outline-none"
                    placeholder="Search...."
                    required
                  />
                  <button
                   
                    className="absolute top-0 right-0 p-2 lg:p-2.5 md:p-2.5 text-sm font-medium text-white bg-blue-700 rounded-r-lg border border-blue-700  focus:ring-4 focus:outline-none  "
                  >
                    <svg
                      className="lg:w-5 lg:h-5 md:w-5 md:h-5 w-4 h-4"
                      fill="none"
                      stroke="currentColor"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                      ></path>
                    </svg>
                  </button>
                </div>
              
            </div>

            <div className="relative pr-0  flex justify-end">
              <div className="flex justify-end items-center">
                <select
                  onChange={handleOnChange}
                  defaultValue={""}
                 className="block p-2 w-full  text-xs sm:text-sm md:text-sm lg:text-base text-gray-900 bg-white rounded-lg focus:outline-none">
                  <option disabled hidden value="">
                    Choose a generation
                  </option>
                  {generation?.map((item, key) => {
                    return (
                      <option key={key} value={item.id}>
                        {item.generationName}
                      </option>
                    );
                  })}
                </select>
              </div>
            </div>
          </div>
        </div>

        <div className="p-3 bg-gray-100 ">
          <div className="overflow-y-auto h-[80vh]">
            <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 divide-y-2">
              <thead className="text-xs text-gray-700 uppercase dark:bg-gray-700 dark:text-gray-400">
                <tr>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base "
                  >
                    No
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px]  sm:text-sm md:text-sm lg:text-base items-center"
                  >
                    <div className="flex items-center">
                      <span>Name</span>
                      <span className="ml-2">
                        <svg
                          width="9"
                          height="7"
                          viewBox="0 0 9 7"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M3.4083 6.74155C3.3677 6.702 3.1941 6.55265 3.0513 6.41354C2.1532 5.59795 0.6832 3.47034 0.2345 2.35675C0.1624 2.18763 0.0098 1.76006 0 1.53161C0 1.31271 0.0504 1.10404 0.1526 0.90492C0.2954 0.656698 0.5201 0.457574 0.7854 0.348466C0.9695 0.278227 1.5204 0.169118 1.5302 0.169118C2.1329 0.0600097 3.1122 0 4.1944 0C5.2255 0 6.1649 0.0600097 6.7767 0.149342C6.7865 0.159571 7.4711 0.26868 7.7056 0.388018C8.134 0.606917 8.4 1.03449 8.4 1.49206V1.53161C8.3895 1.82962 8.1235 2.45631 8.1137 2.45631C7.6643 3.50989 6.2664 5.58841 5.3375 6.42377C5.3375 6.42377 5.0988 6.65904 4.9497 6.76132C4.7355 6.9209 4.4702 7 4.2049 7C3.9088 7 3.633 6.91067 3.4083 6.74155Z"
                            fill="#4E5A69"
                          />
                        </svg>
                      </span>
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base "
                  >
                    <span className="">Gender</span>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base "
                  >
                    <span>Created By </span>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base"
                  >
                    <div className="flex items-center">
                      <span>Date</span>
                      <span className="ml-2 ">
                        <svg
                          width="9"
                          height="7"
                          viewBox="0 0 9 7"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M3.4083 6.74155C3.3677 6.702 3.1941 6.55265 3.0513 6.41354C2.1532 5.59795 0.6832 3.47034 0.2345 2.35675C0.1624 2.18763 0.0098 1.76006 0 1.53161C0 1.31271 0.0504 1.10404 0.1526 0.90492C0.2954 0.656698 0.5201 0.457574 0.7854 0.348466C0.9695 0.278227 1.5204 0.169118 1.5302 0.169118C2.1329 0.0600097 3.1122 0 4.1944 0C5.2255 0 6.1649 0.0600097 6.7767 0.149342C6.7865 0.159571 7.4711 0.26868 7.7056 0.388018C8.134 0.606917 8.4 1.03449 8.4 1.49206V1.53161C8.3895 1.82962 8.1235 2.45631 8.1137 2.45631C7.6643 3.50989 6.2664 5.58841 5.3375 6.42377C5.3375 6.42377 5.0988 6.65904 4.9497 6.76132C4.7355 6.9209 4.4702 7 4.2049 7C3.9088 7 3.633 6.91067 3.4083 6.74155Z"
                            fill="#4E5A69"
                          />
                        </svg>
                      </span>
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base "
                  >
                    <span className=""> Role </span>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px]  sm:text-sm md:text-sm lg:text-base "
                  >
                    Course Types
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base "
                  >
                    Generation
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base"
                  >
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                {data?.payload?.filter((item) => {
                if(search === ''){
                  return item;
                }else if(item?.fullName?.toLowerCase().includes(search?.toLowerCase())){
                  return item;
                }
              }).map((item, index) => (
                  <tr className="bg-white border-b-4 border-gray" key={index}>
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base ">
                      {index+=1}
                    </td>
                    <td className="max-w-[500px] font-medium text-gray-900   px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4">
                      <NavLink
                        to={`/profile-detail/${item?.uuid}`}
                        className="flex items-center"
                      >
                        <img
                          src={item?.profileImage ?? inf}
                          alt=""
                          className="w-[24px] h-[24px] lg:w-[35px] lg:h-[35px] md:w-[50px] md:h-[50px] object-fill rounded-full mr-2"
                        />
                        <p className="text-[10px] sm:text-sm md:text-sm lg:text-base">
                          {item.fullName}
                        </p>
                      </NavLink>
                    </td>
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base ">
                      {item.gender}
                    </td>
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base ">
                      {/* {item.createdBy} */}Hello
                    </td>
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base ">
                      <div className="flex items-center">
                        <svg
                          width="13"
                          height="14"
                          viewBox="0 0 13 14"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M9.38762 0.538032L9.38831 1.06277C11.3166 1.21389 12.5903 2.52784 12.5924 4.54283L12.6 10.4409C12.6028 12.6378 11.2226 13.9895 9.01026 13.993L3.60632 14C1.40784 14.0028 0.0103716 12.6189 0.00760708 10.4157L4.65482e-06 4.5869C-0.00275986 2.55862 1.22607 1.24818 3.15432 1.07117L3.15363 0.546428C3.15294 0.238582 3.38101 0.00699808 3.68511 0.00699808C3.9892 0.00629843 4.21728 0.237183 4.21797 0.545029L4.21866 1.03478L8.32397 1.02919L8.32328 0.539431C8.32259 0.231586 8.55066 0.000701235 8.85476 1.58584e-06C9.15194 -0.000698064 9.38693 0.230186 9.38762 0.538032ZM1.06509 4.80311L11.5288 4.78912V4.54424C11.4991 3.04 10.7444 2.25079 9.38975 2.13325L9.39044 2.67198C9.39044 2.97283 9.15615 3.21141 8.85896 3.21141C8.55486 3.21211 8.3261 2.97423 8.3261 2.67338L8.32541 2.10666L4.2201 2.11226L4.22079 2.67828C4.22079 2.97983 3.99341 3.21771 3.68931 3.21771C3.38521 3.21841 3.15645 2.98123 3.15645 2.67968L3.15576 2.14095C1.80806 2.27598 1.06233 3.06798 1.0644 4.58552L1.06509 4.80311ZM8.56799 7.98297V7.99066C8.5749 8.3125 8.83753 8.55668 9.15614 8.54968C9.46715 8.54199 9.71526 8.27542 9.70835 7.95358C9.69384 7.64574 9.44434 7.39456 9.13402 7.39526C8.8161 7.40226 8.5673 7.66113 8.56799 7.98297ZM9.13887 11.1244C8.82095 11.1174 8.56454 10.8522 8.56385 10.5304C8.55694 10.2086 8.81197 9.942 9.12989 9.9343H9.1368C9.46163 9.9343 9.72495 10.1995 9.72495 10.5283C9.72564 10.8571 9.46301 11.1237 9.13887 11.1244ZM5.72048 7.99424C5.7343 8.31608 5.99762 8.56726 6.31554 8.55326C6.62655 8.53857 6.87466 8.2727 6.86084 7.95086C6.85324 7.63602 6.59752 7.39115 6.28651 7.39184C5.96859 7.40584 5.71979 7.6724 5.72048 7.99424ZM6.31827 11.093C6.00035 11.107 5.73772 10.8558 5.72321 10.534C5.72321 10.2121 5.97133 9.94626 6.28925 9.93157C6.60025 9.93087 6.85666 10.1757 6.86357 10.4899C6.87809 10.8124 6.62928 11.0783 6.31827 11.093ZM2.87297 8.01868C2.88679 8.34052 3.15011 8.59239 3.46803 8.5777C3.77904 8.56371 4.02715 8.29714 4.01264 7.9753C4.00573 7.66046 3.75001 7.41558 3.43831 7.41628C3.12039 7.43028 2.87228 7.69684 2.87297 8.01868ZM3.47098 11.0964C3.15307 11.1111 2.89044 10.8592 2.87592 10.5374C2.87523 10.2156 3.12404 9.949 3.44196 9.93501C3.75297 9.93431 4.00937 10.1792 4.01629 10.494C4.0308 10.8159 3.78268 11.0824 3.47098 11.0964Z"
                            fill="#03A89E"
                          />
                        </svg>
                        <span className="ml-1 ">{new Date(
                            item?.createdDate
                          ).toLocaleDateString()}</span>
                      </div>
                    </td>
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base ">
                      {item.roles[0].name}
                    </td>
                    <td className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base ">
                      {item?.course?.courseName}
                    </td>
                    {/* Generation */}
                    <td className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base ">
                      <div className="flex  items-center">
                        <span className="ml-1">{item?.generation?.generationName}</span>
                      </div>
                    </td>
                    {/* Delete */}

                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 ">
                      <div className="dropdown dropdown-end">
                        <label tabIndex="0" className="m-1 cursor-pointer">
                          <img src={menu} alt="" className=" rounded-lg mr-3" />
                        </label>
                        <ul
                          tabIndex="0"
                          className="dropdown-content p-2 shadow bg-bgmodal rounded-md w-[200px] mr-[20px]"
                        >
                          <div>
                            <EditUserComponent index={item}/>
                          </div>
                          <div>{<DeleteUserComponent id={item?.id} />}</div>
                        </ul>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div className="flex justify-end mr-3">
              <div>
                <ListUserPagination setPage={setPage} data={data} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
