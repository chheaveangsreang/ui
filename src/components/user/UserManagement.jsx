import React from "react";
import { NavLink } from "react-router-dom";
import Popup from "reactjs-popup";
import logo from "../../../src/assets/logo.png";
const data = [
  {
    name: "G-Dragon",
    gender: "Male",
    createby: "Chhuon Rina",
    date: "12/09/21",
    role: "Teacher",
    course: " Basic Course",
    gen: "10th gen",
  },
  {
    name: "G-Dragon",
    gender: "Male",
    createby: "Chhuon Rina",
    date: "12/09/21",
    role: "Teacher",
    course: " Basic Course",
    gen: "10th gen",
  },
  {
    name: "G-Dragon",
    gender: "Male",
    createby: "Chhuon Rina",
    date: "12/09/21",
    role: "Teacher",
    course: " Basic Course",
    gen: "10th gen",
  },
  {
    name: "G-Dragon",
    gender: "Male",
    createby: "Chhuon Rina",
    date: "12/09/21",
    role: "Teacher",
    course: " Basic Course",
    gen: "10th gen",
  },
  {
    name: "G-Dragon",
    gender: "Male",
    createby: "Chhuon Rina",
    date: "12/09/21",
    role: "Teacher",
    course: " Basic Course",
    gen: "10th gen",
  },
  {
    name: "G-Dragon",
    gender: "Male",
    createby: "Chhuon Rina",
    date: "12/09/21",
    role: "Teacher",
    course: " Basic Course",
    gen: "10th gen",
  },
  {
    name: "G-Dragon",
    gender: "Male",
    createby: "Chhuon Rina",
    date: "12/09/21",
    role: "Teacher",
    course: " Basic Course",
    gen: "10th gen",
  },
  {
    name: "G-Dragon",
    gender: "Male",
    createby: "Chhuon Rina",
    date: "12/09/21",
    role: "Teacher",
    course: " Basic Course",
    gen: "10th gen",
  },
  {
    name: "G-Dragon",
    gender: "Male",
    createby: "Chhuon Rina",
    date: "12/09/21",
    role: "Teacher",
    course: " Basic Course",
    gen: "10th gen",
  },
  {
    name: "G-Dragon",
    gender: "Male",
    createby: "Chhuon Rina",
    date: "12/09/21",
    role: "Teacher",
    course: " Basic Course",
    gen: "10th gen",
  },
];

export default function UserManagement() {
  return (
    <div>
      <div className="bg-gray-100 mt-0 pt-0 h-screen">
        <div className="block  max-w-full border-gray-200 sticky top-0 mt-0 pt-0 bg-gray-100">
          <div className="grid grid-flow-row-dense md:grid-cols-12 grid-cols-6 lg:grid-cols-12  items-center  lg:p-3 md:p-3 p-3">
            <h3 className="whitespace-nowrap lg:text-3xl md:text-lg sm:text-sm text-xs  sm:block md:block lg:block  font-bold text-left col-span-2  md:col-span-3 lg:col-span-6">
              User
            </h3>
            <div className="lg:col-span-6 md:col-span-9 col-span-4 justify-end">
              <div className="flex justify-end items-center">
                <span className="mr-3 text-xs sm:text-sm lg:text-base font-bold hidden sm:hidden md:block lg:block">
                  Sreang Chheaveang
                </span>
                <div>
                  <img
                    className="rounded-full w-8 h-8 lg:w-10 lg:h-10"
                    src="https://i.pinimg.com/564x/05/94/ac/0594ac73a508313beb64be181bff6fb3.jpg"
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="grid grid-flow-row-dense grid-cols-8 md:grid-cols-12 lg:grid-cols-12  items-center lg:p-3 md:p-3 p-3">
            <h3 className="whitespace-nowrap lg:text-3xl md:text-lg sm:text-sm text-xs  sm:block md:block lg:block  font-bold text-left col-span-4  md:col-span-3 lg:col-span-4">
              <div>
                <form>
                  <div className="relative pr-0">
                    <input
                      type="search"
                      id="default-search"
                      className="block p-2 w-full  text-xs sm:text-sm md:text-sm lg:text-base text-gray-900 bg-white rounded-lg focus:outline-none"
                      placeholder="Search..."
                      required
                    />
                    <button
                      type="submit"
                      className="text-white absolute right-2.5 bottom-1 bg-purple-800 hover:bg-purole-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-1  py-1 "
                    >
                      <svg
                        width="17"
                        height="17"
                        viewBox="0 0 17 17"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M14.125 12.0307L15.5663 13.4719C16.1446 14.0503 16.1446 14.9879 15.5663 15.5663C14.9879 16.1446 14.0503 16.1446 13.4719 15.5663L12.0307 14.125M1 7.375C1 3.85418 3.85418 1 7.375 1C10.8958 1 13.75 3.85418 13.75 7.375C13.75 10.8958 10.8958 13.75 7.375 13.75C3.85418 13.75 1 10.8958 1 7.375Z"
                          stroke="#FDFDFD"
                          stroke-width="1.5"
                          stroke-linecap="round"
                        />
                      </svg>
                    </button>
                  </div>
                </form>
              </div>
            </h3>
            <div className="relative pr-0 lg:col-span-8  md:col-span-9 col-span-4 flex justify-end">
              <div className="flex justify-end items-center">
                <select className="block p-2 w-full  text-xs sm:text-sm md:text-sm lg:text-base text-gray-900 bg-white rounded-lg focus:outline-none">
                  <option disabled selected>
                    10th Generation
                  </option>
                  <option>9th Generation</option>
                  <option>8th Generation</option>
                  <option>7th Generation</option>
                  <option>6th Generation</option>
                </select>
              </div>
            </div>
          </div>
        </div>

        <div className="p-3 bg-gray-100 ">
          <div className="overflow-x-auto ">
            <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 divide-y-2">
              <thead className="text-xs text-gray-700 uppercase dark:bg-gray-700 dark:text-gray-400">
                <tr>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base "
                  >
                    No
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px]  sm:text-sm md:text-sm lg:text-base items-center"
                  >
                    <div className="flex items-center">
                      <span>Name</span>
                      <span className="ml-2">
                        <svg
                          width="9"
                          height="7"
                          viewBox="0 0 9 7"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M3.4083 6.74155C3.3677 6.702 3.1941 6.55265 3.0513 6.41354C2.1532 5.59795 0.6832 3.47034 0.2345 2.35675C0.1624 2.18763 0.0098 1.76006 0 1.53161C0 1.31271 0.0504 1.10404 0.1526 0.90492C0.2954 0.656698 0.5201 0.457574 0.7854 0.348466C0.9695 0.278227 1.5204 0.169118 1.5302 0.169118C2.1329 0.0600097 3.1122 0 4.1944 0C5.2255 0 6.1649 0.0600097 6.7767 0.149342C6.7865 0.159571 7.4711 0.26868 7.7056 0.388018C8.134 0.606917 8.4 1.03449 8.4 1.49206V1.53161C8.3895 1.82962 8.1235 2.45631 8.1137 2.45631C7.6643 3.50989 6.2664 5.58841 5.3375 6.42377C5.3375 6.42377 5.0988 6.65904 4.9497 6.76132C4.7355 6.9209 4.4702 7 4.2049 7C3.9088 7 3.633 6.91067 3.4083 6.74155Z"
                            fill="#4E5A69"
                          />
                        </svg>
                      </span>
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base "
                  >
                    <span className="">Gender</span>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base "
                  >
                    <span>Created By </span>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base"
                  >
                    <div className="flex items-center">
                      <span>Date</span>
                      <span className="ml-2 ">
                        <svg
                          width="9"
                          height="7"
                          viewBox="0 0 9 7"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M3.4083 6.74155C3.3677 6.702 3.1941 6.55265 3.0513 6.41354C2.1532 5.59795 0.6832 3.47034 0.2345 2.35675C0.1624 2.18763 0.0098 1.76006 0 1.53161C0 1.31271 0.0504 1.10404 0.1526 0.90492C0.2954 0.656698 0.5201 0.457574 0.7854 0.348466C0.9695 0.278227 1.5204 0.169118 1.5302 0.169118C2.1329 0.0600097 3.1122 0 4.1944 0C5.2255 0 6.1649 0.0600097 6.7767 0.149342C6.7865 0.159571 7.4711 0.26868 7.7056 0.388018C8.134 0.606917 8.4 1.03449 8.4 1.49206V1.53161C8.3895 1.82962 8.1235 2.45631 8.1137 2.45631C7.6643 3.50989 6.2664 5.58841 5.3375 6.42377C5.3375 6.42377 5.0988 6.65904 4.9497 6.76132C4.7355 6.9209 4.4702 7 4.2049 7C3.9088 7 3.633 6.91067 3.4083 6.74155Z"
                            fill="#4E5A69"
                          />
                        </svg>
                      </span>
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base "
                  >
                    <span className=""> Role </span>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px]  sm:text-sm md:text-sm lg:text-base "
                  >
                    Course Types
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base "
                  >
                    Generation
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base"
                  >
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                {data.map((d) => (
                  <tr className="bg-white border-b-4 border-gray">
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base ">
                      01
                    </td>
                    <td
                      scope="row"
                      className="max-w-[500px] font-medium text-gray-900  flex items-center px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4"
                    >
                      <img
                        src="https://im.idiva.com/photogallery/2020/Sep/4---2020-09-21T173052187_5f68969905c97.jpg"
                        alt=""
                        className="w-[24px] h-[24px] lg:w-[35px] lg:h-[35px] md:w-[50px] md:h-[50px]  rounded-full mr-2"
                      />
                      <p className="text-[10px] sm:text-sm md:text-sm lg:text-base">
                        {d.name}
                      </p>
                    </td>
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base ">
                      {d.gender}
                    </td>
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base ">
                      {d.createby}
                    </td>
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base ">
                      <div className="flex items-center">
                        <svg
                          width="13"
                          height="14"
                          viewBox="0 0 13 14"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M9.38762 0.538032L9.38831 1.06277C11.3166 1.21389 12.5903 2.52784 12.5924 4.54283L12.6 10.4409C12.6028 12.6378 11.2226 13.9895 9.01026 13.993L3.60632 14C1.40784 14.0028 0.0103716 12.6189 0.00760708 10.4157L4.65482e-06 4.5869C-0.00275986 2.55862 1.22607 1.24818 3.15432 1.07117L3.15363 0.546428C3.15294 0.238582 3.38101 0.00699808 3.68511 0.00699808C3.9892 0.00629843 4.21728 0.237183 4.21797 0.545029L4.21866 1.03478L8.32397 1.02919L8.32328 0.539431C8.32259 0.231586 8.55066 0.000701235 8.85476 1.58584e-06C9.15194 -0.000698064 9.38693 0.230186 9.38762 0.538032ZM1.06509 4.80311L11.5288 4.78912V4.54424C11.4991 3.04 10.7444 2.25079 9.38975 2.13325L9.39044 2.67198C9.39044 2.97283 9.15615 3.21141 8.85896 3.21141C8.55486 3.21211 8.3261 2.97423 8.3261 2.67338L8.32541 2.10666L4.2201 2.11226L4.22079 2.67828C4.22079 2.97983 3.99341 3.21771 3.68931 3.21771C3.38521 3.21841 3.15645 2.98123 3.15645 2.67968L3.15576 2.14095C1.80806 2.27598 1.06233 3.06798 1.0644 4.58552L1.06509 4.80311ZM8.56799 7.98297V7.99066C8.5749 8.3125 8.83753 8.55668 9.15614 8.54968C9.46715 8.54199 9.71526 8.27542 9.70835 7.95358C9.69384 7.64574 9.44434 7.39456 9.13402 7.39526C8.8161 7.40226 8.5673 7.66113 8.56799 7.98297ZM9.13887 11.1244C8.82095 11.1174 8.56454 10.8522 8.56385 10.5304C8.55694 10.2086 8.81197 9.942 9.12989 9.9343H9.1368C9.46163 9.9343 9.72495 10.1995 9.72495 10.5283C9.72564 10.8571 9.46301 11.1237 9.13887 11.1244ZM5.72048 7.99424C5.7343 8.31608 5.99762 8.56726 6.31554 8.55326C6.62655 8.53857 6.87466 8.2727 6.86084 7.95086C6.85324 7.63602 6.59752 7.39115 6.28651 7.39184C5.96859 7.40584 5.71979 7.6724 5.72048 7.99424ZM6.31827 11.093C6.00035 11.107 5.73772 10.8558 5.72321 10.534C5.72321 10.2121 5.97133 9.94626 6.28925 9.93157C6.60025 9.93087 6.85666 10.1757 6.86357 10.4899C6.87809 10.8124 6.62928 11.0783 6.31827 11.093ZM2.87297 8.01868C2.88679 8.34052 3.15011 8.59239 3.46803 8.5777C3.77904 8.56371 4.02715 8.29714 4.01264 7.9753C4.00573 7.66046 3.75001 7.41558 3.43831 7.41628C3.12039 7.43028 2.87228 7.69684 2.87297 8.01868ZM3.47098 11.0964C3.15307 11.1111 2.89044 10.8592 2.87592 10.5374C2.87523 10.2156 3.12404 9.949 3.44196 9.93501C3.75297 9.93431 4.00937 10.1792 4.01629 10.494C4.0308 10.8159 3.78268 11.0824 3.47098 11.0964Z"
                            fill="#03A89E"
                          />
                        </svg>
                        <span className="ml-1 ">{d.date}</span>
                      </div>
                    </td>
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] hidden lg:table-cell sm:text-sm md:text-sm lg:text-base ">
                      {d.role}
                    </td>
                    <td className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base ">
                      {d.course}
                    </td>
                    {/* Generation */}
                    <td className="px-2 py-2 lg:px-5 lg:py-2  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base ">
                      <div className="flex  items-center">
                        <span className="ml-1">{d.gen}</span>
                      </div>
                    </td>
                    {/* Delete */}

                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 ">
                      <Popup
                        trigger={
                          <a
                            type="btn"
                            className="flex ml-4 items-center w-1/4"
                          >
                            <svg
                              width="22"
                              height="22"
                              viewBox="0 0 22 22"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M3.97222 7C3.97222 6.58579 3.63644 6.25 3.22222 6.25C2.80801 6.25 2.47222 6.58579 2.47222 7H3.97222ZM19.5278 7C19.5278 6.58579 19.192 6.25 18.7778 6.25C18.3636 6.25 18.0278 6.58579 18.0278 7H19.5278ZM13.9722 10C13.9722 9.58579 13.6364 9.25 13.2222 9.25C12.808 9.25 12.4722 9.58579 12.4722 10H13.9722ZM12.4722 16C12.4722 16.4142 12.808 16.75 13.2222 16.75C13.6364 16.75 13.9722 16.4142 13.9722 16H12.4722ZM9.52778 10C9.52778 9.58579 9.19199 9.25 8.77778 9.25C8.36356 9.25 8.02778 9.58579 8.02778 10H9.52778ZM8.02778 16C8.02778 16.4142 8.36356 16.75 8.77778 16.75C9.19199 16.75 9.52778 16.4142 9.52778 16H8.02778ZM21 4.75C21.4142 4.75 21.75 4.41421 21.75 4C21.75 3.58579 21.4142 3.25 21 3.25V4.75ZM1 3.25C0.585786 3.25 0.25 3.58579 0.25 4C0.25 4.41421 0.585786 4.75 1 4.75V3.25ZM13.8819 1.8906L13.2793 2.33702L13.8819 1.8906ZM8.11807 1.8906L7.51541 1.44418V1.44418L8.11807 1.8906ZM7.66667 21.75H14.3333V20.25H7.66667V21.75ZM2.47222 7V17H3.97222V7H2.47222ZM19.5278 17V7H18.0278V17H19.5278ZM14.3333 21.75C17.1259 21.75 19.5278 19.6957 19.5278 17H18.0278C18.0278 18.7226 16.45 20.25 14.3333 20.25V21.75ZM7.66667 20.25C5.55 20.25 3.97222 18.7226 3.97222 17H2.47222C2.47222 19.6957 4.87414 21.75 7.66667 21.75V20.25ZM12.4722 10V16H13.9722V10H12.4722ZM8.02778 10L8.02778 16H9.52778L9.52778 10H8.02778ZM9.96708 1.75H12.0329V0.25H9.96708V1.75ZM13.2793 2.33702L14.8418 4.44642L16.0471 3.55358L14.4846 1.44418L13.2793 2.33702ZM15.4444 3.25H6.55556V4.75H15.4444V3.25ZM7.15822 4.44642L8.72074 2.33702L7.51541 1.44418L5.95289 3.55358L7.15822 4.44642ZM15.4444 4.75H21V3.25H15.4444V4.75ZM6.55556 3.25H1V4.75H6.55556V3.25ZM12.0329 1.75C12.5585 1.75 13.0202 1.98727 13.2793 2.33702L14.4846 1.44418C13.9194 0.681135 12.9933 0.25 12.0329 0.25V1.75ZM9.96708 0.25C9.00665 0.25 8.08062 0.681135 7.51541 1.44418L8.72074 2.33702C8.97982 1.98727 9.44148 1.75 9.96708 1.75V0.25Z"
                                fill="#FF0000"
                              />
                            </svg>
                          </a>
                        }
                        position="left center"
                      >
                        <div className="w-full h-screen flex justify-center items-center">
                          <div class="card card-compact w-64 bg-base-100 shadow-xl ">
                            <div class="card-body items-center ">
                              <div class="card-actions justify-end">
                                <button class="btn bg-purple-500">
                                  Delete
                                </button>
                                <button class="btn bg-red-600">Cencal</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </Popup>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>

        <div className="flex justify-end mr-3">
          <div class="btn-group">
            <a class="btn">Prev</a>
            <a class="btn">1</a>
            <a class="btn btn-active">2</a>
            <a class="btn">3</a>
            <a class="btn">4</a>
            <a class="btn">Next </a>
          </div>
        </div>
      </div>
    </div>
  );
}
