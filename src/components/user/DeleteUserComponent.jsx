import React, { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import remove from "./../../assets/icons/delete.svg";
import warning from "./../../assets/icons/warnm.svg";
import UserPerGenerationService from "../../service/UserPerGenerationService";
import { useDispatch } from "react-redux";
import { removeUserGeneration } from "../../redux/slices/UserPerGenerationSlice";

export default function DeleteUserComponent({id}) {
  let [isOpen, setIsOpen] = useState(false);
  const dispatch = useDispatch()

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  const handleDeleteUser = () =>{
    UserPerGenerationService.deleteUserByGeneration(id).then(()=>dispatch(removeUserGeneration(id)))
    setIsOpen(false);
  }

  return (
    <>
      <div
        onClick={openModal}
        className="flex justify-center items-center w-full hover:bg-red-300 bg-red-200 p-2 rounded-lg mb-2 cursor-pointer"
      >
        <img src={remove} alt="" className="w-4 h-4" />
        <p className="ml-3 text-red-600">Delete</p>
      </div>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={openModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center text-center ">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className=" max-w-[300px] md:max-w-[400px] sm:max-w-[300px] transform overflow-hidden rounded-lg bg-white py-5 w-full  text-left align-middle shadow-xl transition-all px-5">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900"
                  >
                    <div className="flex items-top ">
                      <img
                        src={warning}
                        alt=""
                        className="h-10 w-10 bg-red-200 p-3 rounded-full"
                      />
                      <div className="ml-3">
                        <p className="font-semibold">Delete User</p>
                        <p className="text-sm py-2 text-black  text-left">
                          Are you sure? All of your data will be permanently
                          removed. your data cannot be redo.
                        </p>
                      </div>
                    </div>
                  </Dialog.Title>

                  <div className="flex justify-end pt-3">
                    <div>
                      <button
                        onClick={closeModal}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2 text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-red-800  rounded-md "
                      >
                        Cancel
                      </button>
                    </div>
                    <div>
                      <button
                        onClick={()=>handleDeleteUser()}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2  text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-blue-800 rounded-md"
                      >
                        Yes
                      </button>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
