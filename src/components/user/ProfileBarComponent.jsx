import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom';
import { getUserInfo } from '../../utils/crypto';

export default function ProfileBarComponent() {

    const [role, setRole] = useState("");
    const userInfo = getUserInfo()
    useEffect(()=>{
        setRole(userInfo?.roles[0]?.name);
    },[userInfo])


  return (
        <>
   
          
              <NavLink
                to={`/profile-detail/${userInfo?.uuid}`}
                className="flex justify-end items-center"
              >
                <span className="mr-3 text-xs sm:text-sm lg:text-sm font-semibold">
                    {userInfo?.fullName}
                </span>
                <img
                  src={userInfo?.profileImage}
                  alt=""
                  className="object-cover h-7 sm:h-7 md:h-10 lg:h-10 mr-2 lg:mr-0 md:mr-0 w-7 sm:w-7 md:w-10 lg:w-10 rounded-full"
                />
              </NavLink>
 
        </>

  )
}
