import React, { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import warning from "./../../assets/icons/warnm.svg";
import remove from "./../../assets/icons/close.svg";

import { useDispatch } from "react-redux";
import groupChatMemberService from "../../service/groupChatMemberService";
import { deleteMemberGroupChat } from "../../redux/slices/groupChatMemberSlice";

export default function RemoveGroupChatMember({groupId,userId}) {
  let [isOpen, setIsOpen] = useState(false);

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }


  const dispatch = useDispatch()
  const onDeleteGroupChatMember = () => {
      const data = {
          groupId:groupId,
          userId:userId
      }
    groupChatMemberService.deleteGroupChatMember(data).then((r)=>dispatch(deleteMemberGroupChat(r)))//dispatch(deleteMemberGroupChat(r))


    setIsOpen(false)
    
    
  
}

  return (
    <>
      <div onClick={openModal}>
        <li className="flex justify-center items-center  p-3 rounded-lg cursor-pointer">
          <img src={remove} alt="" />
        
        </li>
      </div>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={openModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center text-center ">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className=" max-w-[300px] md:max-w-[400px] sm:max-w-[300px] transform overflow-hidden rounded-lg bg-white py-5 w-full  text-left align-middle shadow-xl transition-all px-5">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900"
                  >
                    <div className="flex items-top ">
                      <img
                        src={warning}
                        alt=""
                        className="h-10 w-10 bg-red-200 p-3 rounded-full"
                      />
                      <div className="ml-3">
                        <p className="font-semibold">Delete Group Chat</p>
                        <p className="text-sm py-2 text-black  text-left">
                          Are you sure? All of your data will be permanently
                          removed. your member cannot be redo.
                        </p>
                      </div>
                    </div>
                  </Dialog.Title>

                  <div className="flex justify-end pt-3">
                    <div>
                      <button
                        onClick={closeModal}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2 text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-red-800  rounded-md "
                      >
                        Cancel
                      </button>
                    </div>
                    <div>
                      <button
                        onClick={()=>onDeleteGroupChatMember()}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2  text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-blue-800 rounded-md"
                      >
                        Yes
                      </button>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
