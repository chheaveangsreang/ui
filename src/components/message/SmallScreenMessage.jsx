import React, { createRef, useEffect, useState } from "react";
import imageupload from "./../../assets/icons/imageupload.svg";
import fileupload from "./../../assets/icons/fileupload.svg";
import member from "./../../assets/icons/member.svg";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import messageService from "../../service/messageService";
import { messageAction } from "../../redux/slices/messageSlice";
import groupChatService from "../../service/groupChatService";
import SockJS from 'sockjs-client';
import { over } from 'stompjs';
import { getUserInfo } from "../../utils/crypto";
import groupChatMemberService from "../../service/groupChatMemberService";
import { getAllGroupChatMember } from "../../redux/slices/groupChatMemberSlice";
import back from "./../../assets/icons/back.svg"
import submit from "./../../assets/icons/submit.svg"
import {useNavigate } from "react-router-dom";

var stompClient = null;

export default function SmallScreenMessage() {
  const [role, setRole] = useState('')
  const userInfo = getUserInfo()
  const [open, setOpen] = useState(false);
  const scrolldiv = createRef();
  const params = useParams();
  const messages = useSelector(state => state.message.value)
  const [groupChat, setGroupChat] = useState()
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const grouChatpMember = useSelector((state) => state.groupChatMember.value)
  const connect = () => {
    let Sock = new SockJS('https://api.media.kshrd-ite.com/ws-kshrdmedia');
    stompClient = over(Sock);
    stompClient.connect({}, onConnected, onError);
  }

  const onConnected = () => {
    stompClient.subscribe(`/user/${userInfo?.id}/private-message`, onReceiveMessage);
  }

  const onReceiveMessage = (g) => {
    let receive = JSON.parse(g.body);
    if(params.id == receive.groupChatId){
      dispatch(messageAction.sendMsg(receive));
    }
  }

  useEffect(() => {
    setRole(userInfo?.roles[0].name)
    groupChatService.getGroupChatById(params.id).then(r => setGroupChat(r))
    messageService.getMessageByGroupId(params.id).then(r => dispatch(messageAction.getMessage(r)));

    // groupMemberService.listGroupMember(params.id).then((r)=>dispatch(getAllGroupMember(r)))
    groupChatMemberService.listGroupChatMember(params.id).then((r) => dispatch(getAllGroupChatMember(r)))

  }, [params.id])

  useEffect(() => {
    const scrollToBottom = (node) => {
      node.scrollTop = node.scrollHeight;
    };
    scrollToBottom(scrolldiv.current);
  })

  const onError = (error) => {
    console.log("error", error)
  }

  const [sendMessage, setSendMessage] = useState("");

  const handleOnChange = (e) => {
    setSendMessage(e.target.value);
  }
  const handleKeyUp = (e) =>{
    if(e.key == "Enter"){
      onSubmit(e);
    }
  }

  const onSubmit = (e) => {
    e.preventDefault();
    const formData = {
      message: sendMessage,
      imageUrl: "",
      groupChatId: groupChat?.payload?.id
    }

    stompClient.send(`/user/private-message`, {}, JSON.stringify({ 'messageContent': formData }));
    messageService.sendMessgage(formData)
    document.getElementById('chat').value = "";
  }

  useEffect(() => {
    connect()
  }, [])

  return (
    <div>
      <div className="max-w-full border rounded">
        <div className="flex justify-between">
          <div className="w-full flex-1  border relative">
            <div className="flex justify-start items-center border-b  border-gray-300">
              <button
                onClick={() => navigate(-1)}
                className="ml-2 cursor-pointer"
              >
                <img src={back} alt="" />
              </button>
              <div className="relative flex items-center p-3 ">
                <img
                  className=" lg:w-10 lg:h-10 md:w-10 md:h-10 sm:w-8 sm:h-8 w-8 h-8 rounded-full"
                  src="https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083383__340.jpg"
                  alt=""
                />
                <span className="block ml-2  text-gray-600 ">
                  <p className=" font-bold text-left text-sm sm:text-sm md:text-base lg:text-base">
                  {groupChat?.payload?.groupName} 
                  </p>
                  <span className="block text-xs text-gray-600 text-left">
                    {groupChat?.payload?.member} members
                  </span>
                </span>
              </div>
            </div>

            <div ref={scrolldiv} className="relative w-full p-6 overflow-y-auto h-[76vh]">
              {messages?.map((item, key) => {
                return (
                  <ul className="space-y-2">
                    {userInfo?.id !== item.userSendInfo?.id ?
                      <li key={key} className="flex justify-start">
                        <div className="relative max-w-xl  text-gray-700 rounded  flex">
                          <img
                            className="object-cover w-6 h-6 mr-1  rounded-full"
                            src={item.userSendInfo?.profileImage}
                            alt=""
                          />
                          <div className="flex justify-start items-center">
                            <span className="block bg-slate-300 text-left  px-2 py-2 shadow mb-1 rounded-md text-sm sm:text-sm md:tex-base lg:text-base">
                              {item.message}
                            </span>
                            <span className="mr-2 text-xs">{new Date(item.createdDate).toLocaleString('en-US', { hour: 'numeric', hour12: true, minute: 'numeric' })}</span>
                          </div>
                        </div>
                      </li>
                      :
                      <li key={key} className="flex justify-end">
                        <div className="relative max-w-xl  text-gray-700 rounded  flex">
                          <div className="flex justify-start items-center">
                            {/* rounded-bl-full rounded-tr-full rounded-br-full */}
                            <span className="mr-2 text-xs">{new Date(item.createdDate).toLocaleString('en-US', { hour: 'numeric', hour12: true, minute: 'numeric' })}</span>
                            <span className="block bg-purple-400 text-left  px-2 py-2 mb-1 shadow rounded-md text-sm sm:text-sm md:tex-base lg:text-base">
                              {item.message}
                            </span>
                            
                          </div>
                          <img
                            className="object-cover w-6 h-6 ml-1  rounded-full"
                            src={item.userSendInfo?.profileImage}
                            alt=""
                          />
                          
                        </div>
                      </li>
                    }
                  </ul>
                )
              })}
            </div>

            <div className="flex items-center justify-between w-full p-3 mt-3 border-t border-gray-300">
              <form className="w-full" onSubmit={onSubmit}>
                <div className="flex items-center w-full rounded-lg">
                  <label htmlFor="multiple_files">
                    <img
                      src={imageupload}
                      alt=""
                      className="cursor-pointer w-7 h-7"
                    />
                    <input id="multiple_files" type="file" className="hidden" />
                  </label>

               
                  <textarea
                    id="chat"
                    rows="1"
                    onKeyUp={handleKeyUp}
                    required
                    onChange={handleOnChange}
                    className="block w-full mx-4 p-2.5 text-sm text-gray-900 bg-white rounded-lg border border-gray-300 focus:outline-none "
                    placeholder="Your message..."
                  ></textarea>
                  <button
                    type="submit"
                    className="inline-flex justify-center  text-blue-600 rounded-full cursor-pointer "
                  >
                    {/* <svg className="w-6 h-6 rotate-90" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z"></path></svg> */}
                    <img src={submit} alt="" className="w-7 h-7 " />
                  </button>
                </div>
              </form>
            </div>
          </div>

          <div
            className={`${open ? "w-72 absolute" : "w-0"
              } relative bg-white duration-300 hidden`}
          >
            <svg
              viewBox="0 0 28 28"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              onClick={() => setOpen(!open)}
              className={`h-8 w-8 absolute -left-14 top-[2%] cursor-pointer`}
            >
              <path
                d="M19.95 1C19.95 0.585786 19.6142 0.25 19.2 0.25C18.7858 0.25 18.45 0.585786 18.45 1H19.95ZM18.45 27C18.45 27.4142 18.7858 27.75 19.2 27.75C19.6142 27.75 19.95 27.4142 19.95 27H18.45ZM21.8 0.25H6.2V1.75H21.8V0.25ZM0.25 6.2V21.8H1.75V6.2H0.25ZM6.2 27.75H21.8V26.25H6.2V27.75ZM27.75 21.8V6.2H26.25V21.8H27.75ZM21.8 27.75C25.0861 27.75 27.75 25.0861 27.75 21.8H26.25C26.25 24.2577 24.2577 26.25 21.8 26.25V27.75ZM0.25 21.8C0.25 25.0861 2.91391 27.75 6.2 27.75V26.25C3.74233 26.25 1.75 24.2577 1.75 21.8H0.25ZM6.2 0.25C2.91391 0.25 0.25 2.91391 0.25 6.2H1.75C1.75 3.74233 3.74233 1.75 6.2 1.75V0.25ZM21.8 1.75C24.2577 1.75 26.25 3.74233 26.25 6.2H27.75C27.75 2.91391 25.0861 0.25 21.8 0.25V1.75ZM18.45 1V27H19.95V1H18.45Z"
                fill="#28303F"
              />
            </svg>

            <div className={`${!open ? "hidden" : "block"}`}>
              <div className="flex justify-center items-center border-b  border-gray-300">
                <div className="relative flex  items-center p-3 ">
                  <p className="text-lg font-bold py-1.5">Group Info</p>
                </div>
              </div>
              <div className="p-3 border-b ">
                <div className="flex items-center ">
                  <img src={imageupload} alt="" />
                  <span className="block ml-2  text-gray-600 py-2">
                    <p className="font-semibold text-left ml-1"> 3 images </p>
                  </span>
                </div>

                <div className="flex items-center">
                  <img src={fileupload} alt="" />
                  <span className="block ml-2  text-gray-600 py-2">
                    <p className="font-semibold text-left ml-1"> 3 files </p>
                  </span>
                </div>
              </div>

              <div className="p-3">
                <div>
                  <div className="flex items-center">
                    <img src={member} alt="" />
                    <span className="block ml-2  text-gray-600 py-2">
                      <p className="font-semibold text-left ml-1">
                     
                        grouChatpMember
                      </p>
                    </span>
                  </div>
                  <div className="h-[50vh] pl-5 rounded-lg bg-gray-100 overflow-y-auto">
                    {grouChatpMember?.map((member, index) => (
                      <div
                        className="relative flex  items-center py-2 "
                        key={index}
                      >
                        <img
                          className=" w-8 h-8 rounded-full"
                          src={member?.profileImage}
                          alt=""
                        />
                        <span className="block ml-2  text-gray-600 ">
                          <p className=" font-semibold text-left">
                            {member?.fullName}
                          </p>
                        </span>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
