import React from 'react'
import PropTypes from 'prop-types'
import Picker from 'emoji-picker-react';
const EmojisComponent = ({pickEmpji}) => {
  return (
    <div className='w-full max-h-full overflow-y-auto bg-white'>
             <Picker onEmojiClick={pickEmpji} pickerStyle={{ width: '100%' }} />
    </div>
  );
};
EmojisComponent.propTypes = {
    pickEmpji:PropTypes.func
};
export default EmojisComponent;
