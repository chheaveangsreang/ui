import React from "react";
import { getUserInfo } from "../../utils/crypto";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { getGroupChat } from "../../redux/slices/groupChatSlice";
import groupChatService from "../../service/groupChatService";

// var stompClient = null;

export default function SmallScreenListUser() {

  const dispatch = useDispatch();
  const userInfo = getUserInfo();
  const groupChat = useSelector(state => state.groupChat.value)
  // const connectChat = () => {
  //   stompClient.subscribe(`/user/${userInfo?.id}/private-message-notification`, onMessageRecieve);
  // }
  // socket.connect(connectChat);
  // const onMessageRecieve = (data) => {
  //   let receive = JSON.parse(data.body);
  // };

  useEffect(() => {
    groupChatService.getGroupChat(20, 1).then(r => dispatch(getGroupChat(r)))
  }, [dispatch])

  return (
    <div>
      <div className="border-r border-gray-300 lg:col-span-1 overflow-y-auto h-[90vh]">
        <div className="relative flex items-center p-3  border-gray-300">
          <div className="w-full">
            <p className="text-lg font-bold text-left px-2 pb-2">All Chat</p>
            <form>
              <div className="relative w-full">
                <input
                  type="search"
                  className="block p-2.5 w-full z-20 text-sm text-gray-900 bg-white rounded-lg border  focus:outline-none"
                  placeholder="Search contact ...."
                  required
                />
                <button
                  type="submit"
                  className="absolute top-0 right-0 p-2.5 text-sm font-medium text-white bg-blue-700 rounded-r-lg border border-blue-700  focus:ring-4 focus:outline-none  "
                >
                  <svg
                    className="w-5 h-5"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    ></path>
                  </svg>
                </button>
              </div>
            </form>
          </div>
        </div>
        <ul className=" overflow-y-auto h-[70vh]">
          {groupChat?.map((item, key) => {
            return (
              <li>
                {userInfo.roles[0].name === "ROLE_ADMIN"
                  ?
                  <NavLink
                    to={`/admin/sm-message/${item.id}`}
                    className={({ isActive }) =>
                      [
                        "flex items-center px-3 py-2 text-sm transition duration-150 ease-in-out  cursor-pointer hover:bg-gray-100 focus:outline-none",
                        isActive ? "bg-gray-200" : null,
                      ]
                        .filter(Boolean)
                        .join(" ")
                    }
                  >
                    <img
                      className="object-cover w-10 h-10 rounded-full"
                      src={item.imageUrl}
                      alt=""
                    />
                    <div className="w-full pb-2">
                      <div className="flex justify-between">
                        <span className="block ml-2 font-semibold text-gray-600 md:text-sm text-">
                          {item?.groupName}
                        </span>
                      </div>
                      <div className="flex justify-between">
                        <span className="block ml-2 text-sm text-gray-600 text-left">
                          Good night
                        </span>
                        <span className="block ml-2 text-sm bg-red-700 rounded-full text-white px-1 min-w-[20px]">
                          123
                        </span>
                      </div>
                    </div>
                  </NavLink>
                  :
                  <NavLink
                    to={`/user/sm-message/${item.id}`}
                    className={({ isActive }) =>
                      [
                        "flex items-center px-3 py-2 text-sm transition duration-150 ease-in-out  cursor-pointer hover:bg-gray-100 focus:outline-none",
                        isActive ? "bg-gray-200" : null,
                      ]
                        .filter(Boolean)
                        .join(" ")
                    }
                  >
                    <img
                      className="object-cover w-10 h-10 rounded-full"
                      src={item.imageUrl}
                      alt=""
                    />
                    <div className="w-full pb-2">
                      <div className="flex justify-between">
                        <span className="block ml-2 font-semibold text-gray-600 md:text-sm text-">
                          {item?.groupName}
                        </span>
                      </div>
                      <div className="flex justify-between">
                        <span className="block ml-2 text-sm text-gray-600 text-left">
                          Good night
                        </span>
                        <span className="block ml-2 text-sm bg-red-700 rounded-full text-white px-1 min-w-[20px]">
                          123
                        </span>
                      </div>
                    </div>
                  </NavLink>
                }
              </li>
            )
          })}
        </ul>
      </div>
    </div>
  );
}
