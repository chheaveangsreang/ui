import React, { Fragment, useEffect, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import update from "./../../assets/icons/update.svg";
import api from "../../utils/api";
import groupChatService from "../../service/groupChatService";
import { useDispatch } from "react-redux";
import {editGroupChat} from "../../redux/slices/groupChatSlice";

export default function EditGroupMessageComponent({id}) {
  let [isOpen, setIsOpen] = useState(false);

  const dispatch = useDispatch();
  const [image, setImage] = useState();
  const [imageUrl, setImageUrl] = useState("");
  const [groupName, setGroupName] = useState("");


  const [groupChat,setGroupChat] = useState("") 

  const handleEditGroupChat = () => {
    const data = {
      groupName:groupName,
      imageUrl:image,
      status:true
    }

    groupChatService.updateGroupChat(id,data).then((r)=>dispatch(editGroupChat(r)))
    
    setIsOpen(false);
    setImage(null)
    setGroupName(null)

  }

  useEffect(() => {
    groupChatService.getGroupChatById(id).then(r=>setGroupChat(r))
  }, [dispatch, id]);

  useEffect(()=>{
    setImage(groupChat?.payload?.imageUrl)
    setGroupName(groupChat?.payload?.groupName)
  },[groupChat])


  const fileChange = (e) => {
    const objectUrl = URL.createObjectURL(e.target.files[0])
    setImageUrl(objectUrl)

    const formData = new FormData();
    formData.append('file', e.target.files[0])
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    api.post('files/', formData, config).then((res)=> setImage(res.data.fileUrl))
  }

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  return (
    <>
      <div onClick={openModal}>
        <li className="flex justify-center items-center w-full hover:bg-green-300 bg-green-200 p-2 rounded-lg mb-2 cursor-pointer">
          <img src={update} alt="" />
          <p className="ml-3 text-green-600">Edit</p>
        </li>
      </div>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={openModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto ">
            <div className="flex min-h-full items-center justify-center text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className=" max-w-[300px] bg-white md:max-w-[400px] sm:max-w-[300px] transform overflow-hidden rounded-lg  py-5 px-5 w-full  text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900"
                  >
                    <p className="text-2xl font-semibold text-black text-center">
                      Update Group
                    </p>
                  </Dialog.Title>

                  <div className="pt-7 mb-7">
                    <input
                      required
                      value={groupName || "" }
                      onChange={(e)=>setGroupName(e.target.value)}
                      placeholder="Group Name"
                      type="text"
                      className="text-[10px] px-2 sm:text-sm md:text-sm lg:text-sm py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md focus:ring-1"
                    />
                  </div>
                  <div>
                    <p className="mb-1">Choose Image</p>
                    <div className="relative overflow-hidden">
                      <div className="flex justify-center items-center">
                        <label
                          htmlFor="dropzone-file"
                          className="flex w-full flex-col justify-center items-center  h-64  rounded-lg   cursor-pointer "
                        >
                          <div className="flex flex-col justify-center items-center pt-5 pb-6">
                            <svg
                              className="mb-3 w-10 h-10 text-gray-400"
                              fill="none"
                              stroke="currentColor"
                              viewBox="0 0 24 24"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"
                              ></path>
                            </svg>
                            <p className="mb-2 text-sm text-gray-500 dark:text-gray-400">
                              <span className="font-semibold">
                                Click to upload
                              </span>
                              
                            </p>
                          </div>
                          <input id="dropzone-file"  className="form-control hidden"  onChange={fileChange}  type="file"></input>
                        
                          <img src={image} alt="" className="object-contain absolute w-full h-full  top-0 left-0 right-0 bottom-0 "/>
                        </label>
                      </div>

                      
                    </div>
                  </div>

                  <div className="flex justify-end mt-3">
                    <div>
                      <button
                        onClick={closeModal}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2  text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-red-800  rounded-md "
                      >
                        Cancel
                      </button>
                    </div>
                    <div>
                      <button
                        onClick={()=>handleEditGroupChat()}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2   text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-blue-800 rounded-md"
                      >
                        OK
                      </button>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
