import React from "react";
import {
  generatePlaceholderName,
  generateRandomColorNumber,
} from "./RandomUtils";
export default function UserPlaceholder({ name }) {
  console.log(name);
  const userPlaceholder = generatePlaceholderName(name);
  const placeholderBackground = `hsl(${generateRandomColorNumber()}, 50% , 50%)`;

  return (
    <div
      className="rounded-full lg:p-3 md:p-3 sm:p-2 p-2"
      style={{
        backgroundColor: placeholderBackground,
      }}
    >
      {userPlaceholder}
    </div>
  );
}
