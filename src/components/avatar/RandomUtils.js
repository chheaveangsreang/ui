export function generateRandomColorNumber() {
  let randomHue = Math.floor(Math.random() * 360);
  return randomHue;
}
export function generatePlaceholderName(name) {
  let splitUsername = name.split(" "),
    firstLetter = splitUsername[0]?.[0] ?? "",
    secondletter = splitUsername[1]?.[0] ?? "";
  return `${firstLetter}${secondletter}`;
}
