import React, { Fragment, useEffect, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import close from "./../../assets/icons/close.svg";
import add from "./../../assets/icons/add.svg";
import UserPerGenerationService from "../../service/UserPerGenerationService";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllGeneration,
} from "../../redux/slices/generationSlice";
import generationService from "../../service/generationService";
import { useParams } from "react-router-dom";
import groupMemberService from "../../service/groupMemberService";
import { addGroupMember } from "../../redux/slices/groupMemberSlice";

export default function AddGroupMember() {
  let [isOpen, setIsOpen] = useState(false);
  const dispatch = useDispatch();

  const { id } = useParams();

  const [search, setNewSearch] = useState("");

  const generation = useSelector((state) => state.generation.value);
  const [member, setMember] = useState([]);
  const [selected, setSelected] = useState([]);
  const groupMember = useSelector((state) => state.groupMember.value);



  const handleOnChange = (e) => {
    UserPerGenerationService.filterUserByGeneration(e.target.value).then(
      (r) => {
        let first = r.payload
        let seconde = groupMember

        for (var i = 0; i < first.length; i++) {
          for (var j = 0; j < seconde.length; j++) {
            if (first[i].id === seconde[j].id) first.splice(i, 1);
          }
        }

        setMember(first);
      }
    );
  };

  const filtered = !search
  ? member
  : member.filter((person) =>
      person.fullName.toLowerCase().includes(search.toLowerCase())
    );

  const handleSearchChange = (e) => {
    setNewSearch(e.target.value);
  };



  useEffect(() => {
    generationService
      .getAllGeneration(10, 1)
      .then((res) => dispatch(getAllGeneration(res)));
  }, [dispatch]);



  const onSelectedChange = (e) => {
    setSelected([...selected, e.target.value]);
  };

  const handleSubmit = () => {
    selected.map((item) => {
      const addData = {
        groupId: id,
        userId: item,
      };
      groupMemberService
        .addGroupMember(addData)
        .then((r) => dispatch(addGroupMember(r)));
      setIsOpen(false);
    });
  };

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }
  return (
    <>
      <div
        onClick={openModal}
        className="group max-w-xs mx-auto hover:rounded-md focus:rounded-sm flex justify-center cursor-pointer"
      >
        <img src={add} alt="" className="bg-white" />
      </div>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={openModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto ">
            <div className="flex min-h-full items-center justify-center text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className=" max-w-[90%]  bg-white md:max-w-[90%] sm:max-w-[90%] lg:max-w-[700px] transform overflow-hidden rounded-lg  py-5 px-5 w-full  text-left align-middle shadow-xl transition-all overflow-y-auto">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900"
                  >
                    <p className="text-lg sm:text-lg md:text-2xl lg:text-2xl font-semibold text-black text-center">
                      Add Group Member
                    </p>
                  </Dialog.Title>

                  <div className="grid-cols-2  sm:grid-cols-2 md:flex lg:flex justify-between pt-3 mb-3 gap-2">
                    <div className="w-full  mt-1">
                      <form>
                        <div className="relative w-full">
                          <input
                            value={search}
                            onChange={handleSearchChange}
                            type="search"
                            className="block p-2.5 w-full text-sm text-gray-900 bg-white rounded-lg border  focus:outline-none"
                            placeholder="Search contact ...."
                            required
                          />
                          <button
                            type="submit"
                            className="absolute top-0 right-0 p-2.5 text-sm font-medium text-white bg-blue-700 rounded-r-lg border border-blue-700  focus:ring-4 focus:outline-none  "
                          >
                            <svg
                              className="w-5 h-5"
                              fill="none"
                              stroke="currentColor"
                              viewBox="0 0 24 24"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                              ></path>
                            </svg>
                          </button>
                        </div>
                      </form>
                    </div>
                    <div className="w-full mt-1">
                      <div>
                        <select
                          onChange={handleOnChange}
                          required
                          defaultValue={""}
                          className="text-sm sm:text-sm md:text-base lg:text-base p-2.5   px-3  w-full bg-white border shadow-sm border-slate-300 placeholder-purple-400 focus:outline-none block  rounded-md focus:ring-1"
                        >
                          <option disabled hidden value="">
                            Choose a generation
                          </option>
                          {generation?.map((item, key) => {
                            return (
                              <option key={key} value={item.id}>
                                {item.generationName}
                              </option>
                            );
                          })}
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="grid-cols-2  sm:grid-cols-2 md:flex   lg:flex justify-between  gap-2  ">
                    <div className="h-[40vh] mt-3  rounded-lg bg-gray-100 overflow-y-auto w-full">
                      {filtered?.map((member, index) => (
                        <div
                          className="relative flex justify-between  items-center py-2 mt-1 px-5 cursor-pointer hover:bg-white "
                          key={index}
                        >
                          <div className="flex items-center">
                            <img
                              className=" w-8 h-8 rounded-full"
                              src={member.profileImage}
                              alt=""
                            />
                            <span className="block ml-2  text-gray-600 ">
                              <p className=" font-semibold text-left">
                                {member.fullName}
                              </p>
                            </span>
                          </div>
                          <div>
                            <input
                              onChange={onSelectedChange}
                              type="checkbox"
                              value={member.id}
                              className="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500"
                            />
                          </div>
                        </div>
                      ))}
                    </div>

                    <div className="h-[40vh] mt-3  rounded-lg bg-gray-100 overflow-y-auto w-full">
                      {groupMember.map((member, index) => (
                        <div
                          className="flex  justify-between items-center cursor-pointer py-2  hover:bg-white mt-1 px-5"
                          key={index}
                        >
                          <div className="relative flex  items-center  ">
                            <img
                              className=" w-8 h-8 rounded-full"
                              src={member.profileImage}
                              alt=""
                            />
                            <span className="block ml-2  text-gray-600 ">
                              <p className=" font-semibold text-left">
                                {member.fullName}{" "}
                              </p>
                            </span>
                          </div>
                          <div>
                            <img src={close} alt="" />
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>

                  <div className="flex justify-end mt-3">
                    <div>
                      <button
                        onClick={closeModal}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2  text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-red-800  rounded-md "
                      >
                        Cancel
                      </button>
                    </div>
                    <div>
                      <button
                        onClick={() => handleSubmit()}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2   text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-blue-800 rounded-md"
                      >
                        OK
                      </button>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
