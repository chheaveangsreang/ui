import React, { useEffect } from 'react'
import ReactPaginate from 'react-paginate'
import { useSelector, useDispatch } from 'react-redux';
import { getAllGroup } from '../../redux/slices/groupSlice';
import groupMemberService from '../../service/groupMemberService';
import groupService from '../../service/groupService';

export default function GroupMemberPagination({setPage}) {

  const allMember = useSelector((state) => state.groupMember.value)

  const dispatch = useDispatch();

  useEffect(() => {
    groupMemberService.listGroupMember().then(r => dispatch(getAllGroup(r)))
  },[dispatch])

  console.log("All Member: ",allMember.length)

  const handlePageClick = ({selected}) => {   
    setPage(selected+1);
    console.log("selected",selected)
  }

    const pageSize = Math.ceil(allMember.length/10);
    console.log("pagesize:",pageSize)
  return (
    
    <div>
      {allMember.length > 0 ?
      <ReactPaginate
        previousLabel={"Prev"}
        nextLabel={"Next"}
        pageCount={pageSize}
        onPageChange={handlePageClick}
        containerClassName={"paginationButton"}
        previousLinkClassName={"paginationButtonPrev"}
        nextLinkClassName={"paginationButtonNext"}
        activeClassName={"onActive"} 
      /> : ''}
    </div>
  )
}
