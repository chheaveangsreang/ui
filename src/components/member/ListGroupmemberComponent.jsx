import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useParams } from "react-router-dom";
import { getAllGroupMember } from "../../redux/slices/groupMemberSlice";
import groupMemberService from "../../service/groupMemberService";
import { getUserInfo } from "../../utils/crypto";
import ProfileBarComponent from "../user/ProfileBarComponent";
import AddGroupMember from "./AddGroupMember";
import DeleteGroupMemberComponent from "./DeleteGroupMemberComponent";
import GroupMemberPagination from "./GroupMemberPagination";
export default function ListGroupmemberComponent() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const [role, setRole] = useState("");

  const userInfo = getUserInfo();

  const groupMember = useSelector((state) => state.groupMember.value);
  console.log("Group Member: ", groupMember);

  const [search, setNewSearch] = useState("");

  const handleSearchChange = (e) => {
    setNewSearch(e.target.value);
  };
  const [page, setPage] = useState(1);
  useEffect(() => {
    setRole(userInfo?.roles[0].name);
    groupMemberService
      .listGroupMember(id, 10, page)
      .then((r) => dispatch(getAllGroupMember(r)));
  }, [dispatch]);

  return (
    <div>
      <div className="block  max-w-full border-gray-200 ">
        <div className="flex justify-between  items-center p-0 lg:p-3 md:p-3 sm:p-3 pt-2">
          <h3 className="whitespace-nowrap lg:text-3xl md:text-lg sm:text-base text-base   font-bold text-left">
            Member
          </h3>

          <div>
            <ProfileBarComponent />
          </div>
        </div>
        <div className="flex justify-between mt-4">
          <div className="flex justify-between gap-2 basis-[80%]">
            <div className="w-[100%]">
              <form>
                <div className="relative w-full">
                  <input
                    type="search"
                    value={search}
                    onChange={handleSearchChange}
                    className="block p-2 lg:p-2.5 md:p-2.5 w-full  lg:text-sm md:text-sm text-xs text-gray-900 bg-white rounded-lg border  focus:outline-none"
                    placeholder="Search member ...."
                    required
                  />
                  <button className="absolute top-0 right-0 p-2 lg:p-2.5 md:p-2.5 text-sm font-medium text-white bg-blue-700 rounded-r-lg border border-blue-700  focus:ring-4 focus:outline-none  ">
                    <svg
                      className="lg:w-5 lg:h-5 md:w-5 md:h-5 w-4 h-4"
                      fill="none"
                      stroke="currentColor"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                      ></path>
                    </svg>
                  </button>
                </div>
              </form>
            </div>
            <div className="w-[100%]"></div>
          </div>

          <div
            className={`${
              role === "ROLE_ADMIN" || role === "ROLE_TEACHER"
                ? "block"
                : "hidden"
            } "flex justify-end basis-[20%]"`}
          >
            <div className="mr-2 mt-2">
              <AddGroupMember />
            </div>
          </div>
        </div>
      </div>

      <div className="bg-white mt-2 mr-2 sm:mr-2 md:mr-0 lg:mr-0 ">
        <div className="h-[80vh] overflow-y-auto ">
          <table className="table-auto w-full text-sm text-left text-gray-500  divide-y-2 bg-bgmodal overflow-x-auto max-w-[100%]">
            <thead className="text-xs text-gray-700 uppercase  ">
              <tr>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs  sm:text-sm md:text-sm lg:text-base"
                >
                  No
                </th>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs  sm:text-sm md:text-sm lg:text-base"
                >
                  All Members
                </th>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base  "
                >
                  Gender
                </th>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs  sm:text-sm md:text-sm lg:text-base  hidden sm:hidden md:block lg:block"
                >
                  Date of Birth
                </th>

                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs  sm:text-sm md:text-sm lg:text-base "
                >
                  Course Type
                </th>
                <th
                  scope="col"
                  className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs  sm:text-sm md:text-sm lg:text-base  hidden sm:hidden md:block lg:block"
                >
                  Created
                </th>
                {role === "ROLE_ADMIN" || role === "ROLE_TEACHER" ? (
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs  sm:text-sm md:text-sm lg:text-base"
                  >
                    Action
                  </th>
                ) : (
                  <th
                    scope="col"
                    className="hidden px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-xs  sm:text-sm md:text-sm lg:text-base"
                  >
                    Action
                  </th>
                )}
              </tr>
            </thead>
            <tbody>
              {groupMember.length > 0 ? groupMember
                ?.filter((item) => {
                  if (item === "") {
                    return item;
                  } else if (
                    item?.fullName
                      ?.toLowerCase()
                      .includes(search?.toLowerCase())
                  ) {
                    return item;
                  }
                })
                .map((item, index) => (
                  <tr
                    className="bg-bgmodal border-b-2 border-white rounded-lg"
                    key={index}
                  >
                    <td className="px-2 py-2 lg:px-6   md:px-6  sm:px-2 sm:py-2 text-xs  sm:text-sm md:text-sm lg:text-base">
                      {item.id}
                    </td>
                    <td className="max-w-[500px]  font-medium text-gray-900  items-center px-2 py-2 lg:px-6   md:px-6 sm:px-2 sm:py-2">
                      {/* {role === "ROLE_ADMIN" ?  */}

                      <NavLink
                        to={`/profile-detail/${item?.uuid}`}
                        className="flex justify-start items-center"
                      >
                        <img
                          src={item?.profileImage}
                          alt=""
                          className="object-cover w-[30px] h-[30px] lg:w-[50px] lg:h-[50px] md:w-[50px] md:h-[50px] sm:w-[30px] sm:h-[30px]   rounded-lg mr-2"
                        />
                        <p className=" text-xs sm:text-sm md:text-sm lg:text-base">
                          {item.fullName}
                        </p>
                      </NavLink>
                    </td>
                    <td className="px-2 py-2 lg:px-6 md:px-6  sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base ">
                      {item?.gender}
                    </td>
                    <td className="px-2 mt-3 py-2 lg:px-6 md:px-6  sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base hidden sm:hidden md:block lg:block">
                      {item?.dob}
                    </td>

                    <td className="px-2 py-2 lg:px-6 md:px-6  sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base   ">
                      {item?.course?.courseName}
                    </td>
                    <td className="px-2  py-2 lg:px-6 md:px-6  sm:px-2 sm:py-2 text-xs sm:text-sm md:text-sm lg:text-base  hidden sm:hidden md:block lg:block">
                      {item?.createdDate}
                    </td>

                    <td>
                      <div
                        className={`${
                          role === "ROLE_ADMIN" || role === "ROLE_TEACHER"
                            ? "block"
                            : "hidden"
                        } px-2 mt-3 py-2 lg:px-6   md:px-6  sm:px-2 sm:py-2 items-center`}
                      >
                        <div>
                          <DeleteGroupMemberComponent
                            groupId={id}
                            memberId={item.id}
                          />
                        </div>
                      </div>
                    </td>
                  </tr>
                )): <b className="text-center">No data available</b>}
            </tbody>
          </table>
          {/* pagenation */}
          <div className="flex justify-end  py-3">
            <div>
              <GroupMemberPagination setPage={setPage}/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
