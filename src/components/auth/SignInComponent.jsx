import GoogleLogin from "react-google-login";
import { NavLink, useNavigate } from "react-router-dom";
import google from "../../assets/images/google.png";
import signin from "./../../assets/images/Sign in.png";
import { useState, useEffect } from "react";
import authService from "../../service/authService";
import { decryptToken, encryptToken } from "../../utils/crypto";
import { data } from "autoprefixer";

export default function SignInComponent({ item }) {
  const navigate = useNavigate();
  const clientId =
    "832345515082-u0i3a360u803e0pl4bbnpt28mjg942pa.apps.googleusercontent.com";
  const [formData, setFormData] = useState({ email: "", password: "" });

  const [userInfo, setUserInfo] = useState(localStorage.getItem("info"));
  const decryptedInfo = decryptToken(userInfo);
  const [error, setError] = useState(null);
  const [btnClass, setBtnClass] = useState('btn btn-wide hover:bg-purple-800 bg-purple-800')

  const onSuccess = (response) => {
    console.log(response.profileObj);
    item = response.profileObj;
    navigate("/register", { state: { ...item } });
  };

  const checkRoleNavigation = () => {
    if (decryptedInfo?.roles[0]?.name === "ROLE_ADMIN") {
      navigate("/admin/newfeed");
    } else if (
      decryptedInfo?.roles[0]?.name === "ROLE_TEACHER" ||
      decryptedInfo?.roles[0]?.name === "ROLE_STUDENT"
    ) {
      navigate("/user/newfeed");
    } else {
      navigate("/");
    }
  };

  useEffect(() => {
    if (userInfo !== null) {
      checkRoleNavigation();
    }
  }, [userInfo]);

  const handleOnChange = (e) => {
    setFormData((pre) => ({
      ...pre,
      [e.target.name]: e.target.value,
    }));
  };


  const onSubmit = (e) => {
    e.preventDefault();
    setBtnClass('btn btn-wide loading hover:bg-purple-800 bg-purple-800 disabled')
    if (
      formData.email === null ||
      (formData.password === "") & (formData.password === null) ||
      formData.password === ""
    ) {
      setError("Please fill out the field before sign in.");
    }
    authService
      .login(formData)
      .then((r) => {
        if (r.success === true && r.payload.status === true) {
          setBtnClass('btn btn-wide hover:bg-purple-800 bg-purple-800')
          const encryptedToken = encryptToken(r.payload.token);
          const encryptedInfo = encryptToken(r.payload);
          localStorage.setItem("info", encryptedInfo);
          localStorage.setItem("jwt", encryptedToken);
          setUserInfo(localStorage.getItem("info"));
          setError(null);
        } else if (r.success === false) {
          setError("Invalid Email or Password");
        }

        console.log("res : ", r);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <div className="w-full lg:w-8/12 md:w-8/12 md:divide-x-2 group mx-auto overflow-hidden rounded-2xl bg-white space-y-3">
      <div className="grid grid-cols-2 gap-2 items-center card">
        <div className="hidden md:hidden sm:hidden lg:block md:col-span-0 sm:col-span-0">
          <img src={signin} alt="" />
        </div>
        <div className="col-span-2 md:col-span-2 lg:col-span-1 ">
          <div className="md:p-10 sm:p-10 lg:p-5 p-10 flex items-center flex-col justify-center">
            <h1 className="lg:text-3xl md:text-2xl sm:text-base text-base text-center mb-10 font-bold">Welcome to <br></br><span className="text-[#5A46AA]">KSHRD Media</span></h1>
            <GoogleLogin
              clientId={clientId}
              render={(renderProps) => (
                <button
                  onClick={renderProps.onClick}
                  type="button"
                  className="btn gap-2"
                >
                  <div className="inline-flex items-center dark:focus:ring-[#4285F4]/55 p-1 text-sm">
                    <img src={google} alt="google" className="w-5 mr-2" />
                    Sign up with Google
                  </div>
                </button>
              )}
              onSuccess={onSuccess}
              cookiePolicy={"single_host_origin"}
              isSignedIn={false}
            />
            <div className="divider m-5">OR</div>
            <h1 className="lg:text-3xl md:text-2xl sm:text-base text-base mb-5 font-bold text-center">Sign In</h1>
            <form onSubmit={onSubmit} className="flex flex-col items-center justify-center">
              <div className="mb-2">
                  {error && (
                    <label>
                      <div className="p-2 text-center w-full bg-red-200 text-red-600 rounded-lg">
                          {error}
                      </div>

                    </label>
                  )}
                </div>
              {/* <label className="block mb-4" htmlFor="email"> */}
                <p className="w-full mb-2 text-md after:content-['*'] after:ml-0.5 after:text-red-500 block font-medium text-slate-700 text-left">
                  Email address
                </p>
                <input
                  required
                  type="email"
                  id="email"
                  name="email"
                  onChange={handleOnChange}
                  value={formData.email || ""}
                  className="input input-bordered w-full mb-3"
                />

              {/* </label> */}
              {/* <label className="block mb-4"> */}
                <p className="w-full mb-2 after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700 text-left">
                Password
                </p>
                <input
                  required
                  type="password"
                  name="password"
                  value={formData.password || ""}
                  onChange={handleOnChange}
                  className="input input-bordered w-full max-w-xs"
                />

              {/* </label> */}
              <div className=" underline my-5 text-right text-sm">
                <NavLink to="/" className={`text-purple-900`}>Forget your password</NavLink>
              </div>
              <div className="flex items-start">
                <button type="submit" className="text-[10px] text-xs sm:text-smmd:text-sm lg:text-sm  mb-4  hover:bg-purple-800 text-white font-semibol py-2 px-8 border border-black-200 rounded-md bg-[#7868b7]">
                  SIGN IN
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      </div>
  );
}
