import React, { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import logoutIcon from "./../../assets/icons/logout.svg";
import chatlogo from "./../../assets/images/chat-logo.png";
import { useNavigate } from "react-router-dom";
import authService from "../../service/authService";
export default function LogoutComponent() {
  let [isOpen, setIsOpen] = useState(false);

  const navigate = useNavigate()

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }




  const handleLogout = ()=>{
    authService.logout();
    navigate('/')
  }

  return (
    <>
      <div
        onClick={openModal}
        className="group max-w-xs mx-auto hover:rounded-md focus:rounded-sm flex justify-center"
      >
        <img src={logoutIcon} alt="" className="w-7 h-7" />
      </div>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto ">
            <div className="flex min-h-full items-center justify-center text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className=" max-w-[300px] md:max-w-[400px] sm:max-w-[300px] transform overflow-hidden rounded-lg bg-white py-5 w-full  text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900"
                  >
                    <img src={chatlogo} alt="" className="w-[70px] mx-auto" />
                    <p className="text-base font-semibold pt-3 text-black text-center">
                      Log out of KSHRD Media
                    </p>
                  </Dialog.Title>

                  <p className="text-sm py-5 text-black text-center">
                    Are you sure! do you want to Log out ?
                  </p>

                  <div className="flex justify-evenly py-3">
                    <div>
                      <button
                        onClick={handleLogout}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2 mb-2 text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-purple-800 rounded-md"
                      >
                        Log out
                      </button>
                    </div>
                    <div>
                      <button
                        onClick={closeModal}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2 mb-2 text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-red-800  rounded-md "
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
