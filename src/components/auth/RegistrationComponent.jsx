import React, { useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { getAllGeneration } from "../../redux/slices/generationSlice";
import generationService from "../../service/generationService";
import signin from "./../../assets/images/Sign in.png";
import { useDispatch, useSelector } from "react-redux/es/exports";
import authService from "../../service/authService";
import Swal from "sweetalert2";


export default function RegistrationComponent() {
  const navigate = useNavigate();
  const [fullName, setFullName] = useState("");
  const [profileImage,setProfileImage] = useState("")
  const [email,setEmail]= useState("")
  const [error, setError] = useState(null);
  const [btnClass, setBtnClass] = useState('btn btn-wide hover:bg-purple-800 bg-purple-800')
  const location = useLocation();
  const dispatch = useDispatch();
  const [formData, setFormData] = useState({
    fullName: fullName,
    password: "",
    confirmPassword: "",
    generationId: 0,
    profileImage: profileImage,
    gender: "",
    email: email,
    dob: "",
  });

  const generation = useSelector((state) => state.generation.value);

  useEffect(() => {
    setFullName(location.state.name);
    setProfileImage(location.state.imageUrl)
    setEmail(location.state.email)
    generationService
      .getAllGeneration(5, 1)
      .then((r) => dispatch(getAllGeneration(r)));
  }, []);

  const handleOnChange = (e) => {
    setFormData((pre) => ({
      ...pre,
      [e.target.name]: e.target.value,
      fullName: fullName,
      email: email,
      profileImage: profileImage,
    }));
  };

  const onSubmit = (e) => {
    
    e.preventDefault();
    setBtnClass('btn btn-wide loading hover:bg-purple-800 bg-purple-800 disabled')
    if (
      formData.fullName === "" || formData.dob === "" ||
      formData.gender === "" || formData.password === "" ||
      formData.generationId === 0 || formData.email === ""
    ) {
      setError("Please fill out the field before sign up.");
    }else if(formData.password !== formData.confirmPassword){
      setError("Password and confirm password not match");
    }else{
      authService
      .register(formData)
      .then((r) => {
        if (r.success === true) {
          setBtnClass('btn btn-wide hover:bg-purple-800 bg-purple-800')
          setError(null);
          Swal.fire({
            icon: "info",
            position: "center",
            icon: "success",
            title: "Your account have been register successfully! Please check your email ...",
          }).then(() => {
            navigate("/");
          });
        }else if (r.success === false) {
          setError("Invalid Register please try again");
        }
      })
      .catch((e) => {
        setError(e.response?.data?.error?.password ?? e.response?.data?.error?.dob);
      });
    }
  };

  return (
    <div className="w-full lg:w-8/12 md:w-8/12 md:divide-x-2 group mx-auto  bg-white  space-y-3">
      <div className="grid grid-cols-2 gap-2 bg-white items-center">
        <div className="hidden sm:hidden md:hidden lg:block">
          <img src={signin} alt="" />
        </div>
        <div className="col-span-2 sm:col-span-2 md:col-span-2 lg:col-span-1">
          <div className="lg:p-5 md:p-10 sm:p-10 p-5 ">
            <h1 className="lg:text-3xl md:text-2xl sm:text-base text-base text-left mb-5 font-bold">
              Setup Your Profile
            </h1>
            <form onSubmit={onSubmit}>
            <div className="mb-2">
                  {error && (
                    <label>
                      <div className="p-2 text-center w-full bg-red-200 text-red-600 rounded-lg">
                          {error}
                      </div>

                    </label>
                  )}
                </div>
              <div className=" mb-4">
                <input
                  value={fullName}
                  onChange={handleOnChange}
                  type="text"
                  disabled={true}
                  name="fullName"
                  className="disabled:bg-gray-100 px-3 py-2 text-xs w-full bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  rounded-md sm:text-sm focus:ring-1"
                  placeholder="Full name"
                />
              </div>

              <div className="grid grid-cols-3 gap-1 space-x-0 sm:space-x-0 mt-1 mb-4">
                <div className="md:col-span-2 sm:col-span-2 lg:col-span-2 col-span-2 ">
                  <input
                    onChange={handleOnChange}
                    type="date"
                    name="dob"
                    placeholder="Date of Birth"
                   className=" w-full text-xs px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  rounded-md sm:text-sm focus:ring-1"
                  />
                </div>
                <div className="md:col-span-1 sm:col-span-1 lg:col-span-1 col-span-1 ">
                  <select
                    onChange={handleOnChange}
                    id="gender"
                    name="gender"
                    className="px-3 py-2 text-xs w-full  bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  rounded-md sm:text-sm focus:ring-1"
                    defaultValue={""}
                  >
                    <option disabled hidden value="">
                      Gender
                    </option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                  </select>
                </div>
              </div>
              <div className="mb-4">
                <select
                  defaultValue={""}
                  onChange={handleOnChange}
                  id="generation"
                  name="generationId"
                  className=" px-3 text-xs py-2 w-full bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  rounded-md sm:text-sm focus:ring-1"
                >
                  <option disabled hidden value="">
                    Choose a generation
                  </option>
                  {generation?.map((item, key) => {
                    return (
                      <option key={key} value={item.id}>
                        {item.generationName}
                      </option>
                    );
                  })}
                </select>
              </div>

              <div className="mb-4">
                <input
                  onChange={handleOnChange}
                  type="password"
                  name="password"
                  className="px-3 py-2 text-xs w-full bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  rounded-md sm:text-sm focus:ring-1"
                  placeholder="Password"
                />
              </div>

              <div className="mb-4">
                <input
                  onChange={handleOnChange}
                  type="password"
                  name="confirmPassword"
                  placeholder="Comfirm Password"
                  className=" px-3 py-2 w-full text-xs bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  rounded-md sm:text-sm focus:ring-1"
                />
              </div>

              <div className="flex items-start">
                <button
                  type="submit"
               
                  className="text-[10px] text-xs sm:text-smmd:text-sm lg:text-sm  mb-4  hover:bg-purple-800 text-white font-semibol py-2 px-8 border border-black-200 rounded-md bg-[#7868b7]"
                >
                  SIGN UP
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
