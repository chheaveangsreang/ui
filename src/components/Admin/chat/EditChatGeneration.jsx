import React, { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import update from "./../../../assets/icons/update.svg";
export default function EditChatGeneration() {
  let [isOpen, setIsOpen] = useState(false);

  const payload = {
    id: 1,
    name: "java 10th generation",
  };

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }
  return (
    <>
      <li onClick={openModal}>
        <div className="flex justify-center items-center w-full hover:bg-green-300 bg-green-200 p-2 rounded-lg mb-2 cursor-pointer">
          <img src={update} alt="" />
          <p className="ml-3 text-green-600">Edit</p>
        </div>
      </li>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={openModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto ">
            <div className="flex min-h-full items-center justify-center text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className=" max-w-[300px] bg-white md:max-w-[400px] sm:max-w-[300px] transform overflow-hidden rounded-lg  py-5 px-5 w-full  text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900"
                  >
                    <p className="text-2xl font-semibold text-black text-center">
                      Edit Chat Generation
                    </p>
                  </Dialog.Title>

                  <div className="pt-7 mb-7">
                    <input
                      required
                      defaultValue={payload.name}
                      placeholder="Group Name"
                      type="text"
                      className="text-[10px] px-2 sm:text-sm md:text-sm lg:text-sm py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md focus:ring-1"
                    />
                  </div>

                  <div className="flex justify-end ">
                    <div>
                      <button
                        onClick={closeModal}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2 mr-2  text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-red-800  rounded-md "
                      >
                        Cancel
                      </button>
                    </div>
                    <div>
                      <button
                        onClick={closeModal}
                        className="py-2 px-5 flex items-center sm:px-3 md:px-5 lg:px-5 sm:py-2 md:py-2 lg:py-2   text-xs md:text-sm lg:text-sm font-medium  text-white focus:outline-none bg-blue-800 rounded-md"
                      >
                        OK
                      </button>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
