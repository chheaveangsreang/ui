import React, { useEffect, useState } from "react";
import menu from "../../../../src/assets/icons/menu.svg";
import iconDate from "../../../../src/assets/icons/iconDate.svg";
import EditGenerationComponent from "./EditGenerationComponent";
import DeleteGenerationComponent from "./DeleteGenerationComponent";
import PostGenerationComponent from "./PostGenerationComponent";
import Pagination from "../../Pagination";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import generationService from "../../../service/generationService";
import { getAllGeneration } from "../../../redux/slices/generationSlice";
import ProfileBarComponent from "../../user/ProfileBarComponent";
import GenerationPagination from "./GenerationPagination";



export default function Academic() {

  const dispatch = useDispatch()
  const generation = useSelector((state)=>state.generation.value)
  const [search, setNewSearch] = useState("");

  const handleSearchChange = (e) => {
    setNewSearch(e.target.value);
  };

  const [page,setPage] = useState(1)
  
  // Pagination
  useEffect(() => {
    generationService.getAllGeneration(10,page).then((r) => dispatch(getAllGeneration(r)))
  },[dispatch, page])

  return (
    <div>
      <div className="bg-gray-100 mt-0 pt-0 h-screen">
        <div className="block  max-w-full border-gray-200 sticky top-0 mt-0 pt-0 bg-gray-100">
          <div className="flex justify-between  items-center lg:p-3 md:p-3 p-3">
            <h3 className="whitespace-nowrap lg:text-3xl md:text-lg sm:text-sm text-xs    font-bold text-left">
              Academic
            </h3>
            <div>
              <ProfileBarComponent/>
            </div>
          </div>

          {/* Button Search  */}
          <div className="flex justify-between items-center lg:p-3 md:p-3 p-3">
            <div className="w-[50%]">
             
                <div className="relative w-full">
                  <input
                    type="search"
                    value={search}
                    onChange={handleSearchChange}
                    className="block p-2 lg:p-2.5 md:p-2.5 w-full  lg:text-sm md:text-sm text-xs text-gray-900 bg-white rounded-lg border  focus:outline-none"
                    placeholder="Search...."
                    required
                  />
                  <button
                    type="submit"
                    className="absolute top-0 right-0 p-2 lg:p-2.5 md:p-2.5 text-sm font-medium text-white bg-blue-700 rounded-r-lg border border-blue-700  focus:ring-4 focus:outline-none  "
                  >
                    <svg
                      className="lg:w-5 lg:h-5 md:w-5 md:h-5 w-4 h-4"
                      fill="none"
                      stroke="currentColor"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                      ></path>
                    </svg>
                  </button>
                </div>
          
            </div>

            {/* Add  */}
            <div>
              <PostGenerationComponent />
            </div>
          </div>
        </div>

        <div className="p-3 bg-gray-100 ">
          <div className="overflow-y-auto max-h-[80vh]">
            <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 divide-y-2  ">
              <thead className="text-xs text-gray-700 uppercase dark:bg-gray-700 dark:text-gray-400  ">
                <tr className="rounded-lg">
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base"
                  >
                    No
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base"
                  >
                    <p className="float-left">Name</p>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base"
                  >
                    Student
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base"
                  >
                    <p className="float-left">Date</p>
                  </th>
                  <th
                    scope="col"
                    className="px-2 py-2 lg:px-5 lg:py-3  md:px-5 md:py-3 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base"
                  >
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                {generation?.filter((item) => {
                if(search === ''){
                  return item;
                }else if(item?.generationName?.toLowerCase().includes(search?.toLowerCase())){
                  return item;
                }
              }).map((item, index) => (
                  <tr className="bg-white border-b-4 border-gray" key={index}>
                    {/* Auto Number */}
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base">
                      {index+=1}
                    </td>
                    {/* Image */}

                    <td className="max-w-[500px] mt-3 font-medium text-gray-900  flex items-center px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2">
                      <NavLink
                        // to={`/admin/groupacademic/${item.id}`}
                        to={`/admin/groupacademic`}
                        className={({ isActive }) =>
                          [isActive ? "bg-purple-200  text-purple-800" : null]
                            .filter(Boolean)
                            .join("")
                        }
                      >
                        <div className="flex justify-start">
                          <img
                            src={item?.imageUrl}
                            alt=""
                            className="w-[30px] h-[30px] lg:w-[40px] lg:h-[40px] md:w-[50px] md:h-[50px] sm:w-[30px] sm:h-[30px]  rounded-lg mr-3 object-contain"
                          />
                          <p className="text-[10px] sm:text-sm md:text-sm lg:text-base ">
                            {item?.generationName}
                          </p>
                        </div>
                      </NavLink>
                    </td>

                    {/* Amount Student */}
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base">
                      <p className="text-[10px] sm:text-sm md:text-sm lg:text-base"></p>
                      {item?.amountOfStudent} Students
                    </td>
                    {/* Date */}
                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2  ">
                      <div className="flex justify-start">
                        <img src={iconDate} alt="" />
                        <p className="px-2 py-2 lg:px-1 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-[10px] sm:text-sm md:text-sm lg:text-base ml-1">
                          {item?.createdDate}
                        </p>
                      </div>
                    </td>
                    {/* Action */}

                    <td className="px-2 py-2 lg:px-6 lg:py-2  md:px-6 md:py-4 sm:px-2 sm:py-2 text-center ">
                      <div className="dropdown dropdown-end">
                        <label tabIndex="0" className="m-1 cursor-pointer">
                          <img src={menu} alt="" className=" rounded-lg mr-3" />
                        </label>
                        <ul
                          tabIndex="0"
                          className="dropdown-content p-2 shadow bg-bgmodal rounded-md w-[200px] mr-[20px]"
                        >
                          <div>
                            <EditGenerationComponent index={item}/>
                          </div>
                          <div>
                            <DeleteGenerationComponent index={item?.id}/>
                          </div>
                        </ul>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            {/* pagenation */}
            <div className="flex justify-end  py-3">
              <div>
                <GenerationPagination setPage={setPage} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
