import React, { useEffect } from 'react'
import ReactPaginate from 'react-paginate'
import { useSelector, useDispatch } from 'react-redux';
import { getAllGeneration } from '../../../redux/slices/generationSlice';
import generationService from '../../../service/generationService';


export default function GenerationPagination({setPage}) {

  const allGeneration = useSelector((state) => state.generation.value)

  const dispatch = useDispatch();

  useEffect(() => {
    generationService.getAllGeneration().then(r => dispatch(getAllGeneration(r)))
  },[dispatch])

  console.log("All group: ",allGeneration.length)

  const handlePageClick = ({selected}) => {   
    setPage(selected+1);
    console.log("selected",selected)
  }

    const pageSize = Math.ceil(allGeneration.length/10);
    console.log("pagesize:",pageSize)
  return (
    
    <div>
      {allGeneration.length > 0 ?
      <ReactPaginate
        previousLabel={"Prev"}
        nextLabel={"Next"}
        pageCount={pageSize}
        onPageChange={handlePageClick}
        containerClassName={"paginationButton"}
        previousLinkClassName={"paginationButtonPrev"}
        nextLinkClassName={"paginationButtonNext"}
        activeClassName={"onActive"} 
      /> : ''}
    </div>
  )
}
