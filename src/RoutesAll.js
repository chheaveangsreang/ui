import { Route, Routes } from "react-router-dom";
import ListChatGeneration from "./components/Admin/chat/ListChatGeneration";
import Academic from "./components/Admin/generation/Academic";
import EditAnnouncementComponent from "./components/announcement/EditAnnouncementComponent";
import NewsFeedComponent from "./components/announcement/NewsFeedComponent";
import PostDetailsComponent from "./components/announcement/PostDetailsComponent";
import PostOnGenerationComponent from "./components/announcement/PostOnGenerationComponent";
import RegistrationComponent from "./components/auth/RegistrationComponent";
import SignInComponent from "./components/auth/SignInComponent";
import GroupComponent from "./components/group/GroupComponent";
import GroupTeacherComponent from "./components/group/GroupTeacherComponent";
import ListGroupmemberComponent from "./components/member/ListGroupmemberComponent";
import MessageComponent from "./components/message/MessageComponent";
import SmallScreenListUser from "./components/message/SmallScreenListUser";
import SmallScreenMessage from "./components/message/SmallScreenMessage";
import StatisticManagement from "./components/statistic/StatisticManagement";
import ListUserComponent from "./components/user/ListUserComponent";
import ProfileDetailsComponent from "./components/user/ProfileDetailsComponent";
import HomePageAdmin from "./pages/HomePageAdmin";
import MessagePage from "./pages/MessagePage";
import PageNotFound from "./pages/PageNotFound";
import ShowAllPostPage from "./pages/ShowAllPostPage";
import SignInUpPage from "./pages/SignInUpPage";
import RequireAuth from "./redux/slices/requierRoute";

function RoutesAll() {
    return (
        <Routes>
            <Route path="/" element={<SignInUpPage />}>
                <Route path="" element={<SignInComponent />} />
                <Route path="register" element={<RegistrationComponent />} />
            </Route>

            {/*public*/}
            <Route element={<RequireAuth allowedRoles={["ROLE_TEACHER", "ROLE_STUDENT", "ROLE_ADMIN"]} />}>
                <Route path="profile-detail/:id" element={<ProfileDetailsComponent />} />
                <Route path="post-detail/:id" element={<PostDetailsComponent />} />
            </Route>
            <Route element={<RequireAuth allowedRoles={["ROLE_TEACHER", "ROLE_ADMIN"]} />}>
                <Route path="edit-announcement/:id" element={<EditAnnouncementComponent />} />
            </Route>

            {/* teacher */}
            <Route element={<RequireAuth allowedRoles={["ROLE_TEACHER", "ROLE_STUDENT"]} />}>
                <Route path="/user" element={<ShowAllPostPage />}>
                    <Route path="newfeed" element={<NewsFeedComponent />}></Route>
                    <Route path="group" element={<GroupComponent />} />
                    <Route path="group/:id" element={<ListGroupmemberComponent />} />
                    <Route
                        path="post-generation/:id"
                        element={<PostOnGenerationComponent />}
                    />
                    <Route path="contact-user" element={<MessagePage />}>
                        <Route path=":id" element={<MessageComponent />} />
                    </Route>
                    <Route path="edit-announcement/:id" element={<EditAnnouncementComponent />} />
                    <Route path="profile-detail/:id" element={<ProfileDetailsComponent />} />
                    <Route path="post-detail/:id" element={<PostDetailsComponent />} />
                    <Route path="sm-contact" element={<SmallScreenListUser />} />
                    <Route path="sm-message/:id" element={<SmallScreenMessage />} />
                    <Route path="group-teacher" element={<GroupTeacherComponent />} />
                    <Route path="group-teacher/:id" element={<ListGroupmemberComponent />} />
                </Route>
            </Route>
            {/* admin */}
            <Route element={<RequireAuth allowedRoles={["ROLE_ADMIN"]} />}>
                <Route path="/admin" element={<HomePageAdmin />}>
                    <Route path="newfeed" element={<NewsFeedComponent />} />
                    {/* <Route path="chat" element={<ListChatGeneration />} /> */}
                    <Route path="contact" element={<MessagePage />}>
                        <Route path=":id" element={<MessageComponent />} />
                    </Route>
                    <Route path="academic" element={<Academic />} />
                    <Route path="groupacademic" element={<GroupTeacherComponent />} />
                    <Route path="groupacademic/:id" element={<ListGroupmemberComponent />} />
                    <Route
                        path="post-generation/:id"
                        element={<PostOnGenerationComponent />}
                    />
                    <Route path="edit-announcement/:id" element={<EditAnnouncementComponent />} />
                    <Route path="profile-detail/:id" element={<ProfileDetailsComponent />} />
                    <Route path="post-detail/:id" element={<PostDetailsComponent />} />
                    <Route path="user" element={<ListUserComponent />} />
                    <Route path="sm-contact" element={<SmallScreenListUser />} >
                        <Route path=":id" element={<SmallScreenMessage />} />
                    </Route>
                    <Route path="statistic" element={<StatisticManagement />} />
                </Route>
            </Route>
            <Route path="*" element={<PageNotFound />}></Route>
        </Routes>
    )
}

export default RoutesAll;