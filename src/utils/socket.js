
import SockJS from 'sockjs-client';
import { over } from 'stompjs';

var stompClient = null;

// export const connectPost = (id) => {
//     stompClient.subscribe(`/user/${id}/private-post-notification`, onMessageRecieve)
// }

// export const connectChat = (id) => {
//     stompClient.subscribe(`/user/${id}//private-message-notification`, onMessageRecieve);
// }

// export const connectToComment = (id) => {
//     stompClient.subscribe(`/user/${id}/private-comment-notification`, onMessageRecieve);
// }

// export const connectReplyComment = (id) => {
//     stompClient.subscribe(`/user/${id}/private-like-notification`, onMessageRecieve);
// }

// export const connectLikePost = (id) => {
//     stompClient.subscribe(`/user/${id}/private-like-notification`, onMessageRecieve);
// }

// export const onMessageRecieve = (data) => {
//     return JSON.parse(data.body);
// }

export const connect = (onConnected) => {
    let Sock = new SockJS('https://api.media.kshrd-ite.com/api/v1/ws-kshrdmedia');
    stompClient = over(Sock);
    stompClient.connect({}, onConnected);
}

const socket = {
    connect,
}

export default socket;
