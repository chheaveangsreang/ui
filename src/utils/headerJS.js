import { decryptToken, parseJwt } from "./crypto";

export const JWT_ATUH = () =>{
  let token = localStorage.getItem("jwt");
  let jwt =
  token !== null
      ? decryptToken(token)
      : null;
  if (jwt !== null) {
    console.log("JWT : ", jwt)
    return `Bearer ${jwt}`;
  }else {
    return "application/json";
  }
};
