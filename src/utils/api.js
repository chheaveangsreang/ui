import axios from "axios";

import  {JWT_ATUH}  from "./headerJS";

axios.defaults.headers.common['Authorization'] = JWT_ATUH();
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

const api = axios.create({
  baseURL: "https://api.media.kshrd-ite.com/api/v1/",
  // baseURL: "http://localhost:8080/api/v1/",
});
console.log("jwt",JWT_ATUH());

export default api;
