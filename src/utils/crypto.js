
var CryptoJS = require("crypto-js");

export const encryptToken = (token) => {
  if (token !== null) {
    const encryptedText = CryptoJS.AES.encrypt(
      JSON.stringify(token),
      "@1!2KSHR_Media$$"
    ).toString();
    return encryptedText;
  }
};

export const decryptToken = (token) => {
  if (token !== null) {
    const bytes = CryptoJS.AES.decrypt(token, "@1!2KSHR_Media$$");
    const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    return decryptedData;
  }
};

export const getUserInfo = () => {
  const userInfo = localStorage.getItem("info");
  if (userInfo !== null) {
    const bytes = CryptoJS.AES.decrypt(userInfo, "@1!2KSHR_Media$$");
    const decryptedInfo = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    return decryptedInfo;
  }
};
