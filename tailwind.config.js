module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'whitesmoke': '#f5f5f5',
        'bgmodal':'#F5F7FA',
      },
      height: {
        '128': '32rem',
      }
    },
  },
  plugins: [require("daisyui")],
}
